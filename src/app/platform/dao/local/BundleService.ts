import { Injectable } from '@angular/core';
import { TrxSubmission } from '@app/model/esub/transaction';

@Injectable({
  providedIn: 'root'
})
export class BundleService {
  trxSubmisson: TrxSubmission;

  constructor() {}

  getData(): TrxSubmission {
    return this.trxSubmisson;
  }
  setData(trxSubmission: TrxSubmission): void {
    this.trxSubmisson = trxSubmission;
  }
}
