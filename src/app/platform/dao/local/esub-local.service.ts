import { Injectable } from '@angular/core';
import {
  DRAFT_LABEL,
  Esubmission,
  IDCARD_REJECTED_LABEL,
  INACTIVE_AGENT_LABEL,
  INCOMPLETE_AGENT_CORE_LABEL,
  LICENSE_EXPIRED_LABEL,
  mstStatus,
  MstStatus,
  PAID_LABEL,
  PAYMENT_INVALID_LABEL,
  PAYMENT_VALIDATION_LABEL,
  SUBMITTED_LABEL,
  Tertanggung,
  TrxSubmission
} from '@app/model/esub/transaction';
import { Conn } from '@app/platform/dao/conn/db.conn';
import { MatDialog } from '@angular/material';
import { LoaderComponent } from '@app/shared';
import { Filter } from '@app/model/filter/Filter';
import { CredentialsService } from '@app/core';
import moment = require('moment');

@Injectable({
  providedIn: 'root'
})
export class EsubLocalService {
  progressDialogRef: any = null;

  statuses: string[] = [
    DRAFT_LABEL,
    SUBMITTED_LABEL,
    INACTIVE_AGENT_LABEL,
    IDCARD_REJECTED_LABEL,
    INCOMPLETE_AGENT_CORE_LABEL,
    LICENSE_EXPIRED_LABEL,
    PAYMENT_INVALID_LABEL,
    PAYMENT_VALIDATION_LABEL,
    PAID_LABEL
  ];

  constructor(private conn: Conn, public progressDialog: MatDialog, public credentialsService: CredentialsService) {}

  public getEsubmissions(spajNo: string, filter: Filter, myCallback: (result: any) => void): void {
    console.log('Filter ===== ' + JSON.stringify(filter));
    if (window.cordova == undefined) {
      myCallback(new Array());
      return;
    }
    this.openProgress();

    var query =
      'SELECT id, draft_id, spajNo, noVa, version, groupPolicyId, agentCode, agentName, officeName, json, spajCloudUrl, idCardCloudUrl, kodeSetor, metodePembayaran, paymentDate, submissionStatusJson, tertanggungListJson, created FROM TRX_SUBMISSION ';
    let param: any[] = new Array();

    query = query + ' WHERE 1+1=2 ';

    if (spajNo != '') {
      query = query + ' AND spajNo LIKE ? ';
      param.push('%' + spajNo + '%');
    }
    query = query + ' AND agentCode = ? ';
    param.push(this.credentialsService.credentials.username);

    if (filter != undefined) {
      if (filter.startDate != undefined && filter.endDate != undefined) {
        query = query + ' AND created >= ? AND created <= ? ';
        param.push(filter.startDate.getTime());
        param.push(filter.endDate.getTime());
      }
      if (filter.productName != undefined && filter.productName != '') {
        if (filter.productName != '--Semua--') {
          query = query + ' AND json LIKE ? ';
          param.push('%' + filter.productName + '%');
        }
      }
      if (filter.status != undefined && filter.status != '') {
        if (filter.status != '--Semua--') {
          query = query + ' AND submissionStatusJson LIKE ? ';
          param.push('%' + filter.status + '%');
        }
      }
    }

    console.log('PARAM ' + param);

    query = query + ' ORDER BY created DESC ';
    this.conn.database.executeSql(
      query,
      param,
      (res: any) => {
        console.log('RESULT SQL ' + JSON.stringify(res));
        let trxSubmissions: TrxSubmission[] = new Array();
        for (var x = 0; x < res.rows.length; x++) {
          let trxSubmission = new TrxSubmission();
          trxSubmission = res.rows.item(x);
          if (trxSubmission.json != null && trxSubmission.json != undefined && trxSubmission.json != '') {
            trxSubmission.esubmission = JSON.parse(trxSubmission.json);
          }
          if (
            trxSubmission.submissionStatusJson != null &&
            trxSubmission.submissionStatusJson != undefined &&
            trxSubmission.submissionStatusJson != ''
          ) {
            trxSubmission.submissionStatus = JSON.parse(trxSubmission.submissionStatusJson);
          }
          trxSubmissions.push(trxSubmission);
        }
        if (filter != undefined) {
          trxSubmissions = trxSubmissions.sort((a: TrxSubmission, b: TrxSubmission) => {
            try {
              if (filter.polisFieldFilterId == 1) {
                // By Date Created
                if (filter.polisFieldFilterDirection == 1) {
                  return (
                    moment(b.created)
                      .toDate()
                      .getTime() -
                    moment(a.created)
                      .toDate()
                      .getTime()
                  );
                }
                if (filter.polisFieldFilterDirection == 2) {
                  return (
                    moment(a.created)
                      .toDate()
                      .getTime() -
                    moment(b.created)
                      .toDate()
                      .getTime()
                  );
                }
              }
              if (filter.polisFieldFilterId == 2) {
                // By Polis Holder Name
                let esubmission1: Esubmission = JSON.parse(a.json);
                let esubmission2: Esubmission = JSON.parse(b.json);
                if (filter.polisFieldFilterDirection == 1) {
                  return esubmission2.nama > esubmission1.nama ? 1 : -1;
                }
                if (filter.polisFieldFilterDirection == 2) {
                  return esubmission1.nama > esubmission2.nama ? 1 : -1;
                }
              }
              if (filter.polisFieldFilterId == 3) {
                // By No KTP
                let esubmission1: Esubmission = JSON.parse(a.json);
                let esubmission2: Esubmission = JSON.parse(b.json);
                if (filter.polisFieldFilterDirection == 1) {
                  return parseInt(b.spajNo) - parseInt(a.spajNo);
                }
                if (filter.polisFieldFilterDirection == 2) {
                  return parseInt(a.spajNo) - parseInt(b.spajNo);
                }
              }
            } catch (e) {
              console.error(e);
            }
            return -1;
          });
        }
        console.log('result jso=====n', trxSubmissions);
        console.log('RESULT SQL PARSE ' + JSON.stringify(trxSubmissions));
        myCallback(trxSubmissions);
        this.closeProgress();
      },
      (error: any) => {
        console.error(error);
        this.closeProgress();
      },
      () => {
        console.log('FINAL');
        this.closeProgress();
      }
    );
  }

  async countSpaj(spajNo: string): Promise<number> {
    var query = 'SELECT COUNT(*) as _count FROM TRX_SUBMISSION WHERE spajNo = ?';
    let param = [spajNo];
    return new Promise<number>(resolve => {
      this.conn.database.executeSql(query, param, (res: any) => {
        let countRes = res.rows.item(0)._count;
        console.log('COUNT EXISTING =' + countRes);
        resolve(countRes);
      });
    });
  }

  public saveEsubmission(draft_id: number, trxSubmission: TrxSubmission, myCallback: (result: any) => void) {
    if (trxSubmission.submissionStatusJson == null) {
      trxSubmission.submissionStatusJson = JSON.stringify(mstStatus);
    }
    if (trxSubmission.json == null) {
      trxSubmission.json = JSON.stringify(trxSubmission.esubmission);
    }
    // const count = await this.countSpaj(spajNo)

    if (!localStorage.getItem(draft_id.toString())) {
      var query =
        'INSERT INTO TRX_SUBMISSION ' +
        '(id, draft_id, spajNo, version, groupPolicyId, agentCode, agentName, officeName, json, ' +
        ' kodeSetor, metodePembayaran, submissionStatusJson, ktp_base64, agent_sign_base64, customer_sign_base64, created) ' +
        'VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) ';
      this.conn.database.executeSql(
        query,
        [
          trxSubmission.id,
          draft_id,
          trxSubmission.spajNo,
          trxSubmission.version,
          trxSubmission.groupPolicyId,
          trxSubmission.agentCode,
          trxSubmission.agentName,
          trxSubmission.officeName,
          trxSubmission.json,
          trxSubmission.kodeSetor,
          trxSubmission.metodePembayaran,
          trxSubmission.submissionStatusJson,
          trxSubmission.ktp_base64,
          trxSubmission.agent_sign_base64,
          trxSubmission.customer_sign_base64,
          new Date().getTime()
        ],
        function(res: any) {
          console.log('INSERT ID ### ' + res.insertId);
          localStorage.setItem(draft_id.toString(), trxSubmission.spajNo);
          myCallback(trxSubmission);
          console.log(res);
        },
        function(error: any) {
          console.log(error);
        }
      );
    } else {
      let param: any[] = [
        trxSubmission.id,
        trxSubmission.spajNo,
        trxSubmission.version,
        trxSubmission.groupPolicyId,
        trxSubmission.agentCode,
        trxSubmission.agentName,
        trxSubmission.officeName,
        trxSubmission.json,
        trxSubmission.kodeSetor,
        trxSubmission.metodePembayaran
      ];

      if (trxSubmission.ktp_base64) {
        param.push(trxSubmission.ktp_base64);
      }
      if (trxSubmission.agent_sign_base64) {
        param.push(trxSubmission.agent_sign_base64);
      }
      if (trxSubmission.customer_sign_base64) {
        param.push(trxSubmission.customer_sign_base64);
      }
      param.push(new Date().getTime(), draft_id, this.credentialsService.credentials.username);

      var query =
        'UPDATE TRX_SUBMISSION SET id = ?, spajNo = ?, version = ?, groupPolicyId = ?, agentCode = ?, agentName = ?, officeName = ?, json = ?, kodeSetor = ?, metodePembayaran = ?, ktp_base64 = ? ';

      if (trxSubmission.ktp_base64) {
        query = query + ', ktp_base64 = ? ';
      }

      if (trxSubmission.agent_sign_base64) {
        query = query + ', agent_sign_base64 = ? ';
      }
      if (trxSubmission.customer_sign_base64) {
        query = query + ', customer_sign_base64 = ? ';
      }

      query = query + ', created = ? ';
      query = query + ' WHERE draft_id = ? AND agentCode = ? ';

      this.conn.database.executeSql(
        query,
        param,
        function(res: any) {
          myCallback(trxSubmission);
          console.log(res);
        },
        function(error: any) {
          console.log(error);
        }
      );
    }
  }

  updateKtpBase64(draft_id: number, ktpBase64: string, myCallback: (result: any) => void) {
    if (window.cordova == undefined) {
      myCallback(null);
      return;
    }
    let param = [ktpBase64, draft_id, this.credentialsService.credentials.username];
    let query = 'UPDATE TRX_SUBMISSION SET ktp_base64 = ? WHERE draft_id = ? AND agentCode = ? ';
    this.conn.database.executeSql(
      query,
      param,
      function(res: any) {
        myCallback(mstStatus);
        console.log(res);
      },
      function(error: any) {
        console.log(error);
        myCallback(mstStatus);
      }
    );
  }

  public updateSubmissionStatusJson(draft_id: number, mstStatus: MstStatus, myCallback: (result: any) => void): void {
    if (window.cordova == undefined) {
      myCallback(null);
      return;
    }
    let param = [JSON.stringify(mstStatus), draft_id, this.credentialsService.credentials.username];
    let query = 'UPDATE TRX_SUBMISSION SET submissionStatusJson = ? WHERE draft_id = ? AND agentCode = ? ';
    this.conn.database.executeSql(
      query,
      param,
      function(res: any) {
        myCallback(mstStatus);
        console.log(res);
      },
      function(error: any) {
        console.log(error);
        myCallback(mstStatus);
      }
    );
  }

  public updateEsubmission(draft_id: number, esubmision: Esubmission, myCallback: (result: any) => void): void {
    if (window.cordova == undefined) {
      myCallback(null);
      return;
    }

    let param = [JSON.stringify(esubmision), draft_id, this.credentialsService.credentials.username];

    let query = 'UPDATE TRX_SUBMISSION SET json = ? WHERE draft_id = ? AND agentCode = ? ';
    this.conn.database.executeSql(
      query,
      param,
      function(res: any) {
        myCallback(mstStatus);
        console.log(res);
      },
      function(error: any) {
        console.log(error);
        myCallback(mstStatus);
      }
    );
  }

  public updateSpajNo(draft_id: number, spajNo: string, myCallback: (result: any) => void): void {
    if (window.cordova == undefined) {
      myCallback(null);
      return;
    }

    let param = [spajNo, draft_id, this.credentialsService.credentials.username];

    let query = 'UPDATE TRX_SUBMISSION SET spajNo = ? WHERE draft_id = ? AND agentCode = ? ';
    this.conn.database.executeSql(
      query,
      param,
      function(res: any) {
        myCallback(mstStatus);
        console.log(res);
      },
      function(error: any) {
        console.log(error);
        myCallback(mstStatus);
      }
    );
  }

  public updateTertanggungListJson(
    draft_id: number,
    tertanggungs: Tertanggung[],
    myCallback: (result: any) => void
  ): void {
    if (window.cordova == undefined) {
      myCallback(null);
      return;
    }

    let param = [JSON.stringify(tertanggungs), draft_id, this.credentialsService.credentials.username];

    let query = 'UPDATE TRX_SUBMISSION SET tertanggungListJson = ? WHERE draft_id  = ? AND agentCode = ? ';
    this.conn.database.executeSql(
      query,
      param,
      function(res: any) {
        myCallback(mstStatus);
        console.log(res);
      },
      function(error: any) {
        console.log(error);
        myCallback(mstStatus);
      }
    );
  }

  public updateIdAndSubmissionStatusJson(
    draft_id: number,
    id: number,
    mstStatus: MstStatus,
    myCallback: (result: any) => void
  ): void {
    if (window.cordova == undefined) {
      myCallback(null);
      return;
    }

    let param = [id, JSON.stringify(mstStatus), draft_id, this.credentialsService.credentials.username];

    let query = 'UPDATE TRX_SUBMISSION SET id = ?, submissionStatusJson = ? WHERE draft_id = ? AND agentCode = ? ';
    this.conn.database.executeSql(
      query,
      param,
      function(res: any) {
        myCallback(mstStatus);
        console.log(res);
      },
      function(error: any) {
        console.log(error);
        myCallback(mstStatus);
      }
    );
  }

  public updateSignature(
    draft_id: number,
    agentSignBase64: string,
    customerSignBase64: string,
    myCallback: (result: any) => void
  ): void {
    if (window.cordova == undefined) {
      myCallback(null);
      return;
    }
    let param = [agentSignBase64, customerSignBase64, draft_id, this.credentialsService.credentials.username];
    let query =
      'UPDATE TRX_SUBMISSION SET agent_sign_base64 = ?, customer_sign_base64 = ? WHERE draft_id = ? AND agentCode = ? ';
    this.conn.database.executeSql(
      query,
      param,
      function(res: any) {
        myCallback(mstStatus);
        console.log(res);
      },
      function(error: any) {
        console.log(error);
        myCallback(mstStatus);
      }
    );
  }

  public updateSignatureAndEsubmission(
    draft_id: number,
    agentSignBase64: string,
    customerSignBase64: string,
    esubmission: Esubmission,
    myCallback: (result: any) => void
  ): void {
    if (window.cordova == undefined) {
      myCallback(null);
      return;
    }
    let param = [
      agentSignBase64,
      customerSignBase64,
      JSON.stringify(esubmission),
      draft_id,
      this.credentialsService.credentials.username
    ];
    let query =
      'UPDATE TRX_SUBMISSION SET agent_sign_base64 = ?, customer_sign_base64 = ?, json = ? WHERE draft_id = ? AND agentCode = ? ';
    this.conn.database.executeSql(
      query,
      param,
      function(res: any) {
        myCallback(mstStatus);
        console.log(res);
      },
      function(error: any) {
        console.log(error);
        myCallback(mstStatus);
      }
    );
  }

  public getAgentSignatureBase64(draft_id: number, myCallback: (result: any) => void): void {
    if (window.cordova == undefined) {
      myCallback(null);
      return;
    }

    this.openProgress();
    var query = 'SELECT agent_sign_base64 FROM TRX_SUBMISSION WHERE draft_id = ? AND agentCode = ? ';
    let param = [draft_id, this.credentialsService.credentials.username];
    this.conn.database.executeSql(
      query,
      param,
      (res: any) => {
        if (res.rows.length > 0) {
          let signature = res.rows.item(0);
          myCallback(signature.agent_sign_base64);
        }
        this.closeProgress();
      },
      (error: any) => {
        console.error('AGENT SIGNATURE BASE 64 ' + error);
        this.closeProgress();
      },
      () => {
        console.log('FINAL');
        this.closeProgress();
      }
    );
  }
  public getCustomerSignatureBase64(draft_id: number, myCallback: (result: any) => void): void {
    if (window.cordova == undefined) {
      myCallback(null);
      return;
    }

    this.openProgress();
    var query = 'SELECT customer_sign_base64 FROM TRX_SUBMISSION WHERE draft_id = ? AND agentCode = ? ';
    let param = [draft_id, this.credentialsService.credentials.username];
    this.conn.database.executeSql(
      query,
      param,
      (res: any) => {
        if (res.rows.length > 0) {
          let signature = res.rows.item(0);
          console.log('SIGNATURE ' + JSON.stringify(signature));
          myCallback(signature.customer_sign_base64);
        }
        this.closeProgress();
      },
      (error: any) => {
        console.error('CUSTOMER SIGNATURE 64 ' + error);
        this.closeProgress();
      },
      () => {
        console.log('FINAL');
        this.closeProgress();
      }
    );
  }

  public getKtpBase64(draft_id: number, myCallback: (result: any) => void): void {
    if (window.cordova == undefined) {
      myCallback(null);
      return;
    }

    this.openProgress();
    var query = 'SELECT ktp_base64 FROM TRX_SUBMISSION WHERE draft_id = ? AND agentCode = ? ';
    let param = [draft_id, this.credentialsService.credentials.username];
    this.conn.database.executeSql(
      query,
      param,
      (res: any) => {
        if (res.rows.length > 0) {
          let signature = res.rows.item(0);
          console.log('ktp_base64 ' + JSON.stringify(signature));
          myCallback(signature.ktp_base64);
        }
        this.closeProgress();
      },
      (error: any) => {
        console.error('KTP BASE 64 ' + error);
        this.closeProgress();
      },
      () => {
        console.log('FINAL');
        this.closeProgress();
      }
    );
  }

  public deleteEsubmission(draft_id: number, myCallback: (result: any) => void): void {
    var query = 'DELETE FROM TRX_SUBMISSION WHERE draft_id = ? AND agentCode = ? ';

    this.conn.database.executeSql(
      query,
      [draft_id, this.credentialsService.credentials.username],
      (res: any) => {
        localStorage.removeItem(draft_id.toString());
        myCallback(res);
      },
      (error: any) => {
        console.error(error);
      }
    );
  }
  openProgress(): void {
    if (this.progressDialogRef == null) {
      this.progressDialogRef = this.progressDialog.open(LoaderComponent, {
        data: {}
      });
      this.progressDialogRef.afterClosed().subscribe((result: any) => {
        console.log('The dialog was closed');
      });
    }
  }

  closeProgress() {
    if (this.progressDialogRef) {
      this.progressDialog.closeAll();
      this.progressDialogRef = null;
    }
  }
}
