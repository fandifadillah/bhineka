import { Injectable } from '@angular/core';
import { TrxSubmission } from '@app/model/esub/transaction';
import { Filter } from '@app/model/filter/Filter';

@Injectable({
  providedIn: 'root'
})
export class FilterService {
  filter: Filter;

  constructor() {}

  getFilter(): Filter {
    return this.filter;
  }
  setFilter(filter: Filter): void {
    this.filter = filter;
  }
}
