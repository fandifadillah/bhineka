import { Injectable } from '@angular/core';
import { TrxSubmission } from '@app/model/esub/transaction';
import { MstApplication } from '@app/model/esub/MstApplication';
import { HttpHeaders } from '@angular/common/http';
import { CHECK_APP_VERSION, X_IBM_CLIENT_ID, X_IBM_CLIENT_SECRET } from '@app/app.config';
import { HttpService } from '@app/core';

@Injectable({
  providedIn: 'root'
})
export class AppVersionService {
  mstApplication: MstApplication;

  constructor(private httpService: HttpService) {}

  setMstApplication(mstApplication: MstApplication): void {
    this.mstApplication = mstApplication;
  }
  getMstApplication(): MstApplication {
    return this.mstApplication;
  }

  checkVersion(myCallback: () => void) {
    let httpHeaders = new HttpHeaders({
      'X-IBM-Client-Id': X_IBM_CLIENT_ID,
      'X-IBM-Client-secret': X_IBM_CLIENT_SECRET
    });

    let httpOptions = {
      headers: httpHeaders
    };
    const $service = this.httpService.get(CHECK_APP_VERSION, httpOptions);
    $service.pipe().subscribe(
      (value: any) => {
        console.log('RESPONSE ' + JSON.stringify(value));
        if (value.status == 200 && value.message == 'no.available.update.found') {
        } else if (value.status == 200) {
          let mstApplicationVersion = value.data;
          this.mstApplication = mstApplicationVersion;
        }
        myCallback();
      },
      error => {
        console.log(error);
        myCallback();
      },
      () => {}
    );
  }
}
