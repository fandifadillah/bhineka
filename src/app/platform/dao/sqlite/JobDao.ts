import { Injectable } from '@angular/core';
import { Conn } from '@app/platform/dao/conn/db.conn';
import { MatDialog } from '@angular/material';
import { LoaderComponent } from '@app/shared';

@Injectable({
  providedIn: 'root'
})
export class JobDao {
  progressDialogRef: any = null;

  constructor(private conn: Conn, public progressDialog: MatDialog) {}

  getJobs(myCallback: (result: any) => void) {
    if (window.cordova == undefined) {
      myCallback(new Array());
      return;
    }
    this.openProgress();
    var query = 'SELECT distinct NAMA_PEKERJAAN FROM MST_JOB ';
    this.conn.database.executeSql(
      query,
      null,
      (res: any) => {
        let jobs: string[] = new Array();
        for (var x = 0; x < res.rows.length; x++) {
          let job = res.rows.item(x).NAMA_PEKERJAAN;
          jobs.push(job);
        }
        myCallback(jobs);
        this.closeProgress();
      },
      (error: any) => {
        console.error(error);
        this.closeProgress();
      },
      () => {
        console.log('FINAL');
        this.closeProgress();
      }
    );
  }
  openProgress(): void {
    if (this.progressDialogRef == null) {
      this.progressDialogRef = this.progressDialog.open(LoaderComponent, {
        data: {}
      });
      this.progressDialogRef.afterClosed().subscribe((result: any) => {
        console.log('The dialog was closed');
      });
    }
  }

  closeProgress() {
    if (this.progressDialogRef) {
      this.progressDialog.closeAll();
      this.progressDialogRef = null;
    }
  }
}
