import { Injectable } from '@angular/core';
import { Conn } from '@app/platform/dao/conn/db.conn';
import { appInformation } from '@app/app.config';
export class Package {
  unit_name: string;
  total_premium: number;
}

@Injectable({
  providedIn: 'root'
})
export class PackageDao {
  constructor(private conn: Conn) {}

  getPackages(productId: number, myCallback: (result: any) => void) {
    if (window.cordova == undefined) {
      myCallback(new Array());
      return;
    }

    var query = 'SELECT unit_name, total_premium FROM MST_PS_PRODUCT_UNITIZING WHERE product_id = ? ';
    var param = [productId];
    this.conn.database.executeSql(
      query,
      param,
      (res: any) => {
        let packages: Package[] = new Array();
        for (var x = 0; x < res.rows.length; x++) {
          let packaze = new Package();
          packaze = res.rows.item(x);
          packages.push(packaze);
        }
        myCallback(packages);
      },
      (error: any) => {
        console.error(error);
      },
      () => {
        console.log('FINAL');
      }
    );
  }
}
