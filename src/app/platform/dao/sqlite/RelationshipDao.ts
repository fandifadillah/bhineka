import { Injectable } from '@angular/core';
import { Conn } from '@app/platform/dao/conn/db.conn';
import { MatDialog } from '@angular/material';
import { LoaderComponent } from '@app/shared';

export class Relationship {
  id: string;
  label: string;
  sortir: string;
  parent: string;
}

@Injectable({
  providedIn: 'root'
})
export class RelationshipDao {
  progressDialogRef: any = null;

  constructor(private conn: Conn, public progressDialog: MatDialog) {}

  getRelationship(sortir: string, parent: string, myCallback: (result: any) => void) {
    if (window.cordova == undefined) {
      myCallback(new Array());
      return;
    }
    this.openProgress();
    var query = 'SELECT distinct id, label, sortir, parent FROM MST_RELATIONSHIP WHERE sortir = ? ';
    if (parent && parent !== '') {
      query = query + ' AND parent = ? ';
    }
    let param: any = null;
    if (parent) {
      param = [sortir, parent];
    } else {
      param = [sortir];
    }
    this.conn.database.executeSql(
      query,
      param,
      (res: any) => {
        let relationships: Relationship[] = new Array();
        for (var x = 0; x < res.rows.length; x++) {
          let relationship = res.rows.item(x);
          relationships.push(relationship);
        }
        myCallback(relationships);
        this.closeProgress();
      },
      (error: any) => {
        console.error(error);
        this.closeProgress();
      },
      () => {
        console.log('FINAL');
        this.closeProgress();
      }
    );
  }

  openProgress(): void {
    if (this.progressDialogRef == null) {
      this.progressDialogRef = this.progressDialog.open(LoaderComponent, {
        data: {}
      });
      this.progressDialogRef.afterClosed().subscribe((result: any) => {
        console.log('The dialog was closed');
      });
    }
  }

  closeProgress() {
    if (this.progressDialogRef) {
      this.progressDialog.closeAll();
      this.progressDialogRef = null;
    }
  }
}
