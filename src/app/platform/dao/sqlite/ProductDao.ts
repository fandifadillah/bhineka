import { Injectable } from '@angular/core';
import { Conn } from '@app/platform/dao/conn/db.conn';
export class Product {
  product_id: number;
  name: string;
  category: string;
}

@Injectable({
  providedIn: 'root'
})
export class ProductDao {
  constructor(private conn: Conn) {}

  getProducts(myCallback: (result: any) => void) {
    if (window.cordova == undefined) {
      myCallback(new Array());
      return;
    }
    var query = 'SELECT product_id, name, category  FROM MST_PS_PRODUCT ';
    this.conn.database.executeSql(
      query,
      null,
      (res: any) => {
        let products: Product[] = new Array();
        for (var x = 0; x < res.rows.length; x++) {
          let product = new Product();
          product = res.rows.item(x);
          products.push(product);
        }
        myCallback(products);
      },
      (error: any) => {
        console.error(error);
      },
      () => {
        console.log('FINAL');
      }
    );
  }
}
