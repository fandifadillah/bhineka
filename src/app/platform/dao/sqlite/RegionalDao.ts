import { Injectable } from '@angular/core';
import { Conn } from '@app/platform/dao/conn/db.conn';
import { MatDialog } from '@angular/material';
import { LoaderComponent } from '@app/shared';

export class Province {
  kd_propinsi: string;
  nama_propinsi: string;
}

export class City {
  kd_kota_1: string;
  nama_kota: string;
}
export class District {
  kd_kecamatan: string;
  nama_kecamatan: string;
}
export class Village {
  kd_kelurahan: string;
  nama_kelurahan: string;
}

@Injectable({
  providedIn: 'root'
})
export class RegionalDao {
  progressDialogRef: any = null;

  constructor(private conn: Conn, public progressDialog: MatDialog) {}

  getProvinces(myCallback: (result: any) => void) {
    if (window.cordova == undefined) {
      myCallback(new Array());
      return;
    }
    this.openProgress();
    var query = 'SELECT distinct KD_PROPINSI_1, NAMA_PROPINSI  FROM MST_REGIONAL ORDER BY NAMA_PROPINSI ASC ';
    this.conn.database.executeSql(query, null);
    this.conn.database.executeSql(
      query,
      null,
      (res: any) => {
        let provinces: Province[] = new Array();
        for (var x = 0; x < res.rows.length; x++) {
          let province = new Province();
          let kdpropinsi1 = res.rows.item(x).KD_PROPINSI_1;
          let namapropinsi = res.rows.item(x).NAMA_PROPINSI;
          province.kd_propinsi = kdpropinsi1;
          province.nama_propinsi = namapropinsi;
          provinces.push(province);
        }
        myCallback(provinces);
        this.closeProgress();
      },
      (error: any) => {
        console.error(error);
        this.closeProgress();
      },
      () => {
        console.log('FINAL');
        this.closeProgress();
      }
    );
  }

  getCities(kd_propinsi: string, myCallback: (result: any) => void) {
    if (window.cordova == undefined) {
      myCallback(new Array());
      return;
    }
    this.openProgress();
    var query =
      'SELECT distinct KD_KOTA_1, NAMA_KOTA  FROM MST_REGIONAL WHERE KD_PROPINSI_1 = ? ORDER BY NAMA_KOTA ASC ';
    let param = [kd_propinsi];
    this.conn.database.executeSql(
      query,
      param,
      (res: any) => {
        let cities: City[] = new Array();
        for (var x = 0; x < res.rows.length; x++) {
          let city = new City();
          let kdkota = res.rows.item(x).KD_KOTA_1;
          let namakota = res.rows.item(x).NAMA_KOTA;
          city.kd_kota_1 = kdkota;
          city.nama_kota = namakota;
          cities.push(city);
        }
        myCallback(cities);
        this.closeProgress();
      },
      (error: any) => {
        console.error(error);
        this.closeProgress();
      },
      () => {
        console.log('FINAL');
        this.closeProgress();
      }
    );
  }

  getDistricts(kd_city: string, myCallback: (result: any) => void) {
    if (window.cordova == undefined) {
      myCallback(new Array());
      return;
    }

    this.openProgress();
    var query =
      'SELECT distinct KD_KECAMATAN_1, NAMA_KECAMATAN  FROM MST_REGIONAL WHERE KD_KOTA_1 = ? ORDER BY NAMA_KECAMATAN ASC ';
    let param = [kd_city];
    this.conn.database.executeSql(
      query,
      param,
      (res: any) => {
        let districts: District[] = new Array();
        for (var x = 0; x < res.rows.length; x++) {
          let district = new District();
          let kdKecamatan = res.rows.item(x).KD_KECAMATAN_1;
          let namaKecamatan = res.rows.item(x).NAMA_KECAMATAN;
          district.kd_kecamatan = kdKecamatan;
          district.nama_kecamatan = namaKecamatan;
          districts.push(district);
        }
        myCallback(districts);
        this.closeProgress();
      },
      (error: any) => {
        console.error(error);
        this.closeProgress();
      },
      () => {
        console.log('FINAL');
        this.closeProgress();
      }
    );
  }

  getVillages(kd_district: string, myCallback: (result: any) => void) {
    if (window.cordova == undefined) {
      myCallback(new Array());
      return;
    }

    this.openProgress();
    var query =
      'SELECT distinct KD_KELURAHAN, NAMA_KELURAHAN  FROM MST_REGIONAL WHERE KD_KECAMATAN_1 = ? ORDER BY NAMA_KELURAHAN ASC ';
    let param = [kd_district];
    this.conn.database.executeSql(
      query,
      param,
      (res: any) => {
        let villages: Village[] = new Array();
        for (var x = 0; x < res.rows.length; x++) {
          let village = new Village();
          let kdKelurahan = res.rows.item(x).KD_KELURAHAN;
          let namaKelurahan = res.rows.item(x).NAMA_KELURAHAN;
          village.kd_kelurahan = kdKelurahan;
          village.nama_kelurahan = namaKelurahan;
          villages.push(village);
        }
        myCallback(villages);
        this.closeProgress();
      },
      (error: any) => {
        console.error(error);
        this.closeProgress();
      },
      () => {
        console.log('FINAL');
        this.closeProgress();
      }
    );
  }

  openProgress(): void {
    if (this.progressDialogRef == null) {
      this.progressDialogRef = this.progressDialog.open(LoaderComponent, {
        data: {}
      });
      this.progressDialogRef.afterClosed().subscribe((result: any) => {
        console.log('The dialog was closed');
      });
    }
  }

  closeProgress() {
    if (this.progressDialogRef) {
      this.progressDialog.closeAll();
      this.progressDialogRef = null;
    }
  }
}
