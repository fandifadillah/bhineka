import { Injectable, EventEmitter } from '@angular/core';
import { appInformation, IMEI, VERSION_CODE, VERSION_NAME } from '../../../app.config';
import { Logger } from '@app/core';

declare var window: any;
declare var AppVersion: any;

var log = new Logger('Conn');

@Injectable({
  providedIn: 'root'
})
export class Conn {
  // public database: any  = sqlitePlugin.openDatabase('lifepos.sqlite.db');

  public database: any;

  constructor() {
    setTimeout(() => {
      if (window.cordova && window.cordova.platformId !== 'browser') {
        let imei = window.device.uuid;
        console.log('got imei: ' + imei);
        localStorage.setItem(IMEI, imei);
        localStorage.setItem(VERSION_NAME, AppVersion.version);
        localStorage.setItem(VERSION_CODE, AppVersion.build);

        // console.log("DEVICE INFO ## "+JSON.stringify(this.deviceDetector.getDeviceInfo()))
        // const isMobile = this.deviceDetector.isMobile();
        // if(isMobile){
        //   let imei = window.cordova.device.uuid
        //   console.log("got imei: " + imei);
        // }

        this.database = window.sqlitePlugin.openDatabase(
          {
            name: 'lifepos.sqlite.db',
            location: 'default',
            androidDatabaseImplementation: 2,
            createFromLocation: 1
          },
          function(database: any) {
            console.log('DATABASE #### ' + JSON.stringify(database));
            // this.replaceDatabase();

            database.transaction(
              function(transaction: any) {
                var executeQuery = 'SELECT app_version FROM MST_APPLICATION WHERE app_version = ? COLLATE NOCASE';
                var param = [appInformation.version];
                log.debug('DB VERSION :: ' + param);
                transaction.executeSql(executeQuery, param, (res: any) => {
                  log.debug('this.database.executeSql');
                  if (res.rows.length === 0) {
                    log.debug('app version tidak sama dengan yg di deploy');
                    this.replaceDatabase();
                  }
                }),
                  function(error: any) {
                    this.replaceDatabase();
                    log.error('executeQuery ERROR: ' + error);
                  },
                  function() {
                    log.error('executeQuery OK');
                  };
              },
              function(error: any) {
                this.replaceDatabase();
                console.error(error);
              },
              function() {
                console.log('Populated database OK');
              }
            );
          }
        );
      }
    }, 2000);
  }

  replaceDatabase() {
    window.sqlitePlugin.deleteDatabase(
      { name: 'lifepos.sqlite.db', location: 'default' },
      (res: any) => {
        //console.log('copy new database..........');
        this.database = window.sqlitePlugin.openDatabase({
          name: 'lifepos.sqlite.db',
          location: 'default',
          androidDatabaseImplementation: 2,
          createFromLocation: 1
        });
        localStorage.clear();
      },
      (error: any) => {
        log.error(error);
        //console.log('delete database ERROR: ' + error.message);
      }
    );
  }
}
