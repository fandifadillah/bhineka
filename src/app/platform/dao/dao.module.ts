import { NgModule, Optional, SkipSelf } from '@angular/core';
import { Conn } from './conn/db.conn';
import { EsubLocalService } from './local/esub-local.service';
import { SECURITY_DAO_ISERVICES } from './module/security/service/index.interface';
import { PROFILE_DAO_ISERVICES } from './module/profile/service/index.interface';
import { ESUB_DAO_ISERVICES } from './module/esub/service/index.interface';
import { DV_DAO_ISERVICES } from './module/dv/dv.export.bridge';
import { LMS_DAO_ISERVICES } from './module/lms/lms.export.bridge';
import { FUM_DAO_ISERVICES } from './module/fum/fum.export.bridge';
import { CRP_DAO_ISERVICES } from './module/crp/crp.export.bridge';
import { FNA_DAO_ISERVICES } from './module/fna/fna.export.bridge';

@NgModule({
  imports: [],
  declarations: [],
  providers: [
    Conn,
    EsubLocalService,
    ...SECURITY_DAO_ISERVICES,
    ...ESUB_DAO_ISERVICES,
    ...DV_DAO_ISERVICES,
    ...LMS_DAO_ISERVICES,
    ...FUM_DAO_ISERVICES,
    ...CRP_DAO_ISERVICES,
    ...FNA_DAO_ISERVICES,
    ...PROFILE_DAO_ISERVICES
  ],
  exports: []
})
export class DaoModule {
  constructor(@Optional() @SkipSelf() parentModule: DaoModule) {
    if (parentModule) {
      throw new Error('DaoModule already loaded; Import in root module only.');
    }
  }
}
