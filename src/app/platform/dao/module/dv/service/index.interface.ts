/*Service Declaration*/
import { Injectable, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

/*transaction*/
import { DvTransactionService } from './transaction/index.transaction.service';

/*transaction*/
@Injectable()
export class DvTransactionDaoIService extends DvTransactionService {
  constructor(public http: HttpClient) {
    super(http);
  }
}

/*Define constant*/

export const DV_DAO_ISERVICES: any[] = [DvTransactionDaoIService];
