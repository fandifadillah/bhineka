import { DvTransactionService } from './dv.transaction.service';

export const DV_TRANSACTION_SERVICE: any[] = [DvTransactionService];

export * from './dv.transaction.service';
