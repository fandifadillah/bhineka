import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ApiService } from '../api.service';

export class DvTransactionService extends ApiService {
  private uploadUrl = this.DV_BASE_URL;

  constructor(public http: HttpClient) {
    super(http);
  }

  public uploadDv(submission: any): any {
    // console.log('SUBMISSION ' + JSON.stringify(submission));
    const fd = new FormData();

    for (const i of submission.esubPolicies[0].document.documents) {
      if (i.file.length > 0) {
        fd.append('file', i.file[0]);
      }
    }
    fd.append('data', JSON.stringify(submission));
    return new Observable(observer => {
      this.updateMultipart(this.uploadUrl, fd, submission.id).subscribe(
        groupPolicy => {
          observer.next(groupPolicy);
          observer.complete();
        },
        err => {
          observer.error(err);
        }
      );
    });
  }

  public getList(): any {
    return new Observable(observer => {
      this.readList(this.uploadUrl).subscribe(
        groupPolicy => {
          observer.next(groupPolicy);
          observer.complete();
        },
        err => {}
      );
    });
  }

  public createIndexing(indexing: any): any {
    return new Observable(observer => {
      this.create(this.DV_BASE_URL + '/indexing', indexing).subscribe(
        next => {
          observer.next(next);
          observer.complete();
        },
        err => {
          observer.next(err);
        }
      );
    });
  }

  public checkIsDVInReview(selectedGroupPolicyId: number): any {
    return new Observable(observer => {
      this.readList(this.uploadUrl).subscribe(
        groupPolicy => {
          observer.next(groupPolicy);
          observer.complete();
        },
        err => {}
      );
    });
  }
}
