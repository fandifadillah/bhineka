import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ApiService } from '../api.service';

export class NotificationTransactionService extends ApiService {
  private uploadUrl = this.NOTIF_BASE_URL;

  constructor(public http: HttpClient) {
    super(http);
  }

  public getList(): any {
    return new Observable(observer => {
      this.readList(this.uploadUrl).subscribe(
        groupPolicy => {
          observer.next(groupPolicy);
          observer.complete();
        },
        err => {}
      );
    });
  }
}
