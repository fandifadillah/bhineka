import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ApiService } from '../api.service';

export class SubmissionTransactionService extends ApiService {
  private uploadUrl = this.ESUB_BASE_URL;

  constructor(public http: HttpClient) {
    super(http);
  }

  public uploadSubmission(submission: any): any {
    const fd = new FormData();

    for (const i of submission.esubPolicies[0].document.documents) {
      if (i.file.length && i.file.length > 0) {
        fd.append('file', i.file[0]);
      } else if (i.file) {
        fd.append('file', i.file, i.fileName);
      }
    }
    const tempSiFile = submission.siFile;
    const tempEsubFile = submission.esubFile;
    if (submission.siFile != null) {
      fd.append('siFile', submission.siFile);
      submission.siFile = null;
    }
    if (submission.esubFile != null) {
      fd.append('esubFile', submission.esubFile);
      submission.esubFile = null;
    }

    fd.append('data', JSON.stringify(submission));
    // console.log(submission);
    return new Observable(observer => {
      this.createMultipart(this.uploadUrl, fd).subscribe(
        groupPolicy => {
          observer.next(groupPolicy);
          observer.complete();
        },
        err => {
          submission.siFile = tempSiFile;
          submission.esubFile = tempEsubFile;
          observer.error(err);
        }
      );
    });
  }
  public editSubmission(submission: any): any {
    // console.log('SUBMISSION ' + JSON.stringify(submission));
    const fd = new FormData();

    for (const i of submission.esubPolicies[0].document.documents) {
      if (i.file.length && i.file.length > 0) {
        fd.append('file', i.file[0]);
      } else if (i.file) {
        fd.append('file', i.file, i.fileName);
      }
    }
    if (submission.siFile != null) {
      fd.append('siFile', submission.siFile);
    }
    if (submission.esubFile != null) {
      fd.append('esubFile', submission.esubFile);
    }

    fd.append('data', JSON.stringify(submission));
    return new Observable(observer => {
      this.updateMultipart(this.uploadUrl, fd, submission.id).subscribe(
        groupPolicy => {
          observer.next(groupPolicy);
          observer.complete();
        },
        err => {
          observer.error(err);
        }
      );
    });
  }

  public getList(agentCode: string): any {
    return new Observable(observer => {
      this.readList(this.uploadUrl + `/${agentCode}/byAgentCode`).subscribe(
        groupPolicy => {
          observer.next(groupPolicy);
          observer.complete();
        },
        err => {
          observer.error(err);
        }
      );
    });
  }

  public getNoVaAndSpaj(): any {
    return new Observable(observer => {
      this.readList(this.DV_BASE_URL + '/indexing').subscribe(
        groupPolicy => {
          observer.next(groupPolicy);
          observer.complete();
        },
        err => {
          observer.error(err);
        }
      );
    });
  }
}
