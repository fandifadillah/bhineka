/*Service Declaration*/
import { Injectable, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

/*transaction*/
import { SubmissionTransactionService, NotificationTransactionService } from './transaction/index.transaction.service';

/*transaction*/
@Injectable()
export class SubmissionTransactionDaoIService extends SubmissionTransactionService {
  constructor(public http: HttpClient) {
    super(http);
  }
}

@Injectable()
export class NotificationTransactionDaoIService extends NotificationTransactionService {
  constructor(public http: HttpClient) {
    super(http);
  }
}

/*Define constant*/

export const ESUB_DAO_ISERVICES: any[] = [SubmissionTransactionDaoIService, NotificationTransactionDaoIService];
