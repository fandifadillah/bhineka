/*Service Declaration*/
import { Injectable, EventEmitter } from '@angular/core';

/*Security*/
import { AgentProfileService } from './security/agent.service';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Conn } from '../profile.import.bridge';

@Injectable()
export class AgentProfileDaoIService extends AgentProfileService {
  constructor(public http: HttpClient, public conn: Conn) {
    super(http, conn);
  }
}

/*Define constant*/

export const PROFILE_DAO_ISERVICES: any[] = [AgentProfileDaoIService];
