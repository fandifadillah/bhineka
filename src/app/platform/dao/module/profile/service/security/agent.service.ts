import { EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';
import { Conn } from '../../profile.import.bridge';
import { catchError } from 'rxjs/operators';

export class AgentProfileService extends ApiService {
  public TABLE_NAME = 'MST_AGENT';
  private agentURL = this.SERVER_URL + '/api/security/login/sso/user/info/';

  constructor(public http: HttpClient, public conn: Conn) {
    super();
  }

  getProfileFromServer(userName: string): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');

    return this.http.get(this.agentURL + userName).pipe(catchError(error => Observable.throw(error || 'Server error')));
  }

  getProfileFromLocal(userName: string): Observable<any> {
    let obs: any;
    return new Observable(observer => {
      obs = observer;
      var executeQuery = 'SELECT * FROM MST_AGENT WHERE UserName = ? COLLATE NOCASE';
      var param = [userName];
      //console.log(" public selectLocal(userName: any): any{ public selectLocal(userName: any): any{ public selectLocal(userName: any): any{");
      this.conn.database.executeSql(executeQuery, param, (res: any) => {
        //console.log('selectLocal: success');
        //console.log(res);
        if (res.rows === undefined || res.rows.item(0) === undefined) {
          obs.next([]);
          obs.complete();
        } else {
          obs.next(res.rows.item(0));
          obs.complete();
        }
        obs.complete();
      }),
        function(error: any) {
          console.log('executeQuery ERROR: ' + error.message);
        },
        function() {
          console.log('executeQuery OK');
        };
    });
  }
}
