import { AppWebServiceUrl } from '../../../../../app.config';

export class ApiService {
  public SERVER_URL = AppWebServiceUrl.GENERAL;
  public AUTH_BASE_URL: string = this.SERVER_URL + '/auth';
  public SECURITY_BASE_URL: string = this.SERVER_URL + '/api/security';

  constructor() {}
}
