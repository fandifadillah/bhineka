/*Service Declaration*/
import { Injectable, EventEmitter } from '@angular/core';
import { Conn } from '../security.import.bridge';

/*Security*/
import { AgentService } from './security/agent.service';
import { AppVersionService } from './security/appVersion.service';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { AgentProfileDaoIService } from '../security.import.bridge';

@Injectable()
export class AgentDaoIService extends AgentService {
  constructor(public http: HttpClient, public conn: Conn, public agentProfileDaoIService: AgentProfileDaoIService) {
    super(http, conn, agentProfileDaoIService);
  }
}

@Injectable()
export class AppInformationDaoIService extends AppVersionService {
  constructor(public http: HttpClient, public conn: Conn) {
    super(http, conn);
  }
}

/*Define constant*/

export const SECURITY_DAO_ISERVICES: any[] = [AgentDaoIService, AppInformationDaoIService];
