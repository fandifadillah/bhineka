import { EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiService } from '../api.service';
import { Conn } from '../../security.import.bridge';
import { appInformation } from '../../../../../../app.config';
import { catchError } from 'rxjs/operators';

export class AppVersionService extends ApiService {
  public TABLE_NAME = 'MST_APPLICATION';

  private appVersionURL = this.SERVER_URL + '/api/security/app/' + appInformation.platform;
  private enableApplication: boolean = true;

  constructor(public http: HttpClient, public conn: Conn) {
    super();
  }

  checkUpdate(): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.get(this.appVersionURL, {}).pipe(catchError(error => Observable.throw(error || 'Server error')));
  }

  setAppplicationNewVersion(parameter: any): Observable<any> {
    //console.log('setAppplicationNewVersion',parameter);

    let obs: any;

    return new Observable(observer => {
      obs = observer;
      var executeQuery = `UPDATE MST_APPLICATION SET
                status=?,
                api=?,
                app_version=?,
                url=?
                WHERE platform=?`;
      var param = [parameter.status, parameter.api, parameter.appVersion, parameter.url, appInformation.platform];
      //console.log(param)
      this.conn.database.executeSql(executeQuery, param, (res: any) => {
        if (res.rows === undefined || res.rows.item(0) === undefined) {
          obs.next([]);
          obs.complete();
        } else {
          obs.next([res.rows.item(0)]);
          obs.complete();
        }
        obs.complete();
      }),
        function(error: any) {
          console.log('executeQuery ERROR: ' + error.message);
        },
        function() {
          console.log('executeQuery OK');
        };
    });
  }

  getAppplicationNewVersion(): Observable<any> {
    let obs: any;

    return new Observable(observer => {
      obs = observer;
      var executeQuery = 'SELECT * FROM MST_APPLICATION WHERE platform = ? COLLATE NOCASE';
      var param = [appInformation.platform];
      this.conn.database.executeSql(executeQuery, param, (res: any) => {
        if (res.rows === undefined || res.rows.item(0) === undefined) {
          obs.next([]);
          obs.complete();
        } else {
          //console.log(JSON.stringify(res.rows.item(0)));
          obs.next(this.convertToAppVersion(res.rows.item(0)));
          obs.complete();
        }
        obs.complete();
      }),
        function(error: any) {
          console.log('executeQuery ERROR: ' + error.message);
        },
        function() {
          console.log('executeQuery OK');
        };
    });
  }

  convertToAppVersion(origin: any) {
    let newVersion: any = {};

    newVersion.id = origin.id;
    newVersion.version = origin.version;
    newVersion.api = origin.api;
    newVersion.appVersion = origin.app_version;
    newVersion.cutoffDate = origin.cutoff_date;
    newVersion.expiryDate = origin.expiry_date;
    newVersion.message = origin.message;
    newVersion.platform = origin.platform;
    newVersion.startDate = origin.start_date;
    newVersion.status = origin.status;
    newVersion.updateBy = origin.update_by;
    newVersion.updateDate = origin.update_date;
    newVersion.url = origin.url;

    return newVersion;
  }
}
