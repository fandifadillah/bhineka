import { EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ApiService } from '../api.service';
import { Conn } from '../../security.import.bridge';
import { AgentProfileDaoIService } from '../../security.import.bridge';
import { catchError } from 'rxjs/operators';

export class AgentService extends ApiService {
  public TABLE_NAME = 'MST_AGENT';
  private agentURL = this.SERVER_URL + '/api/security/login/sso/internal';

  constructor(public http: HttpClient, public conn: Conn, public agentProfileDaoIService: AgentProfileDaoIService) {
    super();
  }

  login3(loginModel: any): Observable<any> {
    return new Observable(observer => {
      let result: any = {};
      result.status = true;
      result.token = 'mobile';
      observer.next(result);
      observer.complete();
    });
  }

  login(loginModel: any): Observable<any> {
    let obs: any;
    return new Observable(observer => {
      obs = observer;
      let result: any = {};
      /*check local first */
      this.selectLocal(loginModel.name).subscribe((agents: any) => {
        //console.log("selectselectselectselectselectselectselectselect");
        //console.log(agents);

        if (agents.length === 0) {
          this.validateRemote(loginModel).subscribe(
            result => {
              this.agentProfileDaoIService.getProfileFromServer(loginModel.name).subscribe(agentProfile => {
                //console.log("this.agentProfileDaoIService.getProfileFromServer(loginModel.name).subscribe(result=>{");
                //console.log(agentProfile);
                let agent: any = {};
                agent.UserName = loginModel.name;
                agent.AgentCode = agentProfile.AgentCode;
                agent.UserName = agentProfile.UserName;
                agent.AgentName = agentProfile.AgentName;
                // agent.AgentLevelID
                // agent.AgentEmail
                // agent.AgentPassword
                // agent.AgentPasswordDefault
                // agent.isActived
                // agent.isBanned
                // agent.AgentGender
                // agent.AgentStatus
                // agent.AgentDOB
                agent.ContractEffectiveDate = agentProfile.ContractEffectiveDate;
                agent.AgentLicensedNumber = agentProfile.AgentLicensedNumber;
                agent.AgentLicensedEffectiveDate = agentProfile.AgentLicensedEffectiveDate;
                agent.AgentLicensedExpiredDate = agentProfile.AgentLicensedExpiredDate;
                // agent.TerminationDate
                // agent.AgencyName
                // agent.regionname
                // agent.RecruiterCode
                // agent.RecruiterName
                // agent.npwp
                // agent.npwp_entry_date
                // agent.npwp_address1
                // agent.ptkp_code
                // agent.business_line_name
                // agent.channel_name
                // agent.audit_agent
                // agent.userstatus
                // agent.updateAgentCode
                // agent.createByDate
                agent.passwordMD5 = loginModel.password;

                this.createLocal(agent).subscribe((agent: any) => {
                  result.status = true;
                  result.token = 'mobile';
                  observer.next(result);
                  observer.complete();
                });
              });
            },
            err => {
              console.log(err);
              observer.error(err || 'Server error');
            }
          );
          // result.status=false;
          // result.error = {id: 'WRONG_USERNAME'};
          // observer.error(result || 'Server error');
          // observer.complete();
        } else {
          let agent = agents[0];

          if (agent.passwordMD5 === loginModel.password) {
            result.status = true;
            result.token = 'mobile';
            observer.next(result);
            observer.complete();
          } else {
            result.status = false;
            result.error = { id: 'WRONG_PASSWORD' };
            observer.error(result || 'Server error');
          }
        }
      });
    });
  }

  validateRemote(loginModel: any): Observable<any> {
    let bodyString = JSON.stringify(loginModel);
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http
      .post(this.agentURL, bodyString, { responseType: 'json', headers })
      .pipe(catchError(error => Observable.throw(error || 'Server error')));
  }

  public createLocal(agent: any): any {
    return new Observable(observer => {
      // this.conn.database.transaction(txn => {
      var executeQuery = `INSERT INTO MST_AGENT 
                (
                    UserName,
                    AgentCode,
                    AgentName,
                    ContractEffectiveDate,
                    AgentLicensedNumber,
                    AgentLicensedEffectiveDate,
                    AgentLicensedExpiredDate,
                    passwordMD5
                )                 
                VALUES (?,?,?,?,?,?,?,?)`;
      var param = [
        agent.UserName,
        agent.AgentCode,
        agent.AgentName,
        agent.ContractEffectiveDate,
        agent.AgentLicensedNumber,
        agent.AgentLicensedEffectiveDate,
        agent.AgentLicensedExpiredDate,
        agent.passwordMD5
      ];
      //console.log(param)
      this.conn.database.executeSql(executeQuery, param, (res: any) => {
        //console.log('res.insertId: res.insertIdres.insertId');
        //console.log(res);
        //console.log(res.insertId);

        // this.conn.database.transaction(txn => {
        var executeQuery = 'SELECT * FROM MST_AGENT WHERE id=?';
        var param = [res.insertId];
        this.conn.database.executeSql(executeQuery, param, (res: any) => {
          console.log('select createLocal agent: success');
          //console.log(JSON.stringify(res.rows.item(0)));
          observer.next([res.rows.item(0)]);
          observer.complete();
        }),
          function(error: any) {
            console.log(error);
          };
        // })
      }),
        function(error: any) {
          console.log(error);
        };
      // })
    });
  }

  public selectLocal(userName: any): any {
    let obs: any;
    let test: string = 'liehian';
    return new Observable(observer => {
      obs = observer;
      // this.conn.database.transaction(txn => {
      var executeQuery = 'SELECT * FROM MST_AGENT WHERE UserName = ? COLLATE NOCASE';
      var param = [userName];
      //console.log(" public selectLocal(userName: any): any{ public selectLocal(userName: any): any{ public selectLocal(userName: any): any{");
      //console.log(test);
      this.conn.database.executeSql(executeQuery, param, (res: any) => {
        //console.log('selectLocal: success');
        if (res.rows === undefined || res.rows.item(0) === undefined) {
          obs.next([]);
          obs.complete();
        } else {
          obs.next([res.rows.item(0)]);
          obs.complete();
        }
        obs.complete();
      }),
        function(error: any) {
          console.log('executeQuery ERROR: ' + error.message);
        },
        function() {
          console.log('executeQuery OK');
        };
      // }),
      // function(error) {
      // console.log('Transaction ERROR: ' + error.message);
      // }, function() {
      // console.log('Transaction OK');
      // };
    });
  }

  public validateLocalLogin(userName: any, password: any): any {
    return new Observable(observer => {
      // this.conn.database.transaction(txn => {
      var executeQuery = 'SELECT * FROM MST_AGENT WHERE UserName = ? COLLATE NOCASE';
      var param = [userName];
      this.conn.database.executeSql(executeQuery, param, (res: any) => {
        //console.log('validateLocalLogin: success');
        //console.log(JSON.stringify(res.rows.item(0)));
        if (res.rows.item(0) === undefined) {
          observer.next([]);
        } else {
          observer.next([res.rows.item(0)]);
        }
        observer.complete();
      });
      // })
    });
  }
}
