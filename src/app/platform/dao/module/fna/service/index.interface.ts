/*Service Declaration*/
import { Injectable, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

/*transaction*/
import { FnaHistoryTransactionService } from './transaction/index.transaction.service';

/*transaction*/
@Injectable()
export class FnaHistoryTransactionDaoIService extends FnaHistoryTransactionService {
  constructor(public http: HttpClient) {
    super(http);
  }
}

/*Define constant*/

export const FNA_DAO_ISERVICES: any[] = [FnaHistoryTransactionDaoIService];
