import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AppWebServiceUrl } from '../../../../../app.config';
import { catchError } from 'rxjs/operators';

export class ApiService {
  public SERVER_URL = AppWebServiceUrl.GENERAL;
  public TRANSACTION_BASE_URL: string = this.SERVER_URL + '/api/fna/transaction';

  constructor(public http: HttpClient) {}

  readList(url: string): Observable<any> {
    return this.http
      .get(url, { withCredentials: true })
      .pipe(catchError(error => Observable.throw(error || 'Server error')));
  }

  create(url: string, body: any): Observable<any> {
    let bodyString = JSON.stringify(body);
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http
      .post(url, bodyString, { withCredentials: true, responseType: 'json', headers })
      .pipe(catchError(error => Observable.throw(error || 'Server error')));
  }

  read(url: string, id: number) {
    return this.http
      .get(`${url}/${id.toString()}`, { withCredentials: true })
      .pipe(catchError(error => Observable.throw(error || 'Server error')));
  }

  update(url: string, body: any) {
    let bodyString = JSON.stringify(body); // Stringify payload
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http
      .put(`${url}/${body['id']}`, bodyString, { withCredentials: true, responseType: 'json', headers })
      .pipe(catchError(error => Observable.throw(error || 'Server error')));
  }

  delete(url: string, id: number) {
    return this.http
      .delete(`${url}/${id.toString()}`, { withCredentials: true })
      .pipe(catchError(error => Observable.throw(error || 'Server error')));
  }

  post(url: string, body: any): Observable<any> {
    let bodyString = JSON.stringify(body);
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http
      .post(url, bodyString, { withCredentials: true, responseType: 'json', headers })
      .pipe(catchError(error => Observable.throw(error || 'Server error')));
  }

  post2(url: string, body: any): Observable<any> {
    let bodyString = JSON.stringify(body);
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http
      .post(url, bodyString, { withCredentials: true, responseType: 'json', headers })
      .pipe(catchError(error => Observable.throw(error || 'Server error')));
  }
}
