import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ApiService } from '../api.service';

export class FnaHistoryTransactionService extends ApiService {
  private saveUrl = this.TRANSACTION_BASE_URL + '/fnaHistory';

  constructor(public http: HttpClient) {
    super(http);
  }

  public createFnaHistory(fnaHistory: any): any {
    return new Observable(observer => {
      this.create(this.saveUrl, fnaHistory).subscribe(
        fnaHistory => {
          observer.next(this._deserialize(fnaHistory));
          observer.complete();
        },
        err => {}
      );
    });
  }

  /*render and configurator*/

  public listFnaHistory(userName: string): Observable<any[]> {
    return new Observable(observer => {
      this.readList(this.saveUrl).subscribe(
        fnaHistorys => {
          observer.next(fnaHistorys);
          observer.complete();
        },
        err => {}
      );
    });
  }

  public deleteFnaHistory(id: number): Observable<any> {
    return new Observable(observer => {
      this.delete(this.saveUrl, id).subscribe(
        fnaHistorys => {
          observer.next(null);
          observer.complete();
        },
        err => {}
      );
    });
  }

  public deserialize(illustrations: any[]): any[] {
    let fnaHistorys: any[] = new Array<any>();
    for (let i = 0; i < illustrations.length; i++) {
      fnaHistorys.push({});
      fnaHistorys[i] = this._deserialize(illustrations[i]);
    }
    return fnaHistorys;
  }

  public _deserialize(illustrations: any): any {
    let fnaHistorys: any = {};
    fnaHistorys.id = illustrations.id;
    fnaHistorys.agentCode = illustrations.agentCode;
    fnaHistorys.agentName = illustrations.agentName;
    fnaHistorys.groupPolicyId = illustrations.groupPolicyId;
    fnaHistorys.createDate = illustrations.createDate;
    fnaHistorys.policys = JSON.parse(illustrations.policys);
    fnaHistorys.agentCode = illustrations.agentCode;
    fnaHistorys.agentCode = illustrations.agentCode;
    return fnaHistorys;
  }
}
