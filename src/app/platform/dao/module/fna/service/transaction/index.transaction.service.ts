import { FnaHistoryTransactionService } from './fnaHistory.transaction.service';

export const DV_TRANSACTION_SERVICE: any[] = [FnaHistoryTransactionService];

export * from './fnaHistory.transaction.service';
