/*Service Declaration*/
import { Injectable, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

/*transaction*/
import { IssueMasterTransactionService } from './transaction/index.transaction.service';

import { IssueHistoryTransactionService } from './transaction/index.transaction.service';

/*transaction*/
@Injectable()
export class IssueMasterTransactionDaoIService extends IssueMasterTransactionService {
  constructor(public http: HttpClient) {
    super(http);
  }
}

@Injectable()
export class IssueHistoryTransactionDaoIService extends IssueHistoryTransactionService {
  constructor(public http: HttpClient) {
    super(http);
  }
}

/*Define constant*/

export const FUM_DAO_ISERVICES: any[] = [IssueMasterTransactionDaoIService, IssueHistoryTransactionDaoIService];
