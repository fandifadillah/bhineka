import { IssueMasterTransactionService } from './issueMaster.transaction.service';
import { IssueHistoryTransactionService } from './issueHistory.transaction.service';

export const DV_TRANSACTION_SERVICE: any[] = [IssueMasterTransactionService, IssueHistoryTransactionService];

export * from './issueMaster.transaction.service';
export * from './issueHistory.transaction.service';
