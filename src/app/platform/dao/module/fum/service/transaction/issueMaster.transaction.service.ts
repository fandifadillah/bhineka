import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ApiService } from '../api.service';

export class IssueMasterTransactionService extends ApiService {
  private saveUrl = this.TRANSACTION_BASE_URL + '/issueMaster';

  constructor(public http: HttpClient) {
    super(http);
  }

  public createIssueMaster(issueMaster: any): any {
    return new Observable(observer => {
      this.create(this.saveUrl, issueMaster).subscribe(
        issueMaster => {
          observer.next(this._deserialize(issueMaster));
          observer.complete();
        },
        err => {}
      );
    });
  }

  /*render and configurator*/

  public listIssueMaster(userName: string): Observable<any[]> {
    return new Observable(observer => {
      this.readList(this.saveUrl).subscribe(
        issueMasters => {
          observer.next(issueMasters);
          observer.complete();
        },
        err => {}
      );
    });
  }

  public deleteIssueMaster(id: number): Observable<any> {
    return new Observable(observer => {
      this.delete(this.saveUrl, id).subscribe(
        issueMasters => {
          observer.next(null);
          observer.complete();
        },
        err => {}
      );
    });
  }

  public deserialize(illustrations: any[]): any[] {
    let issueMasters: any[] = new Array<any>();
    for (let i = 0; i < illustrations.length; i++) {
      issueMasters.push({});
      issueMasters[i] = this._deserialize(illustrations[i]);
    }
    return issueMasters;
  }

  public _deserialize(illustrations: any): any {
    let issueMasters: any = {};
    issueMasters.id = illustrations.id;
    issueMasters.agentCode = illustrations.agentCode;
    issueMasters.agentName = illustrations.agentName;
    issueMasters.groupPolicyId = illustrations.groupPolicyId;
    issueMasters.createDate = illustrations.createDate;
    issueMasters.policys = JSON.parse(illustrations.policys);
    issueMasters.agentCode = illustrations.agentCode;
    issueMasters.agentCode = illustrations.agentCode;
    return issueMasters;
  }
}
