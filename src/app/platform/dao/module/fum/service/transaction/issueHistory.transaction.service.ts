import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ApiService } from '../api.service';

export class IssueHistoryTransactionService extends ApiService {
  private saveUrl = this.TRANSACTION_BASE_URL + '/issueHistory';

  constructor(public http: HttpClient) {
    super(http);
  }

  public createIssueHistory(issueHistory: any): any {
    return new Observable(observer => {
      this.create(this.saveUrl, issueHistory).subscribe(
        issueHistory => {
          observer.next(this._deserialize(issueHistory));
          observer.complete();
        },
        err => {}
      );
    });
  }

  /*render and configurator*/

  public listIssueHistory(userName: string): Observable<any[]> {
    return new Observable(observer => {
      this.readList(this.saveUrl).subscribe(
        issueHistorys => {
          observer.next(issueHistorys);
          observer.complete();
        },
        err => {}
      );
    });
  }

  public deleteIssueHistory(id: number): Observable<any> {
    return new Observable(observer => {
      this.delete(this.saveUrl, id).subscribe(
        issueHistorys => {
          observer.next(null);
          observer.complete();
        },
        err => {}
      );
    });
  }

  public deserialize(illustrations: any[]): any[] {
    let issueHistorys: any[] = new Array<any>();
    for (let i = 0; i < illustrations.length; i++) {
      issueHistorys.push({});
      issueHistorys[i] = this._deserialize(illustrations[i]);
    }
    return issueHistorys;
  }

  public _deserialize(illustrations: any): any {
    let issueHistorys: any = {};
    issueHistorys.id = illustrations.id;
    issueHistorys.agentCode = illustrations.agentCode;
    issueHistorys.agentName = illustrations.agentName;
    issueHistorys.groupPolicyId = illustrations.groupPolicyId;
    issueHistorys.createDate = illustrations.createDate;
    issueHistorys.policys = JSON.parse(illustrations.policys);
    issueHistorys.agentCode = illustrations.agentCode;
    issueHistorys.agentCode = illustrations.agentCode;
    return issueHistorys;
  }
}
