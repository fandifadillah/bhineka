/*Service Declaration*/
import { Injectable, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

/*transaction*/
import { CrpHistoryTransactionService } from './transaction/index.transaction.service';

/*transaction*/
@Injectable()
export class CrpHistoryTransactionDaoIService extends CrpHistoryTransactionService {
  constructor(public http: HttpClient) {
    super(http);
  }
}

/*Define constant*/

export const CRP_DAO_ISERVICES: any[] = [CrpHistoryTransactionDaoIService];
