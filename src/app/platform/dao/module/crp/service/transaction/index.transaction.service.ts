import { CrpHistoryTransactionService } from './crpHistory.transaction.service';

export const DV_TRANSACTION_SERVICE: any[] = [CrpHistoryTransactionService];

export * from './crpHistory.transaction.service';
