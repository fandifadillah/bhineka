import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ApiService } from '../api.service';

export class CrpHistoryTransactionService extends ApiService {
  private saveUrl = this.TRANSACTION_BASE_URL + '/crpHistory';

  constructor(public http: HttpClient) {
    super(http);
  }

  public createCrpHistory(crpHistory: any): any {
    return new Observable(observer => {
      this.create(this.saveUrl, crpHistory).subscribe(
        crpHistory => {
          observer.next(this._deserialize(crpHistory));
          observer.complete();
        },
        err => {}
      );
    });
  }

  /*render and configurator*/

  public listCrpHistory(userName: string): Observable<any[]> {
    return new Observable(observer => {
      this.readList(this.saveUrl).subscribe(
        crpHistorys => {
          observer.next(crpHistorys);
          observer.complete();
        },
        err => {}
      );
    });
  }

  public deleteCrpHistory(id: number): Observable<any> {
    return new Observable(observer => {
      this.delete(this.saveUrl, id).subscribe(
        crpHistorys => {
          observer.next(null);
          observer.complete();
        },
        err => {}
      );
    });
  }

  public deserialize(illustrations: any[]): any[] {
    let crpHistorys: any[] = new Array<any>();
    for (let i = 0; i < illustrations.length; i++) {
      crpHistorys.push({});
      crpHistorys[i] = this._deserialize(illustrations[i]);
    }
    return crpHistorys;
  }

  public _deserialize(illustrations: any): any {
    let crpHistorys: any = {};
    crpHistorys.id = illustrations.id;
    crpHistorys.agentCode = illustrations.agentCode;
    crpHistorys.agentName = illustrations.agentName;
    crpHistorys.groupPolicyId = illustrations.groupPolicyId;
    crpHistorys.createDate = illustrations.createDate;
    crpHistorys.policys = JSON.parse(illustrations.policys);
    crpHistorys.agentCode = illustrations.agentCode;
    crpHistorys.agentCode = illustrations.agentCode;
    return crpHistorys;
  }
}
