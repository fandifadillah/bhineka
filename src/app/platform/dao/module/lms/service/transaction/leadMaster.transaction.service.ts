import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ApiService } from '../api.service';

export class LeadMasterTransactionService extends ApiService {
  private saveUrl = this.TRANSACTION_BASE_URL + '/leadMaster';

  constructor(public http: HttpClient) {
    super(http);
  }

  public createLeadMaster(leadMaster: any): any {
    return new Observable(observer => {
      this.create(this.saveUrl, leadMaster).subscribe(
        leadMaster => {
          observer.next(this._deserialize(leadMaster));
          observer.complete();
        },
        err => {}
      );
    });
  }

  /*render and configurator*/

  public listLeadMaster(userName: string): Observable<any[]> {
    return new Observable(observer => {
      this.readList(this.saveUrl).subscribe(
        leadMasters => {
          observer.next(leadMasters);
          observer.complete();
        },
        err => {}
      );
    });
  }

  public deleteLeadMaster(id: number): Observable<any> {
    return new Observable(observer => {
      this.delete(this.saveUrl, id).subscribe(
        leadMasters => {
          observer.next(null);
          observer.complete();
        },
        err => {}
      );
    });
  }

  public deserialize(illustrations: any[]): any[] {
    let leadMasters: any[] = new Array<any>();
    for (let i = 0; i < illustrations.length; i++) {
      leadMasters.push({});
      leadMasters[i] = this._deserialize(illustrations[i]);
    }
    return leadMasters;
  }

  public _deserialize(illustrations: any): any {
    let leadMasters: any = {};
    leadMasters.id = illustrations.id;
    leadMasters.agentCode = illustrations.agentCode;
    leadMasters.agentName = illustrations.agentName;
    leadMasters.groupPolicyId = illustrations.groupPolicyId;
    leadMasters.createDate = illustrations.createDate;
    leadMasters.policys = JSON.parse(illustrations.policys);
    leadMasters.agentCode = illustrations.agentCode;
    leadMasters.agentCode = illustrations.agentCode;
    return leadMasters;
  }
}
