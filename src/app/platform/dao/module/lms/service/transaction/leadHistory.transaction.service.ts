import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ApiService } from '../api.service';

export class LeadHistoryTransactionService extends ApiService {
  private saveUrl = this.TRANSACTION_BASE_URL + '/leadHistory';

  constructor(public http: HttpClient) {
    super(http);
  }

  public createLeadHistory(leadHistory: any): any {
    return new Observable(observer => {
      this.create(this.saveUrl, leadHistory).subscribe(
        leadHistory => {
          observer.next(this._deserialize(leadHistory));
          observer.complete();
        },
        err => {}
      );
    });
  }

  /*render and configurator*/

  public listLeadHistory(userName: string): Observable<any[]> {
    return new Observable(observer => {
      this.readList(this.saveUrl).subscribe(
        leadHistorys => {
          observer.next(leadHistorys);
          observer.complete();
        },
        err => {}
      );
    });
  }

  public deleteLeadHistory(id: number): Observable<any> {
    return new Observable(observer => {
      this.delete(this.saveUrl, id).subscribe(
        leadHistorys => {
          observer.next(null);
          observer.complete();
        },
        err => {}
      );
    });
  }

  public deserialize(illustrations: any[]): any[] {
    let leadHistorys: any[] = new Array<any>();
    for (let i = 0; i < illustrations.length; i++) {
      leadHistorys.push({});
      leadHistorys[i] = this._deserialize(illustrations[i]);
    }
    return leadHistorys;
  }

  public _deserialize(illustrations: any): any {
    let leadHistorys: any = {};
    leadHistorys.id = illustrations.id;
    leadHistorys.agentCode = illustrations.agentCode;
    leadHistorys.agentName = illustrations.agentName;
    leadHistorys.groupPolicyId = illustrations.groupPolicyId;
    leadHistorys.createDate = illustrations.createDate;
    leadHistorys.policys = JSON.parse(illustrations.policys);
    leadHistorys.agentCode = illustrations.agentCode;
    leadHistorys.agentCode = illustrations.agentCode;
    return leadHistorys;
  }
}
