import { LeadMasterTransactionService } from './leadMaster.transaction.service';
import { LeadHistoryTransactionService } from './leadHistory.transaction.service';

export const DV_TRANSACTION_SERVICE: any[] = [LeadMasterTransactionService, LeadHistoryTransactionService];

export * from './leadMaster.transaction.service';
export * from './leadHistory.transaction.service';
