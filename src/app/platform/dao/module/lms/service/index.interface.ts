/*Service Declaration*/
import { Injectable, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

/*transaction*/
import { LeadMasterTransactionService } from './transaction/index.transaction.service';

import { LeadHistoryTransactionService } from './transaction/index.transaction.service';

/*transaction*/
@Injectable()
export class LeadMasterTransactionDaoIService extends LeadMasterTransactionService {
  constructor(public http: HttpClient) {
    super(http);
  }
}

@Injectable()
export class LeadHistoryTransactionDaoIService extends LeadHistoryTransactionService {
  constructor(public http: HttpClient) {
    super(http);
  }
}

/*Define constant*/

export const LMS_DAO_ISERVICES: any[] = [LeadMasterTransactionDaoIService, LeadHistoryTransactionDaoIService];
