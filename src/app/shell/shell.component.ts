import { Title } from '@angular/platform-browser';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MediaObserver } from '@angular/flex-layout';

import { Credentials, CredentialsService, I18nService } from '@app/core';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { HomeEmitterService } from '@app/home/service/home-emitter.service';
import { ShellEmitterService } from '@app/shell/service/shell-emitter.service';
import { Conn } from '@app/platform/dao/conn/db.conn';
import { closeApp } from '@app/app.component';
import * as moment from 'moment';
import { VERSION_NAME } from '@app/app.config';
import { ConfirmComponent } from '@app/shared/dialog/confirm/confirm.component';
import { MatDialog, MatSidenav } from '@angular/material';

@Component({
  selector: 'app-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.scss']
})
export class ShellComponent implements OnInit {
  @ViewChild('sidenav', { static: false }) sideNav: MatSidenav;
  // @Output() searchEventEmitter = new EventEmitter();

  moment = moment;
  credentials: Credentials;

  sideNavOpen: boolean = true;

  constructor(
    private homeEmitterService: HomeEmitterService,
    private shellEmitterService: ShellEmitterService,
    private router: Router,
    private titleService: Title,
    private media: MediaObserver,
    private authenticationService: AuthenticationService,
    private credentialsService: CredentialsService,
    private conn: Conn,
    private i18nService: I18nService,
    public dialog: MatDialog
  ) {
    this.credentials = this.credentialsService.credentials;
    document.addEventListener('backbutton', closeApp, false);
  }

  ngOnInit() {
    if (this.shellEmitterService.subsVar === undefined) {
      this.shellEmitterService.subsVar = this.shellEmitterService.shellEmitter.subscribe(() => {
        this.onClickPayment();
      });
    }
  }

  setLanguage(language: string) {
    this.i18nService.language = language;
  }

  logout() {
    this.openConfirmDialog('Konfirmasi', 'Apakah anda ingin keluar dari aplikasi');
  }

  get username(): string | null {
    const credentials = this.credentialsService.credentials;
    return credentials ? credentials.username : null;
  }

  get languages(): string[] {
    return this.i18nService.supportedLanguages;
  }

  get isMobile(): boolean {
    return this.media.isActive('xs') || this.media.isActive('sm');
  }

  get title(): string {
    return this.titleService.getTitle();
  }

  onClickSearch() {
    this.homeEmitterService.onClickSearch();
  }

  onClickFilter() {
    // this.homeEmitterService.onClickSearch()
    // this.searchEventEmitter.emit(null)
    // this.homeComponent.toggleSearch();
    document.removeEventListener('backbutton', closeApp, false);
    this.router.navigate(['/home/filter'], { replaceUrl: false });
  }

  onClickPayment() {
    document.removeEventListener('backbutton', closeApp, false);
    this.router.navigate(['/payment'], { replaceUrl: false });
  }

  versionName() {
    return localStorage.getItem(VERSION_NAME);
  }

  openConfirmDialog(title: string, message: string): void {
    const dialogRef = this.dialog.open(ConfirmComponent, {
      width: '80%',
      data: { title: title, message: message }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.toString() === 'OK') {
        this.authenticationService.logout().subscribe(() => this.router.navigate(['/login'], { replaceUrl: true }));
      }
      console.log('The dialog was closed');
    });
  }

  getAgentName() {
    let jLogin: any = this.credentialsService.credentials.jLogin;
    let coreLogin: string = jLogin.coreLogin;
    let object = JSON.parse(coreLogin);
    return object.user.name;
  }
}
