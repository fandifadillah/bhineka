import { Injectable, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';

@Injectable({
  providedIn: 'root'
})
export class ShellEmitterService {
  shellEmitter = new EventEmitter();
  subsVar: Subscription;

  constructor() {}

  onClickPayment() {
    this.shellEmitter.emit();
  }
}
