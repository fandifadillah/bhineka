import { HttpHeaders } from '@angular/common/http';

export var APPLICATION_VERSION = 1;
export var ANDROID = 'ANDROID';
export var IOS = 'IOS';
export var PLATFORM = ANDROID;
export var IMEI = 'IMEI';
export var EMAIL = 'EE';
export var SMS = 'ES';
export var VERSION_NAME = 'VERSION_NAME';
export var VERSION_CODE = 'VERSION_CODE';

export var ESUBMISSIONS = 'ESUBMISSIONS';

export var KPLKGA = 'KPLKGA';
export var SUAMI = 'SUAMI';
export var ISTRI = 'ISTRI';
export var ANAK = 'ANAK';

export var X_IBM_CLIENT_ID = 'd0984904-e2c8-4785-b412-4d504089c2d3';
export var X_IBM_CLIENT_SECRET = 'Y0sQ1jA7pR4uJ3nO2vC7gW2xM1rW3jY8uH2gU8rJ6tS4lD8kG8';

export let httpHeaders = new HttpHeaders({
  'X-IBM-Client-Id': X_IBM_CLIENT_ID,
  'X-IBM-Client-secret': X_IBM_CLIENT_SECRET
});

export let defaultHttpOptions = {
  headers: httpHeaders
};

export var BASE_URL = 'http://192.168.8.101:8071/api';

export var LOGIN_URL = 'https://api.au-syd.apiconnect.appdomain.cloud/bhinnekalife-dev/esubdev/esub/insura/login';
// export var LOGIN_URL = BASE_URL+"/login";

export var SUBMIT_ESUB =
  'https://api.au-syd.apiconnect.appdomain.cloud/bhinnekalife-dev/esubdev/esub/insura/trxSubmission/save';
// export var SUBMIT_ESUB = BASE_URL+"/trxSubmission/save";

export var SUBMIT_PAYMENT =
  'https://api.au-syd.apiconnect.appdomain.cloud/bhinnekalife-dev/esubdev/esub/insura/trxSubmission/payment';

export var CHECK_APP_VERSION =
  'https://api.au-syd.apiconnect.appdomain.cloud/bhinnekalife-dev/esubdev/esub/insura/mstApplication/activeVersion';

export var APK_STORE = 'https://drive.google.com/open?id=1kkmiaQwDUc1II-wixrORgeIZuuK1QnJF';

// export var SUBMIT_PAYMENT = BASE_URL+"/trxSubmission/payment";

export const AppWebServiceUrl = Object.freeze({
  GENERAL: BASE_URL
});

// export const dbConnStringDefault = Object.freeze({
//   client: 'sql-server',
//   connection: {
//     host : '127.0.0.1',
//     user : 'liehianok',
//     password : 'liehianok',
//     database : 'LIFEPOS'
//   }
// });

export const appInformation = Object.freeze({
  api: 24,
  platform: 'android',
  version: 'AND-1.0.0',
  expiryDate: '31-03-2020'
});
