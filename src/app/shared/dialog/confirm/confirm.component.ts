import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { DialogData } from '@app/shared/dialog/alert/alert.component';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<ConfirmComponent>, @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  ngOnInit() {}

  onClick(): void {
    this.dialogRef.close();
  }

  onClickOk(): void {
    this.dialogRef.close('OK');
  }
  onClickCancel(): void {
    this.dialogRef.close('CANCEL');
  }
}
