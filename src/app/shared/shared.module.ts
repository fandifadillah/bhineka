import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MaterialModule } from '@app/material.module';
import { LoaderComponent } from './loader/loader.component';
import { AlertComponent } from './dialog/alert/alert.component';
import { MatDialogModule } from '@angular/material';
import { ConfirmComponent } from './dialog/confirm/confirm.component';

@NgModule({
  imports: [FlexLayoutModule, MaterialModule, CommonModule, MatDialogModule],
  declarations: [LoaderComponent, AlertComponent, ConfirmComponent],
  entryComponents: [LoaderComponent, AlertComponent, ConfirmComponent],
  exports: [LoaderComponent, AlertComponent, ConfirmComponent]
})
export class SharedModule {}
