import { Component, NgZone, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { merge } from 'rxjs';
import { filter, map, switchMap } from 'rxjs/operators';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { Conn } from '@app/platform/dao/conn/db.conn';

import { environment } from '@env/environment';
import { I18nService, Logger, untilDestroyed } from '@app/core';
import { EsubLocalService } from '@app/platform/dao/local/esub-local.service';
import { AppVersionService } from '@app/platform/dao/local/AppVersionService';

const log = new Logger('App');

export const closeApp = function() {
  if ((<any>navigator).app) {
    (<any>navigator).app.exitApp();
  } else if ((<any>navigator).device) {
    (<any>navigator).device.exitApp();
  } else {
    window.close();
  }
};

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private titleService: Title,
    private translateService: TranslateService,
    private zone: NgZone,
    private keyboard: Keyboard,
    private statusBar: StatusBar,
    private splashScreen: SplashScreen,
    private i18nService: I18nService,
    private conn: Conn,
    private esubService: EsubLocalService,
    private appVersionService: AppVersionService
  ) {}

  async ngOnInit() {
    if (environment.production) {
      Logger.enableProductionMode();
    }

    log.debug('init');

    // Setup translations
    this.i18nService.init(environment.defaultLanguage, environment.supportedLanguages);

    const onNavigationEnd = this.router.events.pipe(filter(event => event instanceof NavigationEnd));

    // Change page title on navigation or language change, based on route data
    merge(this.translateService.onLangChange, onNavigationEnd)
      .pipe(
        map(() => {
          let route = this.activatedRoute;
          while (route.firstChild) {
            route = route.firstChild;
          }
          return route;
        }),
        filter(route => route.outlet === 'primary'),
        switchMap(route => route.data),
        untilDestroyed(this)
      )
      .subscribe(event => {
        const title = event.title;
        if (title) {
          this.titleService.setTitle(this.translateService.instant(title));
        }
      });
    document.addEventListener(
      'deviceready',
      () => {
        this.zone.run(() => this.onCordovaReady());
      },
      false
    );
    this.appVersionService.checkVersion(() => {});
  }
  ngOnDestroy() {
    this.i18nService.destroy();
  }

  private onCordovaReady() {
    log.debug('cordova ready');

    if (window.cordova) {
      log.debug('Cordova init');
      this.keyboard.hideFormAccessoryBar(true);
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    }
  }

  // openDialog(title: string, message: string, myCallback: () => void): void {
  //   const dialogRef = this.dialog.open(AlertComponent, {
  //     width: '80%',
  //     data: { title: title, message: message }
  //   });
  //
  //   dialogRef.afterClosed().subscribe(result => {
  //     console.log('The dialog was closed');
  //     if (myCallback) {
  //       myCallback();
  //     }
  //   });
  // }
}
