import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { environment } from '@env/environment';
import { I18nService, Logger } from '@app/core';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { Utility } from '@app/utils/utility.service';
import { Conn } from '@app/platform/dao/conn/db.conn';
import { APK_STORE, VERSION_CODE, VERSION_NAME } from '@app/app.config';
import { MatDialog } from '@angular/material';
import { AlertComponent } from '@app/shared/dialog/alert/alert.component';
import { AppVersionService } from '@app/platform/dao/local/AppVersionService';

const log = new Logger('Login');

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {
  version: string | null = environment.version;
  error: string | undefined;
  loginForm!: FormGroup;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private i18nService: I18nService,
    private authenticationService: AuthenticationService,
    public dialog: MatDialog,
    private conn: Conn,
    private utility: Utility,
    private appVersionService: AppVersionService
  ) {
    this.createForm();
  }

  ngOnInit() {
    if (this.appVersionService.getMstApplication()) {
      let mstApplication = this.appVersionService.getMstApplication();
      if (mstApplication.version > parseInt(localStorage.getItem(VERSION_CODE).toString())) {
        this.openDialog('Pembaruan', 'Terdapat pembaharuan aplikasi mohon lakukan update terlebih dahulu', () => {
          window.open(APK_STORE, '_system');
        });
      }
    }
  }

  ngOnDestroy() {}

  login() {
    var data = this.loginForm.value;
    this.authenticationService.auth(data, (value: any, valid: boolean) => {
      if (valid) {
        this.loginForm.markAsPristine();
        this.router.navigateByUrl('/', { replaceUrl: true });
      }
    });
  }

  setLanguage(language: string) {
    this.i18nService.language = language;
  }

  get currentLanguage(): string {
    return this.i18nService.language;
  }

  get languages(): string[] {
    return this.i18nService.supportedLanguages;
  }

  private createForm() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.compose([Validators.required, Validators.minLength(3)])],
      remember: true
    });
  }

  versionName() {
    return localStorage.getItem(VERSION_NAME);
  }

  onClickForgetPassword() {
    this.openDialog(
      'Lupa Kata Kunci',
      'Jika Anda lupa kata kunci, silakan ubah kata kunci Anda pada aplikasi' +
        " <a href='https://blat-dev.bhinnekalife.com/forgotpassword/reset-password'>BLAT</a>",
      () => {}
    );
  }

  // openDialog(title: string, message: string): void {
  //   const dialogRef = this.dialog.open(AlertComponent, {
  //     width: '80%',
  //     data: { title: title, message: message }
  //   });
  //
  //   dialogRef.afterClosed().subscribe(result => {
  //     console.log('The dialog was closed');
  //   });
  // }

  openDialog(title: string, message: string, myCallback: () => void): void {
    const dialogRef = this.dialog.open(AlertComponent, {
      disableClose: true,
      width: '80%',
      data: { title: title, message: message }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (myCallback) {
        myCallback();
      }
    });
  }
}
