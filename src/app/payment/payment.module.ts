import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';

import { CoreModule } from '@app/core';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/material.module';
import { PaymentRoutingModule } from '@app/payment/payment-routing.module';
import { PaymentComponent } from '@app/payment/payment.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    CoreModule,
    SharedModule,
    FlexLayoutModule,
    MaterialModule,
    PaymentRoutingModule
  ],
  declarations: [PaymentComponent]
})
export class PaymentModule {}
