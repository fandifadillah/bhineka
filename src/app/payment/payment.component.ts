import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { EsubLocalService } from '@app/platform/dao/local/esub-local.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {
  Esubmission,
  MstStatus,
  PAID_ID,
  PAID_LABEL,
  PaymentData,
  SUBMITTED_ID,
  SUBMITTED_LABEL,
  TrxSubmission
} from '@app/model/esub/transaction';
import { BundleService } from '@app/platform/dao/local/BundleService';
import { CredentialsService, HttpService } from '@app/core';
import { MatDialog } from '@angular/material';
import { AlertComponent } from '@app/shared/dialog/alert/alert.component';
import { LoaderComponent } from '@app/shared';
import { SUBMIT_PAYMENT, X_IBM_CLIENT_ID, X_IBM_CLIENT_SECRET } from '@app/app.config';
import { HttpHeaders } from '@angular/common/http';
import imageCompression from 'browser-image-compression';
import { AuthenticationService } from '@app/core/authentication/authentication.service';

const compressOptions = {
  maxSizeMB: 0.5, // (default: Number.POSITIVE_INFINITY)
  maxWidthOrHeight: 500, // compressedFile will scale down by ratio to a point that width or height is smaller than maxWidthOrHeight (default: undefined)
  useWebWorker: true, // optional, use multi-thread web worker, fallback to run in main-thread (default: true)
  maxIteration: 10 // optional, max number of iteration to compress the image (default: 10)
};

var options: any = {
  quality: 90,
  destinationType: 0,
  sourceType: 1,
  encodingType: 0,
  mediaType: 0,
  allowEdit: true,
  correctOrientation: true
};

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {
  public paymentForm: FormGroup;

  trxSubmission: TrxSubmission;
  esubmission: Esubmission = new Esubmission();
  paymentData: PaymentData;

  expandDisclaimer1Mode = false;

  transferMode: boolean = true;
  public imageSrc: any;

  progressDialogRef: any;

  constructor(
    private location: Location,
    private router: Router,
    private esubLocalService: EsubLocalService,
    private bundleService: BundleService,
    private formBuilder: FormBuilder,
    private httpService: HttpService,
    public dialog: MatDialog,
    public progressDialog: MatDialog,
    public credentialService: CredentialsService,
    private authenticationService: AuthenticationService
  ) {
    this.imageSrc = null;
    if (this.trxSubmission == undefined) {
      this.trxSubmission = this.bundleService.getData();
      this.esubmission = this.trxSubmission.esubmission;
      if (this.trxSubmission.paymentData) {
        this.paymentData = this.trxSubmission.paymentData;
      } else {
        this.paymentData = new PaymentData();
      }
    }
    this.createForm();
  }

  ngOnInit() {}

  onClickBack() {
    this.location.back();
  }

  async onClickSubmit() {
    console.log('Payment Data == ' + JSON.stringify(this.paymentData));
    if (!this.transferMode) {
      if (!this.imageSrc) {
        this.openDialog('Warning', 'Mohon lengkapi foto pembayaran', () => {});
        return;
      }
      if (!this.paymentData.kode_setor) {
        this.openDialog('Warning', 'Mohon lengkapi kode setor', () => {});
        return;
      }
    }

    this.paymentData.no_ktp = this.trxSubmission.spajNo;
    this.paymentData.metode_pembayaran = this.transferMode ? 1 : 2;
    this.paymentData.payment_date = new Date();

    this.trxSubmission.paymentData = this.paymentData;
    console.log('TO BE Send Payment Data == ' + JSON.stringify(this.paymentData));

    this.openProgress();
    let httpHeaders = new HttpHeaders({
      'X-IBM-Client-Id': X_IBM_CLIENT_ID,
      'X-IBM-Client-secret': X_IBM_CLIENT_SECRET,
      Authorization: this.credentialService.credentials.authorization
    });

    let httpOptions = {
      headers: httpHeaders
    };

    var formData = new FormData();
    formData.append('esubId', this.trxSubmission.id.toString());
    formData.append('paymentMethod', this.paymentData.metode_pembayaran.toString());
    formData.append('kodeSetor', this.paymentData.kode_setor);
    if (this.imageSrc) {
      let attachmentFile = await imageCompression.getFilefromDataUrl(this.imageSrc);
      console.log(`attachmentFile size ${attachmentFile.size / 1024 / 1024} MB`); // smaller than maxSizeMB
      const compressedFile = await imageCompression(attachmentFile, compressOptions);
      console.log('compressedFile instanceof Blob', compressedFile instanceof Blob); // true
      console.log(`compressedFile size ${compressedFile.size / 1024 / 1024} MB`); // smaller than maxSizeMB

      let compressedFileBase64 = await imageCompression.getDataUrlFromFile(compressedFile);
      formData.append('attachmentBase64', compressedFileBase64);
    }

    const $service = this.httpService.post(SUBMIT_PAYMENT, formData, httpOptions);
    $service.pipe().subscribe(
      (value: any) => {
        this.closeProgress();
        if (value.status == 200) {
          this.trxSubmission.submissionStatus = new MstStatus();
          this.trxSubmission.submissionStatus.id = PAID_ID;
          this.trxSubmission.submissionStatus.label = PAID_LABEL;
          this.trxSubmission.submissionStatusJson = JSON.stringify(this.trxSubmission.submissionStatus);
          this.esubLocalService.updateSubmissionStatusJson(
            this.trxSubmission.draft_id,
            this.trxSubmission.submissionStatus,
            result => {}
          );
          this.openDialog('Sukses', 'Submit Payment Berhasil', () => {
            this.router.navigate(['/'], { replaceUrl: true });
            this.router.dispose();
          });
        } else if (value.status == 500) {
          this.openDialog('Session expired', 'Anda harus login kembali untuk melanjutkan aktifitas', () => {
            this.authenticationService.logout().subscribe(() => this.router.navigate(['/login'], { replaceUrl: true }));
          });
        } else {
          this.openDialog('Submit Gagal', value.data.message, () => {});
        }
      },
      error => {
        console.log(error);
        this.closeProgress();
        if (error.status == 0) {
          this.openDialog('Network Problem', 'Mohon periksa koneksi internet anda', () => {});
        } else if (error.status == 500 && error.error.moreInformation === 'Failed to establish a backside connection') {
          this.openDialog('Connection Timeout', "Server doesn't reply, please try again later", () => {});
        } else if (error.status == 401 && error.error.message === 'token.has.expired') {
          this.openDialog('Session expired', 'Anda harus login kembali untuk melanjutkan aktifitas', () => {
            this.authenticationService.logout().subscribe(() => this.router.navigate(['/login'], { replaceUrl: true }));
          });
        } else {
          this.openDialog('Submit Pembayaran Gagal', error.error.message, () => {});
        }
      },
      () => {}
    );

    // this.router.navigate(['/esub/preview-brosur'], { replaceUrl: false });
  }

  toggleTransfer() {
    this.expandDisclaimer1Mode = false;
    this.transferMode = true;
  }

  toggleGoPay() {
    this.expandDisclaimer1Mode = true;
    this.transferMode = false;
  }

  onClickCamera() {
    (<any>window).navigator.camera.getPicture(
      (cameraSuccess: any) => {
        this.imageSrc = 'data:image/jpeg;base64,' + cameraSuccess; //(this.sanitizer.bypassSecurityTrustUrl(cameraSuccess) as any);
        console.log('CAMERA RETURN ' + this.imageSrc);
        const imageElement = document.getElementById('receiptImage') as HTMLImageElement;
        imageElement.src = this.imageSrc;
        // document.getElementById("ktpImage").src = this.imageSrc
        // this.convertFileUriToInternalURL(cameraSuccess);
      },
      (error: any) => {},
      options
    );
  }

  private createForm() {
    this.paymentForm = this.formBuilder.group({
      agree: [false, Validators.required]
    });
  }

  openDialog(title: string, message: string, myCallback: () => void): void {
    const dialogRef = this.dialog.open(AlertComponent, {
      width: '80%',
      data: { title: title, message: message }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (myCallback) {
        myCallback();
      }
    });
  }

  openProgress(): void {
    this.progressDialogRef = this.progressDialog.open(LoaderComponent, {
      width: '20%',
      data: {}
    });
    this.progressDialogRef.disableClose = true;

    this.progressDialogRef.afterClosed().subscribe((result: any) => {
      console.log('The dialog was closed');
    });
  }

  closeProgress() {
    if (this.progressDialogRef) {
      this.progressDialog.closeAll();
    }
  }
}
