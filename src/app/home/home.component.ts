import { ChangeDetectorRef, Component, NgZone, OnInit, ViewChild } from '@angular/core';
import { QuoteService } from './quote.service';
import { HomeEmitterService } from '@app/home/service/home-emitter.service';
import { Router } from '@angular/router';
import { ShellEmitterService } from '@app/shell/service/shell-emitter.service';
import { EsubLocalService } from '@app/platform/dao/local/esub-local.service';
import { DRAFT_ID, PAID_ID, SUBMITTED_ID, TrxSubmission } from '@app/model/esub/transaction';
import { DatePipe } from '@angular/common';
import { MatDialog } from '@angular/material';
import { LoaderComponent } from '@app/shared';
import { BundleService } from '@app/platform/dao/local/BundleService';
import { FilterService } from '@app/platform/dao/local/FilterService';
import { closeApp } from '@app/app.component';
import { CredentialsService } from '@app/core';
import { ConfirmComponent } from '@app/shared/dialog/confirm/confirm.component';
import { AuthenticationService } from '@app/core/authentication/authentication.service';
import { AppVersionService } from '@app/platform/dao/local/AppVersionService';
import { AlertComponent } from '@app/shared/dialog/alert/alert.component';
import { APK_STORE, VERSION_CODE } from '@app/app.config';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  @ViewChild('searchInput', { static: false }) searchInput: HTMLInputElement;

  quote: string | undefined;
  isLoading = false;
  searchMode = false;
  progressDialogRef: any = null;

  public trxSubmissions: TrxSubmission[] = new Array();
  trxSubmission: TrxSubmission;

  constructor(
    private router: Router,
    private homeEmitterService: HomeEmitterService,
    private shellEmitterService: ShellEmitterService,
    private quoteService: QuoteService,
    private esubLocalService: EsubLocalService,
    private bundleService: BundleService,
    public progressDialog: MatDialog,
    public filterService: FilterService,
    public credentialService: CredentialsService,
    public deleteConfirmdialog: MatDialog,
    public dialog: MatDialog,
    private authenticationService: AuthenticationService,
    private appVersionService: AppVersionService
  ) {}

  ngOnInit() {
    this.renderData();
    setTimeout(() => {
      if (this.appVersionService.getMstApplication()) {
        let mstApplication = this.appVersionService.getMstApplication();
        if (mstApplication.version > parseInt(localStorage.getItem(VERSION_CODE).toString())) {
          this.openDialog('Pembaruan', 'Terdapat pembaharuan aplikasi mohon lakukan update terlebih dahulu', () => {
            window.open(APK_STORE, '_system');
          });
        }
      }
    }, 4000);
  }

  renderData() {
    this.openProgress();
    setTimeout(() => {
      this.closeProgress();
      this.search('');
    }, 2000);
    this.homeEmitterService.listen(() => {
      this.toggleSearch();
    });
  }

  toggleSearch() {
    this.searchMode = !this.searchMode;
  }

  onKeywordEnter(text: string) {
    this.search(text);
    this.searchInput.value = '';
    this.toggleSearch();
  }

  onSearch(text: string) {
    if (!text || text.length === 0) {
      this.search(text);
      this.toggleSearch();
    }
  }

  search(text: string) {
    this.esubLocalService.getEsubmissions(text, this.filterService.getFilter(), result => {
      this.trxSubmissions = result;
    });
  }
  isSubmitted(trxSubmission: TrxSubmission) {
    if (trxSubmission.submissionStatus && trxSubmission.submissionStatus.id == SUBMITTED_ID) {
      return true;
    }
    return false;
  }

  isDraft(trxSubmission: TrxSubmission) {
    if (trxSubmission.submissionStatus && trxSubmission.submissionStatus.id == DRAFT_ID) {
      return true;
    }
    return false;
  }

  isPaid(trxSubmission: TrxSubmission) {
    if (trxSubmission.submissionStatus && trxSubmission.submissionStatus.id == PAID_ID) {
      return true;
    }
    return false;
  }

  isOtherStatus(trxSubmission: TrxSubmission) {
    if (!this.isSubmitted(trxSubmission) && !this.isDraft(trxSubmission) && !this.isPaid(trxSubmission)) {
      return true;
    }
    return false;
  }

  onClickFab() {
    let trxSubmission = new TrxSubmission();
    trxSubmission.draft_id = new Date().getTime();
    trxSubmission.agentName = this.credentialService.credentials.username;
    trxSubmission.agentCode = this.credentialService.credentials.username;

    this.bundleService.setData(trxSubmission);
    document.removeEventListener('backbutton', closeApp, false);
    this.router.navigate(['/esub/select-product'], { replaceUrl: false });
  }

  onClickView(trxSubmission: TrxSubmission) {
    this.openProgress();
    this.esubLocalService.getCustomerSignatureBase64(trxSubmission.draft_id, result => {
      let esignCustomerBase64: string = result;
      trxSubmission.customer_sign_base64 = esignCustomerBase64;
      setTimeout(() => {
        this.esubLocalService.getAgentSignatureBase64(trxSubmission.draft_id, result => {
          this.closeProgress();
          let esignAgentBase64: string = result;
          trxSubmission.agent_sign_base64 = esignAgentBase64;
          this.bundleService.setData(trxSubmission);
          document.removeEventListener('backbutton', closeApp, false);
          this.router.navigate(['/esub/preview-brosur'], { replaceUrl: false });
        });
      }, 1000);
    });
  }

  onClickRemove(trxSubmission: TrxSubmission) {
    this.trxSubmission = trxSubmission;
    this.openConfirmDialog('Konfirmasi', 'Apakah anda akan menghapus esubmission ini');
    // this.esubLocalService.deleteEsubmission(trxSubmission.spajNo);
    // this.search('');
  }

  onClickEdit(trxSubmission: TrxSubmission) {
    this.bundleService.setData(trxSubmission);
    document.removeEventListener('backbutton', closeApp, false);
    this.router.navigate(['/esub/select-product'], {
      replaceUrl: false
    });
  }
  onClickPayment(trxSubmission: TrxSubmission) {
    this.bundleService.setData(trxSubmission);
    document.removeEventListener('backbutton', closeApp, false);
    this.router.navigate(['/payment'], { replaceUrl: false });
  }

  created(trxSubmission: TrxSubmission) {
    return DatePipe.prototype.transform(trxSubmission.created, 'yyyy-MM-dd');
  }

  openProgress(): void {
    if (this.progressDialogRef == null) {
      this.progressDialogRef = this.progressDialog.open(LoaderComponent, {
        data: {}
      });
      this.progressDialogRef.afterClosed().subscribe((result: any) => {
        console.log('The dialog was closed');
      });
    }
  }

  closeProgress() {
    if (this.progressDialogRef) {
      this.progressDialog.closeAll();
      this.progressDialogRef = null;
    }
  }

  logout() {
    setTimeout(() => {
      this.openConfirmDialog('Konfirmasi', 'Apakah anda yakin ingin keluar \n dari aplikasi ini?');
    }, 1000);
  }

  openConfirmDialog(title: string, message: string): void {
    const dialogRef = this.deleteConfirmdialog.open(ConfirmComponent, {
      width: '80%',
      data: { title: title, message: message }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result.toString() === 'OK') {
        if (this.trxSubmission) {
          this.esubLocalService.deleteEsubmission(this.trxSubmission.draft_id, result1 => {});
          this.search('');
        }
      }
      this.trxSubmission = null;
      console.log('The dialog was closed');
    });
  }

  openDialog(title: string, message: string, myCallback: () => void): void {
    const dialogRef = this.dialog.open(AlertComponent, {
      disableClose: true,
      width: '80%',
      data: { title: title, message: message }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (myCallback) {
        myCallback();
      }
    });
  }
}
