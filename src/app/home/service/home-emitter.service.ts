import { Injectable, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';

@Injectable({
  providedIn: 'root'
})
export class HomeEmitterService {
  eventEmitter = new EventEmitter();
  subsVar: Subscription;

  myCallback: any;

  constructor() {}

  onClickSearch() {
    this.eventEmitter.emit();
    if (this.myCallback) {
      this.myCallback();
    }
  }

  listen(myCallback: () => void) {
    this.myCallback = myCallback;
  }
}
