import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Product, ProductDao } from '@app/platform/dao/sqlite/ProductDao';
import { LoaderComponent } from '@app/shared';
import { DateAdapter, MAT_DATE_FORMATS, MatDialog } from '@angular/material';
import { DRAFT_ID, DRAFT_LABEL, PAID_ID, PAID_LABEL, SUBMITTED_ID, SUBMITTED_LABEL } from '@app/model/esub/transaction';
import { FilterService } from '@app/platform/dao/local/FilterService';
import { Filter } from '@app/model/filter/Filter';
import { APP_DATE_FORMATS, AppDateAdapter } from '@app/utils/format-datepicker';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: AppDateAdapter },
    { provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS }
  ]
})
export class FilterComponent implements OnInit {
  progressDialogRef: any = null;

  products: Product[];
  filter: Filter;

  polisFieldFilters: any[] = [
    {
      id: 1,
      name: 'Tanggal entri'
    },
    {
      id: 2,
      name: 'Nama Pemegang Polis'
    },
    {
      id: 3,
      name: 'No KTP'
    }
  ];

  directions: any[] = [
    {
      id: 1,
      name: 'Descending'
    },
    {
      id: 2,
      name: 'Ascending'
    }
  ];

  spajStatuses: any[] = [
    { id: 0, name: '--Semua--' },
    { id: DRAFT_ID, name: DRAFT_LABEL },
    { id: SUBMITTED_ID, name: SUBMITTED_LABEL },
    { id: PAID_ID, name: PAID_LABEL }
  ];

  constructor(
    private location: Location,
    private productDao: ProductDao,
    public progressDialog: MatDialog,
    public filterService: FilterService
  ) {
    this.filter = filterService.getFilter() ? filterService.getFilter() : new Filter();

    this.openProgress();
    this.productDao.getProducts(result => {
      this.products = new Array();
      let product = new Product();
      product.product_id = 0;
      product.name = '--Semua--';
      this.products.push(product);
      this.products = result;
      this.closeProgress();
    });
  }

  ngOnInit() {}

  onClickBack() {
    this.location.back();
  }

  openProgress(): void {
    if (this.progressDialogRef == null) {
      this.progressDialogRef = this.progressDialog.open(LoaderComponent, {
        data: {}
      });
      this.progressDialogRef.afterClosed().subscribe((result: any) => {
        console.log('The dialog was closed');
      });
    }
  }

  closeProgress() {
    if (this.progressDialogRef) {
      this.progressDialog.closeAll();
      this.progressDialogRef = null;
    }
  }

  onClickSave() {
    let startDate = this.filter.startDate;
    if (startDate) {
      startDate.setSeconds(1);
      startDate.setMinutes(0);
      startDate.setHours(0);
      this.filter.startDate = startDate;
    }
    let endDate = this.filter.endDate;
    if (endDate) {
      endDate.setSeconds(59);
      endDate.setMinutes(59);
      endDate.setHours(23);
      this.filter.endDate = endDate;
    }
    this.filterService.setFilter(this.filter);
    this.location.back();
  }
}
