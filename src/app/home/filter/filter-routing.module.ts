import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { extract } from '@app/core';
import { FilterComponent } from '@app/home/filter/filter.component';

const routes: Routes = [{ path: 'home/filter', component: FilterComponent, data: { title: extract('Filter') } }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class FilterRoutingModule {}
