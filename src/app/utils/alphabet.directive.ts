import { Directive, HostListener, ElementRef, Input } from '@angular/core';
@Directive({
  selector: '[onlyAlphabet]'
})
export class AlphabetDirective {
  regexStr = '^[a-zA-Z ]*$';
  // @Input() onlyAlphabet: boolean;

  constructor(private el: ElementRef) {}

  @HostListener('keydown', ['$event']) onKeyDown(e: KeyboardEvent): boolean {
    return e.keyCode === 8 || e.keyCode === 32 || e.keyCode === 37 || e.keyCode === 39
      ? true
      : new RegExp('^[a-zA-Z_]*$').test(e.key);
  }

  @HostListener('input', ['$event'])
  onInput(event: any) {
    return new RegExp(/[^A-Za-z ]/g).test(event.key);
  }

  @HostListener('paste', ['$event'])
  blockPaste(event: KeyboardEvent) {
    alert('c');
    this.validateFields(event);
  }

  validateFields(event: any) {
    setTimeout(() => {
      alert(this.el.nativeElement.value);
      this.el.nativeElement.value = this.el.nativeElement.value.replace(/[^A-Za-z ]/g, '').replace(/\s/g, '');
      event.preventDefault();
    }, 1);
  }
}
