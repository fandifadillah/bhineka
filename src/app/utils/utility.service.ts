'use strict';
import { Injectable } from '@angular/core';

import * as CryptoJS from 'crypto-js';

@Injectable({
  providedIn: 'root'
})
export class Utility {
  key: string =
    'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoemx7cG0BzHSoj8fWppQNffUQP9GlppvSPIB89pDwVV6Sb2vVe38CYYgf9stKZzWgRxCAGF7yrPTPh/YB+BN' +
    '9yK+UefZGhFhvxifVkNbfgfVM/BV79IyHrUiMhAN19P1MNApQlqlKAtJ839jfB18QFKSRcR3Ycs7+cjJTpSVfuShNxNyDQIn7P+BSolH79wxLei9Cj5yysfgGvFNkayEw5ChmFF4kp+Vp' +
    'R2YPgaiMXrNeKHxoGWPDJwjuyM9WC/740ZL5bz85uvSBFYAwS9lfSKFgs+Pv01DnHH+kf8sznJAmG9H8If6TAgQdVHSbN8aCErOgJArClEjCd21cf+iewIDAQAB';

  constructor() {}

  encryptStringWithRsaPrivateKey(toEncrypt: string) {
    var message = CryptoJS.AES.encrypt(toEncrypt, this.key);
    return message.toString();
  }

  decryptStringWithRsaPublicKey(toDecrypt: string) {
    var code = CryptoJS.AES.decrypt(toDecrypt, this.key);
    var decryptedMessage = code.toString(CryptoJS.enc.Utf8);
    return decryptedMessage;
  }

  monthsDiff(d1: any, d2: any) {
    let date1 = new Date(d1);
    let date2 = new Date(d2);
    let years = this.yearsDiff(d1, d2);
    let months = years * 12 + (date2.getMonth() - date1.getMonth());
    return months;
  }
  yearsDiff(d1: any, d2: any) {
    let date1 = new Date(d1);
    let date2 = new Date(d2);
    let yearsDiff = date2.getFullYear() - date1.getFullYear();
    return yearsDiff;
  }
}
