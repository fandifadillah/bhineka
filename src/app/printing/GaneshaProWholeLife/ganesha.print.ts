import * as _ from 'lodash';
import { AsuransiPitraYadnyaPrintService } from '@app/printing/esub/asuransiPitraYadnya.print';

export class GaneshaProWholeLifePrintService extends AsuransiPitraYadnyaPrintService {
  constructor() {
    super('ganesha');
  }
}
