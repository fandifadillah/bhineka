import * as momentImported from 'moment';

const moment = momentImported;

export class CommonWordingPrintService {
  private _lang: string;
  private _en: any = {
    header: {
      title: 'Product Illustration',
      companyName: 'PT. INSURA',
      proposalNo: 'Proposal No',
      proposalTerm: 'THIS PROPOSAL MERELY USED AS ILLUSTRATION AND NOT AS INSURANCE CONTRACT'
    },
    footer: {
      providedBy: 'Create By',
      providedDate: 'Create Date',
      agentSign: 'Agent Signature',
      phSign: 'Policy Holder Signature',
      agentStatement: 'I have explain illustration content to policy holder',
      clientStatement: 'I have understand this illustration',
      page: 'Page',
      to: 'from',
      programVersion: 'Program Version: '
    },
    common: {},
    param: {
      currNotation: {
        IDR: 'Rp.',
        USD: 'US$.'
      },
      currency: {
        IDR: 'Rupiah',
        USD: 'Dollar'
      },
      sexes: {
        M: 'Male',
        F: 'Female'
      },
      maritalStatuses: {
        S: 'Single',
        M: 'Married',
        D: 'Divorce'
      },
      smokers: {
        0: 'Non Smoker',
        1: 'Smoker'
      },
      riskClasses: {
        1: 'Class 1',
        2: 'Class 2',
        3: 'Class 3',
        4: 'Class 4'
      },
      relationships: {
        1: 'MySelf',
        2: 'Spouse',
        3: 'Childre',
        4: 'Parent',
        5: 'Other'
      },
      paymentModes: {
        S: 'Single',
        Y: 'Yearly',
        H: 'Half Yearly',
        Q: 'Quarterly',
        M: 'Monthly'
      }
    }
  };

  private _id: any = {
    header: {
      title: 'FORMULIR SURAT PENGAJUAN ASURANSI JIWA (SPAJ) BHINNEKA ASSURANCE KELUARGA INDONESIA'
    },
    footer: {
      providedBy: 'Disajikan oleh',
      providedDate: 'Disajikan tanggal',
      agentSign: 'PT Bhinneka Life Indonesia',
      phSign: 'Pemohon',
      agentStatement: 'Tanda tangan & nama jelas Agen',
      clientStatement: 'Tanda tangan & nama jelas',
      page: 'Halaman',
      to: 'dari',
      programVersion: 'Versi Program: ',
      date: 'Tanggal, '
    },
    common: {
      P1001: 'Nama Produk',
      P1002: 'Nama Paket',
      P1003: 'Nama Pemegang Polis',
      P1004: 'Usia/Jenis Kelamin',
      P1005: 'Nama Tertanggung',
      P1006: 'Usia',
      P1007: 'Kelas Pekerjaan',
      P1008: 'Hubungan dengan Pemegang Polis',
      P1009: 'Merokok / Tidak Merokok',
      P1020: 'Cara Pembayaran Premi',
      P1021: 'Mata Uang',
      P1022: 'None - None',
      P1023: 'Penempatan Investasi',
      P1024: 'Rencana Pembayaran Premi',
      P1025: 'Premi Dasar',
      P1026: 'Premi Top Up',
      P1027: 'Premi Total',
      P1028: 'Mengajukan produk Bhinneka Assurance Keluarga Indonesia sebagai berikut:',
      P1029: 'Plan Dasar',
      P1030: 'Plan Rider',
      P1031: 'Pemegang Polis',
      P1032: 'Tanggal Lahir/Usia',
      P1033: 'Data Tertanggung',
      P1034: 'Besar Uang Pertanggungan dan Lama Perlindungan',
      P1035: 'Keterangan / hubungan keluarga',
      P1036: 'Cara Pembayaran Premi/Masa Pembayaran Premi',
      P1037: 'Nama lengkap sesuai KK',
      P1038: 'Besar Premi yang dibayarkan',
      P1039: '(Belum termasuk materai)',
      P1040: 'Rincian Premi',
      P1041: 'Unit',
      P1042: 'Premi Deposit',
      P1043: 'Jumlah Premi yang harus dibayar',
      P1044: 'Biaya Polis',
      P1045: 'GRATIS',
      P1046: 'Manfaat Asuransi',
      P1047: 'Meninggal Dunia akibat Kecelakaan dalam Masa Asuransi',
      P1048: 'Meninggal Dunia akibat sakit dalam Masa Asuransi',
      P1049: 'Hidup pada akhir tahun ke- 5',
      P1050: 'Hidup pada akhir tahun ke- 10',
      P1051: '12 kali Premi Bulanan',
      P1052: 'Uang Pertanggungan Tertanggung',
      P1053: 'Premi Tertanggung',
      P1054: 'Total Premi yang dibayar',
      P1055: 'Premi',
      P1056: 'Cara Pembayaran Premi',
      P1057: 'Tanggal lahir',
      P1058: 'Jenis Kelamin',
      P1059: 'Premi Asuransi',
      P1060: '100% Uang Pertanggungan',
      P1061: '** Wana Makmur Pro -- Tabungan Prima untuk Masa Depan Cerah **',
      P1062: 'Total Premi',
      P1063: 'Diskon Premi',
      P1064: 'Premi Top Up Tunggal',
      P1065: [
        'Apabila ada pertanyaan dan keluhan Anda pada Kami, untuk informasi lebih lanjut mengenai prosedur penanganan keluhan Nasabah, silahkan menghubungi Call Center 24 jam ',
        { text: '(021) 30001288', bold: true },
        ' atau ',
        { text: 'www.wanaarthalife.com.', bold: true }
      ],
      P1066: 'Yang bertanda tangan di bawah ini', //perlu
      P1067: 'TERTANGGUNG',
      P1068: 'PEMBAYARAN PREMI',
      P1069: 'Cara Pembayaran',
      P1070: 'Masa Pembayaran',
      P1071: 'Mata Uang',
      P1072: 'JUMLAH DANA INVESTASI(000)',
      P1073: 'Nama', //perlu
      P1074: 'No. KTP', //perlu
      P1075: 'Alamat',
      P1076: 'No Telp / HP',
      P1077: 'Pekerjaan',
      P1078: 'Penghasilan pertahun',
      P1079: 'PEMBAYARAN PREMI',
      P1080: 'ALOKASI DANA INVESTASI',
      P1081: 'MANFAAT-MANFAAT',
      P1082: 'Disajikan Oleh',
      P1083: 'Calon Pemegang Polis',
      P1084: 'TOTAL',
      P1085: 'BIAYA AKUISISI',
      P1086: 'BIAYA ADMINISTRASI',
      P1087: 'Premi Bulanan',
      P1088: 'Premi Triwulanan',
      P1089: 'Premi Semesteran',
      P1090: 'Premi Tahunan dan Premi Tunggal',
      P1091: 'Rupiah',
      P1092: 'Dollar',
      P1093: 'BIAYA ASURANSI',
      P1094: 'EKSTRA PREMI',
      P1095: 'KETERANGAN:'
    },
    param: {
      currNotation: {
        IDR: 'Rp.',
        USD: 'US$.'
      },
      currency: {
        IDR: 'Rupiah',
        USD: 'Dollar'
      },
      sexes: {
        M: 'Laki-laki',
        F: 'Perempuan'
      },
      maritalStatuses: {
        S: 'Lajang',
        M: 'Menikah',
        D: 'Cerai'
      },
      smokers: {
        0: 'Tidak Merokok',
        1: 'Merokok'
      },
      riskClasses: {
        1: '1',
        2: '2',
        3: '3',
        4: '4'
      },
      relationships: {
        1: 'Diri Sendiri',
        2: 'Suami/Istri',
        3: 'Anak',
        4: 'Orang Tua',
        5: 'Lainnya'
      },
      paymentModes: {
        S: 'Sekaligus',
        Y: 'Tahunan',
        H: 'Semesteran',
        Q: 'Kuartalan',
        M: 'Bulanan'
      }
    }
  };

  get lang(): any {
    return this._lang;
  }

  set lang(lang: any) {
    this._lang = lang;
  }

  get en(): any {
    return this._en;
  }

  set en(en: any) {
    this._en = en;
  }

  get id(): any {
    return this._id;
  }

  set id(id: any) {
    this._id = id;
  }

  constructor() {
    this.contruct();
  }

  contruct() {}

  get word(): any {
    return this[this._lang];
  }

  set word(lang: any) {
    this._lang = lang;
  }
}
