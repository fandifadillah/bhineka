import * as _ from 'lodash';
import * as momentImported from 'moment';

import { GroupPolicyModel } from '@app/model/esub/GroupPolicyModel';

const moment = momentImported;

export class CommonDataPrintService {
  private _lang: string;
  private _groupPolicyModel: GroupPolicyModel = new GroupPolicyModel();
  private _appInformation: any;
  private _sign: any;

  constructor() {}

  get lang(): any {
    return this._lang;
  }

  set lang(lang: any) {
    this._lang = lang;
  }

  get groupPolicyModel(): GroupPolicyModel {
    return this._groupPolicyModel;
  }

  set groupPolicyModel(groupPolicyModel: GroupPolicyModel) {
    this._groupPolicyModel = groupPolicyModel;
  }

  get appInformation(): any {
    return this._appInformation;
  }

  set appInformation(appInformation: any) {
    this._appInformation = appInformation;
  }

  get sign(): any {
    return this._sign;
  }

  set sign(sign: any) {
    this._sign = sign;
  }
}
