import * as momentImported from 'moment';
import { CommonDataPrintService } from './common.data';
import { CommonLayoutPrintService } from './common.layout';
import { CommonWordingPrintService } from './common.wording';

import { GroupPolicyModel } from '@app/model/esub/GroupPolicyModel';
import { TrxSubmission } from '@app/model/esub/transaction';

const moment = momentImported;

export class CommonPrintService {
  private commonLayoutPrintService: CommonLayoutPrintService = new CommonLayoutPrintService();
  public data: CommonDataPrintService = new CommonDataPrintService();
  public wording: CommonWordingPrintService = new CommonWordingPrintService();

  constructor() {}

  setData(lang: string, groupPolicyModel: GroupPolicyModel, appInformation: any, sign: any) {
    console.log(
      'CommonPrintService setData(lang: string, groupPolicyModel: GroupPolicyModel, appInformation: any, sign: any){'
    );
    console.log(lang);
    this.data.lang = lang;
    this.wording.lang = lang;
    this.data.groupPolicyModel = groupPolicyModel;
    this.data.appInformation = appInformation;
    this.data.sign = sign;
    this.commonLayoutPrintService.data = this.data;
    this.commonLayoutPrintService.wording = this.wording;
  }

  public getHeader(currentPage: number, pageCount: number, pageSize: any): any {
    return this.commonLayoutPrintService.getHeader(currentPage, pageCount, pageSize);
  }

  public getFooter(
    currentPage: number,
    pageCount: number,
    pageSize: any,
    customerSignatureBase64: string,
    agentSignatureBase64: string,
    trxSubmission: TrxSubmission
  ): any {
    return this.commonLayoutPrintService.getFooter(
      currentPage,
      pageCount,
      pageSize,
      customerSignatureBase64,
      agentSignatureBase64,
      trxSubmission
    );
  }
}
