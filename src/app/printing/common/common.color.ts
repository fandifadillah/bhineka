export class CommonColorPrintService {
  get white() {
    return '#FFFFFF';
  }

  get black() {
    return '000000';
  }

  get gray() {
    return '#BDBDBD';
  }

  get green() {
    return '#5cb43c';
  }

  get primaryColor() {
    return this.green;
  }

  get textPrimaryColor() {
    return this.black;
  }

  get textContrastPrimaryColor() {
    return this.white;
  }

  get lineColor() {
    return this.gray;
  }
}
