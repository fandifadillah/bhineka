import * as momentImported from 'moment';

import { CommonDataPrintService } from './common.data';
import { CommonWordingPrintService } from './common.wording';
import { CommonImagePrintService } from './common.image';
import { TrxSubmission } from '@app/model/esub/transaction';

const moment = momentImported;

export class CommonLayoutPrintService {
  public data: CommonDataPrintService = new CommonDataPrintService();
  public wording: CommonWordingPrintService = new CommonWordingPrintService();
  public image: CommonImagePrintService = new CommonImagePrintService();
  // public color: CommonColorPrintService = new CommonColorPrintService();

  constructor() {}

  private _getHeader(): any {
    return [
      {
        alignment: 'left',
        fontSize: 8,
        columns: [this.image.headerLogo]
      },
      {
        alignment: 'right',
        fontSize: 8,
        margin: [2, 2, 2, 2],
        columns: [
          {
            width: 30,
            table: {
              body: [{}]
            }
          }
        ]
      },
      {
        columns: [
          {
            width: 20,
            table: {
              body: [{}]
            }
          },
          {
            table: {
              widths: ['*'],
              body: [
                [
                  {
                    color: 'red',
                    text: this.wording.word.header.title,
                    bold: true,
                    width: 200,
                    style: 'cellStyle',
                    alignment: 'left',
                    border: [false, false, false, false],
                    fontSize: 12
                  }
                ]
              ]
            }
          }
        ]
      }
    ];
  }

  private _getFooter(customerSignatureBase64: string, agentSignatureBase64: string, trxSubmission: TrxSubmission): any {
    return [
      {
        alignment: 'justify',
        fontSize: 6,
        columns: [
          {
            text: this.wording.word.footer.agentSign,
            width: 220,
            style: 'cellStyle',
            lineHeight: 0.7,
            alignment: 'center',
            margin: [15, 5, 0, 0],
            border: [false, false, false, false],
            fontSize: 8
          },
          {},
          {
            text: this.wording.word.footer.phSign,
            width: 215,
            style: 'cellStyle',
            lineHeight: 1,
            margin: [80, 5, 0, 0],
            border: [false, false, false, false],
            fontSize: 8
          }
        ]
      },
      {
        alignment: 'justify',
        fontSize: 6,
        columns: [
          {
            text:
              this.wording.word.footer.date +
              moment(trxSubmission.esubmission.agent_signature_date).format('DD-MM-YYYY'),
            width: 220,
            style: 'cellStyle',
            lineHeight: 0.7,
            alignment: 'center',
            margin: [25, 5, 0, 0],
            border: [false, false, false, false],
            fontSize: 8
          },
          {},
          {
            text:
              this.wording.word.footer.date +
              moment(trxSubmission.esubmission.customer_signature_date).format('DD-MM-YYYY'),
            width: 215,
            style: 'cellStyle',
            lineHeight: 1,
            margin: [60, 5, 0, 0],
            border: [false, false, false, false],
            fontSize: 8
          }
        ]
      },
      {
        table: {
          widths: [250, '*', 250],
          body: [
            [
              {
                columns: [
                  {
                    canvas: [
                      {
                        width: 200,
                        type: 'rect',
                        x: 0,
                        y: 0,
                        w: 200,
                        h: 60,
                        r: 5,
                        alignment: 'left',
                        lineColor: 'black'
                      }
                    ],
                    margin: [20, 0, 0, 0]
                  },
                  {
                    image: agentSignatureBase64,
                    width: 170,
                    height: 30,
                    margin: [-185, 15, 0, 0]
                  }
                ]
              },
              {},
              {
                columns: [
                  {
                    canvas: [
                      {
                        width: 200,
                        type: 'rect',
                        x: 0,
                        y: 0,
                        w: 200,
                        h: 60,
                        r: 5,
                        alignment: 'left',
                        lineColor: 'black'
                      }
                    ],
                    margin: [30, 0, 0, 0]
                  },
                  {
                    image: customerSignatureBase64,
                    width: 170,
                    height: 30,
                    margin: [-185, 15, 0, 0]
                  }
                ]
              }
            ]
          ]
        },
        layout: 'noBorders'
      },

      {
        alignment: 'justify',
        fontSize: 6,
        columns: [
          {
            width: 220,
            text: trxSubmission.agentName,
            margin: [20, 0, 0, 0],
            alignment: 'center',
            style: 'cellStyle',
            border: [false, false, false, false],
            fontSize: 8
          },
          {
            text: '',
            margin: [30, 0, 0, 0],
            alignment: 'center',
            style: 'cellStyle',
            border: [false, false, false, false],
            fontSize: 6
          },
          {
            width: 230,
            text: trxSubmission.esubmission.nama,
            margin: [0, 0, 0, 0],
            alignment: 'center',
            style: 'cellStyle',
            border: [false, false, false, false],
            fontSize: 8
          }
        ]
      }
    ];
  }

  public getHeader(currentPage: number, pageCount: number, pageSize: any) {
    return this._getHeader();
  }

  public getFooter(
    currentPage: number,
    pageCount: number,
    pageSize: any,
    customerSignatureBase64: string,
    agentSignatureBase64: string,
    trxSubmission: TrxSubmission
  ): any {
    return this._getFooter(customerSignatureBase64, agentSignatureBase64, trxSubmission);
  }
}
