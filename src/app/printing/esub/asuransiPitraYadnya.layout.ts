import * as _ from 'lodash';
import { AsuransiPitraYadnyaWordingPrintService } from './asuransiPitraYadnya.wording';
import { AsuransiPitraYadnyaDataPrintService } from './asuransiPitraYadnya.data';
// import { CurrencyConverter } from 'lifepos-lib-common-function';
// import { CommonColorPrintService } from '../../common/common.color';
import * as moment_ from 'moment';
import { PolicyModel } from '@app/model/esub/PolicyModel';
import { CommonPrintService } from '@app/printing/common/common.print';
import { CommonColorPrintService } from '@app/printing/common/common.color';
import { Tertanggung, TrxSubmission } from '@app/model/esub/transaction';
import { EMAIL, SMS } from '@app/app.config';
const moment = moment_;

export class AsuransiPitraYadnyaLayoutPrintService {
  public commonPrintService: CommonPrintService = new CommonPrintService();
  public COLOR: CommonColorPrintService = new CommonColorPrintService();

  public data: AsuransiPitraYadnyaDataPrintService = new AsuransiPitraYadnyaDataPrintService();
  public wording: AsuransiPitraYadnyaWordingPrintService = new AsuransiPitraYadnyaWordingPrintService();
  // public currencyConverter: CurrencyConverter = new CurrencyConverter();
  private PA: any;
  private CO: any;
  private WO: any;

  private trxSubmission: TrxSubmission;

  public translatePolicy(policy: PolicyModel): any {
    let tPolicy: any = _.cloneDeep(policy);
    // console.log(this.commonPrintService.wording.word);

    tPolicy.policyHolder.id = tPolicy.policyHolder.id ? tPolicy.policyHolder.id : '';
    tPolicy.policyHolder.customerType = tPolicy.policyHolder.customerType ? tPolicy.policyHolder.customerType : '';
    tPolicy.policyHolder.firstName = tPolicy.policyHolder.firstName ? tPolicy.policyHolder.firstName : '';
    tPolicy.policyHolder.middleName = tPolicy.policyHolder.middleName ? tPolicy.policyHolder.middleName : '';
    tPolicy.policyHolder.lastName = tPolicy.policyHolder.lastName ? tPolicy.policyHolder.lastName : '';
    tPolicy.policyHolder.maritalStatus = tPolicy.policyHolder.maritalStatus
      ? this.commonPrintService.wording.word.param.maritalStatuses[tPolicy.policyHolder.maritalStatus]
      : '';
    tPolicy.policyHolder.sex = tPolicy.policyHolder.sex
      ? this.commonPrintService.wording.word.param.sexes[tPolicy.policyHolder.sex]
      : '';
    tPolicy.policyHolder.dob = tPolicy.policyHolder.dob ? tPolicy.policyHolder.dob : '';
    tPolicy.policyHolder.age = tPolicy.policyHolder.age != null ? tPolicy.policyHolder.age.toString() : '';
    tPolicy.policyHolder.smoker = tPolicy.policyHolder.smoker
      ? this.commonPrintService.wording.word.param.smokers[tPolicy.policyHolder.smoker]
      : '';
    tPolicy.policyHolder.riskClass = tPolicy.policyHolder.riskClass
      ? this.commonPrintService.wording.word.param.riskClasses[tPolicy.policyHolder.riskClass]
      : '';
    tPolicy.policyHolder.isInstitution = tPolicy.policyHolder.isInstitution ? tPolicy.policyHolder.isInstitution : '';
    tPolicy.policyHolder.institutionName = tPolicy.policyHolder.institutionName
      ? tPolicy.policyHolder.institutionName
      : '';

    tPolicy.insured.insuredSeq = tPolicy.insured.insuredSeq ? tPolicy.insured.insuredSeq : '';
    tPolicy.insured.customerType = tPolicy.insured.customerType ? tPolicy.insured.customerType : '';
    tPolicy.insured.firstName = tPolicy.insured.firstName ? tPolicy.insured.firstName : '';
    tPolicy.insured.middleName = tPolicy.insured.middleName ? tPolicy.insured.middleName : '';
    tPolicy.insured.lastName = tPolicy.insured.lastName ? tPolicy.insured.lastName : '';
    tPolicy.insured.maritalStatus = tPolicy.insured.maritalStatus
      ? this.commonPrintService.wording.word.param.maritalStatuses[tPolicy.insured.maritalStatus]
      : '';
    tPolicy.insured.sex = tPolicy.insured.sex
      ? this.commonPrintService.wording.word.param.sexes[tPolicy.insured.sex]
      : '';
    tPolicy.insured.dob = tPolicy.insured.dob ? tPolicy.insured.dob : '';
    tPolicy.insured.age = tPolicy.insured.age != null ? tPolicy.insured.age.toString() : '';
    tPolicy.insured.smoker = tPolicy.insured.smoker
      ? this.commonPrintService.wording.word.param.smokers[tPolicy.insured.smoker]
      : '';
    tPolicy.insured.riskClass = tPolicy.insured.riskClass
      ? this.commonPrintService.wording.word.param.riskClasses[tPolicy.insured.riskClass]
      : '';
    tPolicy.insured.relationWithPolicyHolder = tPolicy.insured.relationWithPolicyHolder
      ? this.commonPrintService.wording.word.param.relationships[tPolicy.insured.relationWithPolicyHolder]
      : '';

    tPolicy.base.currency = tPolicy.base.currency ? tPolicy.base.currency : '';
    tPolicy.base.paymentMode = tPolicy.base.paymentMode
      ? this.commonPrintService.wording.word.param.paymentModes[tPolicy.base.paymentMode]
      : '';
    if (policy.base.paymentMode === 'S') {
      tPolicy.base.paymentTerm = '1';
    } else {
      tPolicy.base.paymentTerm = tPolicy.base.paymentTerm ? tPolicy.base.paymentTerm : '';
    }
    tPolicy.base.coverageTerm = tPolicy.base.coverageTerm ? tPolicy.base.coverageTerm : '';
    tPolicy.base.coverageAge = tPolicy.base.coverageAge ? tPolicy.base.coverageAge : '';

    tPolicy.base.basicPremium = tPolicy.base.basicPremium ? tPolicy.base.basicPremium : '';
    tPolicy.base.basicPremiumPlan = tPolicy.base.basicPremiumPlan ? tPolicy.base.basicPremiumPlan : '';
    tPolicy.base.savingPremium = tPolicy.base.savingPremium ? tPolicy.base.savingPremium : '';
    tPolicy.base.savingPremiumPlan = tPolicy.base.savingPremiumPlan ? tPolicy.base.savingPremiumPlan : '';
    tPolicy.base.totalPremium = tPolicy.base.totalPremium ? tPolicy.base.totalPremium : '';
    tPolicy.base.totalPremiumPlan = tPolicy.base.totalPremiumPlan ? tPolicy.base.totalPremiumPlan : '';
    tPolicy.base.premiumDiscount = tPolicy.base.premiumDiscount ? tPolicy.base.premiumDiscount : '';
    tPolicy.base.paidPremium = tPolicy.base.paidPremium ? tPolicy.base.paidPremium : '';
    tPolicy.base.basicSumAssured = tPolicy.base.basicSumAssured ? tPolicy.base.basicSumAssured : '';
    tPolicy.base.basicSumAssuredPlan = tPolicy.base.basicSumAssuredPlan ? tPolicy.base.basicSumAssuredPlan : '';
    tPolicy.base.totalSumAssured = tPolicy.base.totalSumAssured ? tPolicy.base.totalSumAssured : '';
    tPolicy.base.coi = tPolicy.base.coi ? tPolicy.base.coi : '';
    tPolicy.base.inflationRate = tPolicy.base.inflationRate ? tPolicy.base.inflationRate : '';
    tPolicy.base.inflationRatePlan = tPolicy.base.inflationRatePlan ? tPolicy.base.inflationRatePlan : '';
    tPolicy.base.regularTopup = tPolicy.base.regularTopup ? tPolicy.base.regularTopup : '';
    tPolicy.base.regularTopupPlan = tPolicy.base.regularTopupPlan ? tPolicy.base.regularTopupPlan : '';

    return tPolicy;
  }

  public getTemplate(
    type: string,
    trxSubmission: TrxSubmission,
    customerSignatureBase64: string,
    agentSignatureBase64: string
  ): any {
    this.CO = this.commonPrintService.wording.word.common;
    this.WO = this.wording.word;
    this.trxSubmission = trxSubmission;

    const page1: any = this.getEsubPrint(type);
    const content = [];

    content.push(page1);

    let docDefinition: any;
    docDefinition = {
      pageSize: 'Letter',
      pageOrientation: 'portrait',
      pageMargins: [40, 90, 40, 130],
      header: (currentPage: any, pageCount: any, pageSize: any) => {
        return this.commonPrintService.getHeader(currentPage, pageCount, pageSize);
      },
      footer: (currentPage: any, pageCount: any, pageSize: any) => {
        return this.commonPrintService.getFooter(
          currentPage,
          pageCount,
          pageSize,
          customerSignatureBase64,
          agentSignatureBase64,
          trxSubmission
        );
      },
      content
    };

    return docDefinition;
  }

  public getEsubPrint(type: string) {
    let insureds: Tertanggung[] = this.trxSubmission.esubmission.tertanggung;

    let page = [
      //yang bertanda tangan dibawah ini
      {
        columns: [
          [
            {
              fontSize: 7,
              table: {
                widths: [110, 10, 380],
                body: [
                  [
                    { text: this.CO.P1066, bold: true, style: 'cellStyle', border: [false, false, false, false] },
                    { text: ' ', style: 'cellStyle', border: [false, false, false, false] },
                    { text: ' ', style: 'cellStyle', border: [false, false, false, false] }
                  ],
                  [
                    { text: this.CO.P1073, style: 'cellStyle', border: [false, false, false, false] },
                    { text: ':', style: 'cellStyle', border: [false, false, false, false] },
                    {
                      text: this.trxSubmission.esubmission.nama_pempol,
                      style: 'cellStyle',
                      fillColor: '#ffbfb1',
                      border: [false, false, false, false]
                    }
                  ],
                  [
                    { text: this.CO.P1074, style: 'cellStyle', border: [false, false, false, false] },
                    { text: ':', style: 'cellStyle', border: [false, false, false, false] },
                    {
                      text: this.trxSubmission.esubmission.no_ktp,
                      style: 'cellStyle',
                      fillColor: '#ffbfb1',
                      border: [false, false, false, false]
                    }
                  ],
                  [
                    { text: this.CO.P1075, style: 'cellStyle', border: [false, false, false, false] },
                    { text: ':', style: 'cellStyle', border: [false, false, false, false] },
                    {
                      text:
                        this.trxSubmission.esubmission.alamat +
                        ', ' +
                        this.trxSubmission.esubmission.kelurahan +
                        ', ' +
                        this.trxSubmission.esubmission.kecamatan +
                        ', ' +
                        this.trxSubmission.esubmission.kota,
                      style: 'cellStyle',
                      fillColor: '#ffbfb1',
                      border: [false, false, false, false]
                    }
                  ],
                  [
                    { text: ' ', style: 'cellStyle', border: [false, false, false, false] },
                    { text: ' ', style: 'cellStyle', border: [false, false, false, false] },
                    {
                      text:
                        this.trxSubmission.esubmission.propinsi +
                        '. ' +
                        (this.trxSubmission.esubmission.kode_pos ? this.trxSubmission.esubmission.kode_pos : ''),
                      style: 'cellStyle',
                      fillColor: '#ffbfb1',
                      border: [false, false, false, false]
                    }
                  ],
                  [
                    { text: this.CO.P1076, style: 'cellStyle', border: [false, false, false, false] },
                    { text: ':', style: 'cellStyle', border: [false, false, false, false] },
                    {
                      text: this.trxSubmission.esubmission.no_hp,
                      style: 'cellStyle',
                      fillColor: '#ffbfb1',
                      border: [false, false, false, false]
                    }
                  ],
                  [
                    { text: this.CO.P1077, style: 'cellStyle', border: [false, false, false, false] },
                    { text: ':', style: 'cellStyle', border: [false, false, false, false] },
                    {
                      text: this.trxSubmission.esubmission.pekerjaan,
                      style: 'cellStyle',
                      fillColor: '#ffbfb1',
                      border: [false, false, false, false]
                    }
                  ],
                  [
                    { text: this.CO.P1078, style: 'cellStyle', border: [false, false, false, false] },
                    { text: ':', style: 'cellStyle', border: [false, false, false, false] },
                    {
                      text: this.trxSubmission.esubmission.penghasilan_per_tahun
                        ? Number(
                            parseInt(this.trxSubmission.esubmission.penghasilan_per_tahun)
                              .toFixed(1)
                              .toString()
                          )
                        : '',
                      style: 'cellStyle',
                      fillColor: '#ffbfb1',
                      border: [false, false, false, false]
                    }
                  ],
                  [{ text: ' ', colSpan: 3, border: [false, false, false, false] }]
                ]
              }
            }
          ]
        ]
      },

      {
        fontSize: 7,
        table: {
          widths: [120, 15, '*'],
          body: [[{ text: this.CO.P1028, colSpan: 3, style: 'cellStyle', border: [false, false, false, false] }]]
        }
      },

      {
        fontSize: 7,
        marginBottom: 10,
        table: {
          widths: [167, 167, 167, 167],
          body: [
            [
              {
                text: this.CO.P1037,
                style: 'cellStyle',
                color: 'white',
                fillColor: 'red',
                alignment: 'center',
                border: [false, false, false, false]
              },
              {
                text: this.CO.P1057,
                style: 'cellStyle',
                color: 'white',
                fillColor: 'red',
                alignment: 'center',
                border: [false, false, false, false]
              },
              {
                text: this.CO.P1035,
                style: 'cellStyle',
                color: 'white',
                fillColor: 'red',
                alignment: 'center',
                border: [false, false, false, false]
              }
            ],
            [
              {
                text: insureds.length > 0 ? insureds[0].nama : ' ',
                style: 'cellStyle',
                fillColor: '#ffbfb1',
                border: [false, false, false, false]
              },
              {
                text: insureds.length > 0 ? insureds[0].tgl_lahir : ' ',
                style: 'cellStyle',
                fillColor: '#ffbfb1',
                border: [false, false, false, false]
              },
              {
                text: insureds.length > 0 ? insureds[0].hubungan : ' ',
                style: 'cellStyle',
                fillColor: '#ffbfb1',
                border: [false, false, false, false]
              },
              { text: ' ', style: 'cellStyle', fillColor: '#ffbfb1', border: [false, false, false, false] }
            ],
            [
              {
                text: insureds.length > 1 ? insureds[1].nama : ' ',
                style: 'cellStyle',
                fillColor: '#ffbfb1',
                border: [false, false, false, false]
              },
              {
                text: insureds.length > 1 ? insureds[1].tgl_lahir : ' ',
                style: 'cellStyle',
                fillColor: '#ffbfb1',
                border: [false, false, false, false]
              },
              {
                text: insureds.length > 1 ? insureds[1].hubungan : ' ',
                style: 'cellStyle',
                fillColor: '#ffbfb1',
                border: [false, false, false, false]
              },
              { text: ' ', style: 'cellStyle', fillColor: '#ffbfb1', border: [false, false, false, false] }
            ],
            [
              {
                text: insureds.length > 2 ? insureds[2].nama : ' ',
                style: 'cellStyle',
                fillColor: '#ffbfb1',
                border: [false, false, false, false]
              },
              {
                text: insureds.length > 2 ? insureds[2].tgl_lahir : ' ',
                style: 'cellStyle',
                fillColor: '#ffbfb1',
                border: [false, false, false, false]
              },
              {
                text: insureds.length > 2 ? insureds[2].hubungan : ' ',
                style: 'cellStyle',
                fillColor: '#ffbfb1',
                border: [false, false, false, false]
              },
              { text: ' ', style: 'cellStyle', fillColor: '#ffbfb1', border: [false, false, false, false] }
            ],
            [
              {
                text: insureds.length > 3 ? insureds[3].nama : ' ',
                style: 'cellStyle',
                fillColor: '#ffbfb1',
                border: [false, false, false, false]
              },
              {
                text: insureds.length > 3 ? insureds[3].tgl_lahir : ' ',
                style: 'cellStyle',
                fillColor: '#ffbfb1',
                border: [false, false, false, false]
              },
              {
                text: insureds.length > 3 ? insureds[3].hubungan : ' ',
                style: 'cellStyle',
                fillColor: '#ffbfb1',
                border: [false, false, false, false]
              },
              { text: ' ', style: 'cellStyle', fillColor: '#ffbfb1', border: [false, false, false, false] }
            ]
          ]
        }
      },

      {
        marginBottom: 10,
        fontSize: 7,
        table: {
          body: [[{ text: this.WO[1021], style: 'cellStyle', alignment: 'left', border: [false, false, false, false] }]]
        }
      },

      {
        fontSize: 7,
        table: {
          widths: [9, 100, 10, '*'],
          body: [
            [
              { text: this.WO[1025], colSpan: 4, style: 'cellStyle', border: [false, false, false, false] },
              { text: ' ', colSpan: 3, bold: true, style: 'cellStyle', border: [false, false, false, false] },
              { text: ' ', style: 'cellStyle', border: [false, false, false, true] },
              { text: ' ', style: 'cellStyle', border: [false, false, false, true] }
            ],
            [
              {
                text: this.trxSubmission.esubmission.pilihan_cetak === EMAIL ? 'X' : '',
                style: 'cellStyle',
                border: [true, true, true, true]
              },
              { text: this.WO[1026], style: 'cellStyle', border: [false, false, false, false] },
              { text: ':', style: 'cellStyle', border: [false, false, false, false] },
              {
                text: this.trxSubmission.esubmission.epolicy_email,
                style: 'cellStyle',
                border: [false, false, false, true]
              }
            ],
            [
              { text: ' ', style: 'cellStyle', border: [false, false, false, false] },
              { text: ' ', style: 'cellStyle', border: [false, false, false, false] },
              { text: ' ', style: 'cellStyle', border: [false, false, false, false] },
              { text: this.WO[1027], style: 'cellStyle', border: [false, false, false, false] }
            ],
            [
              {
                text: this.trxSubmission.esubmission.pilihan_cetak === SMS ? 'X' : '',
                style: 'cellStyle',
                border: [true, true, true, true]
              },
              { text: this.WO[1028], style: 'cellStyle', border: [false, false, false, false] },
              { text: ':', style: 'cellStyle', border: [false, false, false, false] },
              { text: this.trxSubmission.esubmission.no_hp, style: 'cellStyle', border: [false, false, false, true] }
            ],
            [
              { text: ' ', style: 'cellStyle', border: [false, false, false, false] },
              { text: ' ', style: 'cellStyle', border: [false, false, false, false] },
              { text: ' ', style: 'cellStyle', border: [false, false, false, false] },
              { text: this.WO[1029], style: 'cellStyle', border: [false, false, false, false] }
            ],
            [
              {
                text: '',
                style: 'cellStyle',
                border: [true, true, true, true]
              },
              { text: this.WO[1030], style: 'cellStyle', border: [false, false, false, false] },
              { text: ' ', style: 'cellStyle', border: [false, false, false, false] },
              { text: ' ', style: 'cellStyle', border: [false, false, false, false] }
            ]
          ]
        }
      },

      {
        fontSize: 7,
        table: {
          widths: ['*'],
          body: [[{ text: this.WO['1018'], style: 'cellStyle', border: [false, false, false, false] }]]
        }
      }
    ];

    return page;
  }
}
