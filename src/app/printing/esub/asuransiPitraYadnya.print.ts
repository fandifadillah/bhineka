import * as _ from 'lodash';
import * as pdfMakeImported from 'pdfmake/build/pdfmake';
import * as pdfFontsImported from 'pdfmake/build/vfs_fonts';
import { Observable } from 'rxjs';
import { AsuransiPitraYadnyaLayoutPrintService } from './asuransiPitraYadnya.layout';
import { PremiumAllocationSetupModel } from '@app/model/esub';
import { FeeSetupModel } from '@app/model/esub';
import { FundSetupModel } from '@app/model/esub';
import { LienClauseSetupModel } from '@app/model/esub';
import { RuleAgeSetupModel } from '@app/model/esub';
import { RuleProductSetupModel } from '@app/model/esub';
import { ProductSetupModel } from '@app/model/esub';
import { GroupPolicyModel } from '@app/model/esub';
import { CalcUnitLinkModel } from '@app/model/esub';

// let  pdfMakeImported : any;
// let  pdfFontsImported : any;
// import {pdfMake} from "pdfmake/build/pdfmake";

// import 'pdfmake/build/pdfmake'
// import 'pdfmake/build/vfs_fonts'
const pdfMake = pdfMakeImported;
const pdfFonts = pdfFontsImported;

export class AsuransiPitraYadnyaPrintService {
  public asuransiPitraYadnyaLayoutPrintService: AsuransiPitraYadnyaLayoutPrintService = new AsuransiPitraYadnyaLayoutPrintService();
  public premiumAllocationSetupModel: PremiumAllocationSetupModel = new PremiumAllocationSetupModel();
  public feeSetupModel: FeeSetupModel = new FeeSetupModel();
  public fundSetupModel: FundSetupModel = new FundSetupModel();
  public lienClauseSetupModel: LienClauseSetupModel = new LienClauseSetupModel();
  public ruleAgeSetupModel: RuleAgeSetupModel = new RuleAgeSetupModel();
  public ruleProductSetupModel: RuleProductSetupModel = new RuleProductSetupModel();
  public productSetupModel: ProductSetupModel = new ProductSetupModel();

  type: string;

  constructor(type = 'apy') {
    pdfMake.vfs = pdfFonts.pdfMake.vfs;
    this.type = type;
  }

  public setData(
    lang: string,
    productSetupModel: ProductSetupModel,
    ruleProductSetupModel: RuleProductSetupModel,
    ruleAgeSetupModel: RuleAgeSetupModel,
    lienClauseSetupModel: LienClauseSetupModel,
    groupPolicyModel: GroupPolicyModel,
    fundSetupModel: FundSetupModel,
    feeSetupModel: FeeSetupModel,
    premiumAllocationSetupModel: PremiumAllocationSetupModel,
    calculationResult: any,
    sign: any,
    maxCoverageAge: any,
    coverageTerm: any,
    appInformation: any
  ) {
    this.asuransiPitraYadnyaLayoutPrintService.commonPrintService.setData(lang, groupPolicyModel, appInformation, sign);
    this.asuransiPitraYadnyaLayoutPrintService.data.lang = lang;
    this.asuransiPitraYadnyaLayoutPrintService.wording.lang = lang;
    this.asuransiPitraYadnyaLayoutPrintService.data.productSetup = productSetupModel;
    this.asuransiPitraYadnyaLayoutPrintService.data.ruleProductSetup = ruleProductSetupModel;
    this.asuransiPitraYadnyaLayoutPrintService.data.lienClauseSetup = lienClauseSetupModel;
    this.asuransiPitraYadnyaLayoutPrintService.data.premiumAllocationSetup = premiumAllocationSetupModel;
    this.asuransiPitraYadnyaLayoutPrintService.data.ruleAgeSetup = ruleAgeSetupModel;
    this.asuransiPitraYadnyaLayoutPrintService.data.feeSetup = feeSetupModel;
    this.asuransiPitraYadnyaLayoutPrintService.data.fundSetup = fundSetupModel;
    this.asuransiPitraYadnyaLayoutPrintService.data.groupPolicy = groupPolicyModel;
    this.asuransiPitraYadnyaLayoutPrintService.data.calculationResult = calculationResult;
    this.asuransiPitraYadnyaLayoutPrintService.data.esign = sign;
    this.asuransiPitraYadnyaLayoutPrintService.data.maxCoverageAge = maxCoverageAge;
    this.asuransiPitraYadnyaLayoutPrintService.data.coverageTerm = coverageTerm;
  }

  setupGroupPolicyData() {
    this.asuransiPitraYadnyaLayoutPrintService.data.policy = this.asuransiPitraYadnyaLayoutPrintService.translatePolicy(
      this.asuransiPitraYadnyaLayoutPrintService.data.groupPolicy.policys[0]
    );
    let healthRider = this.asuransiPitraYadnyaLayoutPrintService.data.policy.riders.filter(
      (item: any) => item.productType == 'RIDER-CHILD' && item.category == 'HEALTH' && item.selected == true
    );
    if (healthRider.length > 0) {
      this.asuransiPitraYadnyaLayoutPrintService.data.healthRiderName = healthRider[0].productName;
      this.asuransiPitraYadnyaLayoutPrintService.data.healthRiderCoi = healthRider[0].coi;
    }

    if (this.asuransiPitraYadnyaLayoutPrintService.data.policy.topups.filter((item: any) => item.year === 1).length) {
      this.asuransiPitraYadnyaLayoutPrintService.data.firstYearSingleTopup = this.asuransiPitraYadnyaLayoutPrintService.data.policy.topups.filter(
        (item: any) => item.year === 1
      )[0].amount;
    }

    if (this.asuransiPitraYadnyaLayoutPrintService.data.policy.base.currency === 'IDR') {
      this.asuransiPitraYadnyaLayoutPrintService.data.currNotation = 'Rp';
    } else {
      this.asuransiPitraYadnyaLayoutPrintService.data.currNotation = 'USD$';
    }
  }

  // createPdf(): Observable<any> {
  //   let docDefinition = this.asuransiPitraYadnyaLayoutPrintService.getTemplate(this.type);
  //
  //   return new Observable<CalcUnitLinkModel[]>(observer => {
  //     const pdfDocGenerator = pdfMake.createPdf(docDefinition);
  //     pdfDocGenerator.getDataUrl((dataUrl: any) => {
  //       observer.next(dataUrl);
  //       observer.complete();
  //     });
  //   });
  // }

  // downloadPDF(fileName: string) {
  //   let docDefinition = this.asuransiPitraYadnyaLayoutPrintService.getTemplate(this.type);
  //   pdfMake.createPdf(docDefinition).download(fileName);
  // }
}
