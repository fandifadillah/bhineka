export class AsuransiPitraYadnyaWordingPrintService {
  private _lang: string;

  public _en: any = {};

  public _id: any = {
    '1001': {
      ganesha: 'PRO WHOLE LIFE (GANESHA)',
      apy: 'ASURANSI PITRA YADNYA',
      pms: 'PROTEKSI MULTI SEJAHTERA'
    },
    '1002':
      '- Premi dibayar {{masaPembayaran}} tahun dan selanjutnya kontrak asuransi tetap berjalan tanpa membayar premi lagi\
		\n- Jika tertanggung meninggal dunia, maka kepada ahli waris yang ditunjuk akan dibayarkan sebesar uang pertanggungan\
		\n- Nilai tunai adalah nilai uang yang akan diterima pemegang polis jika pemegang polis membatalkan pertanggungan',
    '1003': 'Manfaat Pertanggungan (Tambahan)',
    '1004': 'PERSONAL ACCIDENT',
    '1005': 'HOSPITALIZATION CASH PLAN',
    '1006': 'TERM RIDER',
    '1007': 'TOTAL PERMANENT DISABILITY',
    '1118': 'PAYOR BENERFIT RIDER',
    '1008':
      'Uang Pertanggungan Asuransi Personal Accident Risiko ABD sebesar Rp {{uangPertanggung}} dibayarkan sesuai dengan Ketentuan Tambahan Personal Accident Terlampir. Santunan ini diberikan hingga usia Tertanggung {{maxCoverage}} tahun.',
    '1009':
      'Santunan sebesar Rp {{uangPertanggung}}  per hari diberikan apabila tertanggung dirawat inap di rumah sakit (mulai hari pertama) akibat penyakit atau kecelakaan, maksimum 365 hari dalam satu tahun. Masa asuransi HCP hingga usia Tertanggung {{maxCoverage}} tahun.',
    '1010':
      'Uang pertanggungan Rp {{uangPertanggung}}  ditambah dengan uang pertanggungan dasar, akan diberikan jika tertanggung meninggal dunia dalam masa asuransi Term Rider (hingga usia Tertanggung {{maxCoverage}} tahun).',
    '1011':
      'Uang Pertanggungan Asuransi Cacat Tetap Total sebesar Rp {{uangPertanggung}} dibayarkan sesuai dengan Ketentuan Tambahan Asuransi Total Permanent Disability Terlampir. Santunan ini diberikan sesuai dengan Masa Kontrak Asuransi Total Permanent Disability hingga usia {{maxCoverage}} tahun.',
    '1112':
      'Jika Pemegang Polis meninggal dunia atau mengalami Cacat Tetap Total  (akibat sakit maupun kecelakaan), maka polis menjadi bebas premi. Masa asuransi Payor Benefit Rider adalah hingga usia Pemegang Polis {{maxCoverage}} tahun (untuk risiko meninggal dunia) dan 65 tahun (untuk risiko Cacat Tetap Total) atau selama masa pembayaran premi.',
    '1012': 'Tidak ada Biaya Administrasi',
    '1013': 'Rate Premi bisa berubah sesuai kebijakan Management dan persetujuan OJK.',
    '1014': '- Ketentuan rider akan diatur terpisah.',
    '1015':
      'Pemegang Polis memiliki hak untuk membatalkan dan mengembalikan Polis ini kepada Penanggung dalam jangka waktu 14 (empat belas) hari kalender sejak tanggal diterimanya Polis. Atas pembatalan ini, Pemegang Polis dikenakan Biaya Administrasi.',
    '1016':
      'Uang pertanggungan asuransi cacat tetap total dibayarkan sesuai dengan ketentuan tambahan asuransi total permanent disability terlampir. Santunan ini diberikan sesuai dengan masa kontrak asuransi Total Permanent Disability hingga usia 65 tahun',
    '1017': '* Perhitungan proposal ini berlaku sampai dengan tanggal {{expiryDate}}',
    '1018':
      'Surat Pengajuan Asuransi Jiwa yang diisi dengan lengkap dan benar menjadi dasar perjanjian asuransi jwa antara Bhinneka Life dengan Pemegang Polis dan jika kemudian ternyata bahwa keterangan-keterangan yang dinyatakan dalam Surat Pengajuan Asuransi Jiwa tidak benar dan palsu, sedang perjanjian asuransi telah berjalan, maka perjanjian asuransi tidak berlaku atau batal demi hukum.',
    '1019': ' - SCOR Vi, France\
		\n- Munich Re, Germany\
		\n- TOA Re, Japan\
		\n- Maskapai Reasuransi Indonesia',
    '1020':
      '- Reasuransi Internasional Indonesia\
		\n- Reasuransi Nasional Indonesia\
		\n- Tugu Jasatama Reasuransi Indonesia',
    '1021':
      'Dengan ini menyatakan bahwa nama-nama yang tercantum di atas adalah keluarga inti (kepala keluarga, istri, dan anak kandung sesuai dengan Kartu Keluarga) sedang dalam keadaan sehat dan tidak dalam perawatan dokter.',
    '1022': 'Premi Tahunan',
    '1023': 'Nilai Tunai Akhir Tahunan',
    '1024': 'Up Meninggal',
    '1025': 'Pilihan pencetakan polis (pilih salah satu):',
    '1026': 'e Polis melalui email',
    '1027': '(alamat email wajib diisi)',
    '1028': 'e Polis melalui SMS',
    '1029': '(nomor handphone wajib diisi)',
    '1030': 'Cetak',
    '1031':
      'Pengobatan karena sakit akibat suatu tindakan yang bersifat sengaja (pengguguran, kecanduan alkohol atau obat bius, tato, sunat, sterilisasi, bunuh diri, operasi dan perawatan untuk tujuan kecantikan, obesitas dengan segala akibatnya, cidera yang disengaja atau keterbukaan yang disengaja terhadap bahaya besar).',
    '1032':
      'Perawatan di rumah sakit dalam waktu 30 hari dari tanggal mulai berlakunya POLIS kecuali akibat kecelakaan.',
    '1033':
      'Luka yang diakibatkan perjalanan dengan pesawat terbang atau semacamnya kecuali apabila tertanggung merupakan penumpang dari suatu perusahaan penerbangan komersial dengan jadwal penerbangan yang tetap.',
    '1034': 'Kecelakaan sebagai akibat melakukan tindakan kriminal atau tindakan percobaan bunuh diri.',
    '1035':
      'Kecelakaan karena ikut mempersiapkan diri untuk mengambil bagian dari suatu perlombaan ketangkasan, kecepatan, dan sebagainya dengan menggunakan kendaraan bermotor, sepeda, perahu, kuda, pesawat udara atau sejenisnya, terjun payung, mendaki gunung, panjat tebing, menyelam dengan scuba, rafting, bungee jumping, ski air, ski es, tinju, karate, judo, atau olahraga lainnya yang sejenis dan setiap kegiatan yang mengandung bahaya.',
    '1036': 'Akibat atas timbulnya reaksi inti atom atau nuklir.',
    '1037': 'Pemeriksaan fisik dan laboratorium tanpa maksud untuk mengobati suatu penyakit.',
    '1038': 'Kehamilan, kelahiran, upaya mempunyai anak, kemandulan, cuci darah.',
    '1039':
      'Penyakit atau kecelakaan karena perang, pemberontakan, huru-hara, pemogokan, keadaan darurat perang, bencana alam nasional, invasi dan terorisme.',
    '1040':
      'Pengobatan psikosis, neurosis, penyakit jiwa atau penyakit mental lainnya (termasuk setiap manifestasi dari gangguan kejiwaan atau psikosomatik).',
    '1041': 'Infeksi HIV & ARC.',
    '1042':
      'Pengobatan dan perawatan yang bersifat eksperimental atau yang tidak/belum diakui secara sah oleh bidang kedokteran sebagai cara yang tepat untuk mengobati suatu penyakit.',
    '1043': 'Pengobatan penyakit kelamin atau setiap penyakit yang berhubungan dengan penyakit kelamin.',
    '1044': 'Epidemi.',
    '1045':
      'Penyakit-penyakit dibawah ini yang diderita (diketahui ataupun tidak) oleh TERTANGGUNG, kecuali setelah melewati masa tunggu selama 12 (dua belas) bulan sejak mulai berlakunya asuransi:',
    '1046': ' Tuberculosis (TBC) & Asthma Bronchiale.',
    '1047': ' Batu / radang kandung empedu.',
    '1048': ' Batu / radang pada ginjal, saluran kemih atau kandung kemih.',
    '1049': ' Hipertensi (tekanan darah tinggi).',
    '1050': ' Ayan (Epilepsi).',
    '1051': ' Segala jenis tumor jinak / ganas.',
    '1052': ' Varikokel atau hidrokel.',
    '1053': ' Penyakit jantung & pembuluh darah, CVD (Cerebro Vascular Disease).',
    '1054': ' Radang atau tukak pada lambung atau pada usus dua belas jari.',
    '1055': ' Pembedahan pengangkatan rahim, baik dengan atau tanpa pengangkatan saluran telur atau indung telur.',
    '1056': ' Segala jenis kelainan telinga, hidung (termasuk rongga sinus) dan tenggorokan.',
    '1057':
      ' Semua jenis kelainan vertebro spinal (tulang belakang atau sumsum tulang belakang) termasuk kelainan diskus.',
    '1058': ' Segala jenis Hernia.',
    '1059': ' Penyakit kelenjar gondok(Thyroid).',
    '1060': ' Wasir (hemorrhoid) dan kelainan sekitar anus.',
    '1061': ' Amandel yang perlu tindakan operasi.',
    '1062': ' Kencing manis (Diabetes Mellitus).',
    '1063': ' Segala jenis kelainan lutut & rheumatik.',
    '1064': ' Catarract (kekeruhan lensa mata).',
    '1065': ' Segala bentuk gangguan pada hati (liver).',
    '1066':
      'Pembayaran HCP / Perawatan Rumah Sakit yang dijamin sampai dengan 365 hari dalam satu tahun asuransi adalah perawatan yang diakibatkan cidera kecelakaan atau oleh suatu penyakit yang pemberian uang santunan hariannya dihitung mulai sejak hari pertama perawatan di rumah sakit.',
    '1067':
      'Asuransi HCP ini dengan sendirinya berakhir apabila Tertanggung mencapai usia 70 tahun, atau hingga akhir masa asuransi plan dasar (mana yang lebih dahulu tercapai).',
    '1068': 'Personal Accident {{riskClass}}/{{riskResiko}}',
    '1069': [
      'Serangan Jantung',
      'Stroke',
      'Kanker',
      'Gagal Ginjal',
      'Kelumpuhan',
      'Pencangkokan Organ Jantung',
      'Multiple Sklerosis',
      'Hepatitis Virus Fulminan',
      'Hipertensi Arteri Pulmoner',
      'Kehilangan Kemampuan Melihat',
      'Penyakit Alzheimer',
      'Koma',
      'Kehilangan Kemampuan Berbicara',
      'Kehilangan Kemampuan Mendengar',
      'Luka Bakar Mayor',
      'Terminal Illness',
      'Penyakit Motor Neuron',
      'Penyakit Parkinson',
      'Anemia Aplastik',
      'Muscular Dystrophy',
      'Meningitis Bakteri',
      'Tumor Otak Jinak',
      'Radang Otak',
      'Polio',
      'Bedah Aorta',
      'Penyakit Hati Kronis',
      'Penyakit Paru Kronis',
      'Pembedahan Arteri Koroner',
      'Hydrocephalus',
      'Operasi Penggantian Katup Jantung',
      'Angioplasty'
    ],
    '1070': 'Daftar penyakit kritis yang disantun adalah sebagai berikut:',
    '1071':
      'Jika Tertanggung terdiagnosis salah satu dari 31 jenis penyakit kritis, maka akan diberikan percepatan pembayaran santunan sebesar 50% Uang Pertanggungan Dasar (maksimum Rp500.000.000 atau US$60,000). Sisa dari Uang Pertanggungan tersebut akan diberikan jika Tertanggung meninggal dunia atau mencapai usia 100 tahun. Masa asuransi Critical Illness Rider berlaku hingga usia Tertanggung 65 tahun.',
    '1072':
      'Jika Pemegang Polis terdiagnosis salah satu dari 31 jenis penyakit kritis, maka Pemegang Polis akan dibebaskan dari kewajiban membayar premi. Masa asuransi Payor Critical Illness Rider berlaku selama masa pembayaran premi.',
    '1073': 'CRITICAL ILLNESS RIDER',
    '1074': 'PAYOR CRITICAL ILLNESS RIDER'
  };

  get lang(): any {
    return this._lang;
  }

  set lang(lang: any) {
    this._lang = lang;
  }

  get en(): any {
    return this._en;
  }

  set en(en: any) {
    this._en = en;
  }

  get id(): any {
    return this._id;
  }

  set id(id: any) {
    this._id = id;
  }

  get word(): any {
    return this[this._lang];
  }

  set word(lang: any) {
    this._lang = lang;
  }
}
