import * as _ from 'lodash';

export class AsuransiPitraYadnyaDataPrintService {
  private _lang: string;

  public _groupPolicy: any;
  public policy: any;
  public calculationResult: any;
  public esign: any;
  public maxCoverageAge: any;
  public coverageTerm: any;

  public healthRiderName: any;
  public currNotation: any;
  public firstYearSingleTopup: any;
  public healthRiderCoi: any;

  public feeSetup: any;
  public premiumAllocationSetup: any;
  public fundSetup: any;
  public lienClauseSetup: any;
  public ruleAgeSetup: any;
  public ruleProductSetup: any;
  public productSetup: any;

  get lang(): any {
    return this._lang;
  }

  set lang(lang: any) {
    this._lang = lang;
  }

  get groupPolicy(): any {
    return this._groupPolicy;
  }

  set groupPolicy(groupPolicy: any) {
    this._groupPolicy = groupPolicy;
  }

  /**
	let ruleAgeSetup: any = this.ruleAgeSetupIService.getByProductId(this.groupPolicy.policys[0].base.productId);
	let maxCoverageAge: number = 0;

	if (ruleAgeSetup.length){
		maxCoverageAge = ruleAgeSetup[0].maxInsuredCoverageAge;
	}

	let ruleAgeSetup: any = this.ruleAgeSetupIService.getByProductId(this.groupPolicy.policys[0].base.productId);
	let maxCoverageAge: number = 0;

	if (ruleAgeSetup.length){
		maxCoverageAge = ruleAgeSetup[0].maxInsuredCoverageAge;
	}

	let ruleAgeSetup: any = this.ruleAgeSetupIService.getByProductId(this.groupPolicy.policys[0].base.productId);
	let maxCoverageAge: number = 0;
	let coverageTerm: number = 0;

	if (ruleAgeSetup.length){
		maxCoverageAge = ruleAgeSetup[0].maxInsuredCoverageAge;
		coverageTerm = maxCoverageAge-this.policy.insured.age;
	}

	 */

  /**
		this.groupPolicy=groupPolicy;
		this.policy = this.groupPolicy.policys[0];
		CommonPrintIService.groupPolicyModel = groupPolicy;

		this.policy=CommonPrintIService.translatePolicy(this.policy, "id");
		//this.policy=policy;
		let PA = this.getParamWord();
		let CO = this.getCommonWord();
		let WO = this.wording.id;
		let currNotation = this.getCurrencyNotation();
		//Find health rider
		let healthRider = this.policy.riders.filter(item=>item.productType=="RIDER-CHILD" && item.category=="HEALTH" && item.selected==true)
		let healthRiderName : string="";
		let healthRiderCoi : string="";
		if (healthRider.length>0){
			healthRiderName=healthRider[0].productName;
			healthRiderCoi=this.currencyConverter.transform(healthRider[0].coi);
		}

		let firstYearSingleTopup: number = 0;
		if (this.policy.topups.filter(item=>item.year===1).length){
			firstYearSingleTopup = this.policy.topups.filter(item=>item.year===1)[0].amount;
		}					  */
}
