import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ClassProvider, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ServiceWorkerModule } from '@angular/service-worker';
import { TranslateModule } from '@ngx-translate/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';

import { environment } from '@env/environment';
import { CoreModule } from '@app/core';
import { SharedModule } from '@app/shared';
import { HomeModule } from './home/home.module';
import { ShellModule } from './shell/shell.module';
import { LoginModule } from './login/login.module';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HomeEmitterService } from '@app/home/service/home-emitter.service';
import { FilterModule } from '@app/home/filter/filter.module';
import { PaymentModule } from './payment/payment.module';
import { ShellEmitterService } from '@app/shell/service/shell-emitter.service';
import { LoggingInterceptor } from '@app/core/http/logging.interceptor';
import { SplashModule } from '@app/splash/splash.module';
import { DaoModule } from '@app/platform/dao/dao.module';
import { SelectProductModule } from '@app/esub/select-product/select-product.module';
import { PolisHolderModule } from '@app/esub/polis-holder/polis-holder.module';
import { InsuredModule } from '@app/esub/insured/insured..module';
import { PolisDeliveryModule } from '@app/esub/polis-delivery/polis-delivery.module';
import { PreviewDataModule } from '@app/esub/preview-data/preview-data.module';
import { SignaturePadModule } from 'angular2-signaturepad';
import { PreviewBrosurModule } from '@app/esub/preview-brosur/preview-brosur.module';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { DeviceDetectorModule } from 'ngx-device-detector';
import { PolisHolderComponent } from '@app/esub/polis-holder/polis-holder.component';
import { AlphabetDirective } from '@app/utils/alphabet.directive';

const LOGGING_INTERCEPTOR_PROVIDER: ClassProvider = {
  provide: HTTP_INTERCEPTORS,
  useClass: LoggingInterceptor,
  multi: true
};

@NgModule({
  imports: [
    BrowserModule,
    PdfViewerModule,
    ServiceWorkerModule.register('./ngsw-worker.js', { enabled: environment.production }),
    FormsModule,
    HttpClientModule,
    TranslateModule.forRoot(),
    BrowserAnimationsModule,
    MaterialModule,
    SignaturePadModule,
    CoreModule,
    SharedModule,
    ShellModule,
    HomeModule,
    PaymentModule,
    FilterModule,
    LoginModule,
    SplashModule,
    SelectProductModule,
    InsuredModule,
    PolisHolderModule,
    PolisDeliveryModule,
    PreviewDataModule,
    PreviewBrosurModule,
    AppRoutingModule, // must be imported as the last module as it contains the fallback route
    DaoModule,
    DeviceDetectorModule.forRoot()
  ],
  declarations: [AppComponent, AlphabetDirective],
  providers: [Keyboard, StatusBar, SplashScreen, HomeEmitterService, ShellEmitterService, LOGGING_INTERCEPTOR_PROVIDER],
  bootstrap: [AppComponent]
})
export class AppModule {}

// platformBrowserDynamic().bootstrapModule(AppModule, {});
