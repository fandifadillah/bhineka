import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppVersionService } from '@app/platform/dao/local/AppVersionService';
import { APK_STORE, VERSION_CODE, VERSION_NAME } from '@app/app.config';
import { AlertComponent } from '@app/shared/dialog/alert/alert.component';
import { MatDialog } from '@angular/material';

// const log = new Logger('AuthenticationGuard');

@Component({
  selector: 'app-splash',
  templateUrl: './splash.component.html',
  styleUrls: ['./splash.component.scss']
})
export class SplashComponent implements OnInit {
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    public dialog: MatDialog,
    private appVersionService: AppVersionService
  ) {}

  ngOnInit() {
    var redirect: string = '';
    // if(this.appVersionService.getMstApplication()){
    //   let mstApplication = this.appVersionService.getMstApplication();
    //   if(mstApplication.version>parseInt(localStorage.getItem(VERSION_CODE).toString())){
    //     this.openDialog("Pembaruan", "Terdapat pembaharuan aplikasi mohon lakukan update terlebih dahulu", () => {
    //       window.open(APK_STORE, '_system');
    //     })
    //     return;
    //   }else{
    //     this.navigate()
    //   }
    // }else{
    //   this.navigate()
    // }
    this.appVersionService.checkVersion(() => {
      this.navigate();
    });
  }

  navigate() {
    setTimeout(() => {
      this.router.navigateByUrl('/login', {
        relativeTo: this.activatedRoute,
        replaceUrl: true,
        skipLocationChange: true
      });
    }, 1000);
  }

  versionName() {
    return localStorage.getItem(VERSION_NAME);
  }

  openDialog(title: string, message: string, myCallback: () => void): void {
    const dialogRef = this.dialog.open(AlertComponent, {
      disableClose: true,
      width: '80%',
      data: { title: title, message: message }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (myCallback) {
        myCallback();
      }
    });
  }
}
