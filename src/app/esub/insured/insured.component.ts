import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Esubmission, Tertanggung, TrxSubmission } from '@app/model/esub/transaction';
import { EsubLocalService } from '@app/platform/dao/local/esub-local.service';
import { Relationship, RelationshipDao } from '@app/platform/dao/sqlite/RelationshipDao';
import { BundleService } from '@app/platform/dao/local/BundleService';
import { DateAdapter, MAT_DATE_FORMATS, MatDialog } from '@angular/material';
import { LoaderComponent } from '@app/shared';
import { Utility } from '@app/utils/utility.service';
import * as moment from 'moment';
import { APP_DATE_FORMATS, AppDateAdapter } from '@app/utils/format-datepicker';

@Component({
  selector: 'app-insured',
  templateUrl: './insured.component.html',
  styleUrls: ['./insured.component.scss'],
  providers: [
    { provide: DateAdapter, useClass: AppDateAdapter },
    { provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS }
  ]
})
export class InsuredComponent implements OnInit {
  alphabet = /^[a-zA-Z ]+$/;

  moment = moment;
  progressDialogRef: any = null;

  relationships: Relationship[][] = new Array(new Array());
  selectedRelationship: Relationship[] = new Array();
  relationship: Relationship;
  birthdates: Date[] = new Array();
  dateWarnings: string[] = new Array();
  nameWarnings: string[] = new Array();
  relationsWarnings: string[] = new Array();
  insureds: Tertanggung[];
  insured: Tertanggung;
  insuredIndex: number = 0;
  trxSubmission: TrxSubmission;
  esubmission: Esubmission = new Esubmission();
  limitInsured = 4;

  constructor(
    private location: Location,
    private router: Router,
    private esubLocalService: EsubLocalService,
    private bundleService: BundleService,
    private relationshipDao: RelationshipDao,
    public progressDialog: MatDialog,
    private utility: Utility
  ) {
    this.trxSubmission = this.bundleService.getData();
    this.esubmission = JSON.parse(this.trxSubmission.json);
    try {
      this.insureds = JSON.parse(this.trxSubmission.tertanggungListJson);
      if (!this.insureds || this.insureds == null) {
        this.insureds = new Array();
      }
    } catch (e) {
      console.error(e);
      this.insureds = new Array();
    }
    console.log('TRX SUBMISSION ### ' + JSON.stringify(this.trxSubmission));
    if (this.insureds) {
      this.insureds.forEach((value, index) => {
        this.dateWarnings[index] = '';
        this.nameWarnings[index] = '';
        this.relationsWarnings[index] = '';
        this.birthdates[index] = moment(value.tgl_lahir, 'DD-MM-YYYY').toDate();
      });
    }
  }

  ngOnInit() {
    setTimeout(() => {
      this.relationshipDao.getRelationship('1', '0', result => {
        this.relationships[0] = result;
        if (this.insureds && this.insureds.length > 0) {
          this.insureds.forEach((value, index) => {
            if (index == 0) {
              value.nama = this.esubmission.nama;
            }
          });
          this.insuredIndex = this.insureds.length;
          this.onClickRelationships(1, this.insureds[0].hubungan, false);
        } else {
          this.insureds = new Array();
          let tertanggung = new Tertanggung();
          tertanggung.nama = this.esubmission.nama;
          this.insuredIndex = this.insuredIndex + 1;
          tertanggung.no = this.insuredIndex;
          this.insureds.push(tertanggung);
        }
      });
    }, 500);
  }

  onClickAdd() {
    let tertanggung = new Tertanggung();
    this.insuredIndex = this.insuredIndex + 1;
    tertanggung.no = this.insuredIndex;
    this.insureds.push(tertanggung);
  }

  onClickNext() {
    console.log('INSUREDS === ' + JSON.stringify(this.insureds));
    let valid = true;
    if (!this.insureds) {
      return;
    }
    this.insureds.forEach((value, index) => {
      if (!value.nama || value.nama === '') {
        this.nameWarnings[index] = 'Mohon lengkapi nama tertanggung';
        valid = false;
        return;
      }
      if (!value.nama.match(this.alphabet)) {
        this.nameWarnings[index] = 'Nama tidak boleh mengandung karakter non alphabet';
        valid = false;
        return;
      }
      if (!this.birthdates[index]) {
        this.dateWarnings[index] = 'Mohon lengkapi tanggal lahir tertanggung';
        valid = false;
        return;
      }

      if (!value.hubungan || value.hubungan === '') {
        this.relationsWarnings[index] = 'Mohon lengkapi hubungan tertanggung';
        valid = false;
        return;
      }
      let month = this.utility.monthsDiff(this.birthdates[index], new Date());
      if (index === 0) {
        if (value.nama === this.esubmission.nama_pempol) {
          //  Min 17 tahun 6 bulan, Max 64 tahun 6 bulan (suami/istru)
          // ALert Usia [Suami/ Istri] minimal 17 tahun 6 bulan dan maksimal 64 tahun 6 bulan.
          let minMonth = 17 * 12 + 6;
          let maxMonth = 64 * 12 + 6;
          if (month < minMonth) {
            this.dateWarnings[index] = 'Usia ' + value.hubungan + ' minimal 18 tahun & maksimal 64 tahun';
            valid = false;
            return;
          }
          if (month > maxMonth) {
            this.dateWarnings[index] = 'Usia ' + value.hubungan + ' minimal 18 tahun & maksimal 64 tahun';
            valid = false;
            return;
          }
        } else {
          //  Min 18 tahun , Max 64  (suami/istru)
          // ALert Usia [Suami/ Istri] minimal 18 tahun dan maksimal 64 tahun.
          let minMonth = 18 * 12;
          let maxMonth = 64 * 12;
          if (month < minMonth) {
            this.dateWarnings[index] = 'Usia ' + value.hubungan + ' minimal 18 tahun & maksimal 64 tahun ';
            valid = false;
            return;
          }
          if (month > maxMonth) {
            this.dateWarnings[index] = 'Usia ' + value.hubungan + ' minimal 18 tahun & maksimal 64 tahun ';
            valid = false;
            return;
          }
        }
      }
      if (index > 0) {
        if (value.hubungan === 'Istri' || value.hubungan === 'Suami') {
          if (value.nama !== this.esubmission.nama_pempol) {
            // Min 14 tahun 6 bulan, Max 64 tahun 6 bulan (suami/istri)
            // ALert Usia [Suami/ Istri] minimal 14 tahun 6 bulan dan maksimal 64 tahun 6 bulan.
            let minMonth = 14 * 12 + 6;
            let maxMonth = 64 * 12 + 6;
            if (month < minMonth) {
              this.dateWarnings[index] = 'Usia ' + value.hubungan + ' minimal 15 tahun & maksimal 64 tahun';
              valid = false;
              return;
            }
            if (month > maxMonth) {
              this.dateWarnings[index] = 'Usia ' + value.hubungan + ' minimal 15 tahun & maksimal 64 tahun';
              valid = false;
              return;
            }
          } else {
            //  Min 15 tahun , Max 64  (suami/istri)
            // ALert Usia [Suami/ Istri] minimal 15 tahun dan maksimal 64 tahun.
            let minMonth = 15 * 12;
            let maxMonth = 64 * 12;
            if (month < minMonth) {
              this.dateWarnings[index] = 'Usia ' + value.hubungan + ' minimal 15 tahun & maksimal 64 tahun ';
              valid = false;
              return;
            }
            if (month > maxMonth) {
              this.dateWarnings[index] = 'Usia ' + value.hubungan + ' minimal 15 tahun & maksimal 64 tahun ';
              valid = false;
              return;
            }
          }
        } else {
          // Anak 2 bulan dan max 25 tahun 6
          // bulan -1 hari
          // Alert Usia Anak minimal 2 bulan dan maksimal 25
          // tahun.
          let minMonth = 2;
          let maxMonth = 25 * 12 + 6;
          if (month < minMonth) {
            this.dateWarnings[index] = 'Usia ' + value.hubungan + ' minimal 2 bulan & maksimal 25 tahun ';
            valid = false;
            return;
          }
          if (month > maxMonth) {
            this.dateWarnings[index] = 'Usia ' + value.hubungan + ' minimal 2 bulan & maksimal 25 tahun ';
            valid = false;
            return;
          }
        }
      }
    });
    if (valid) {
      this.esubmission.tertanggung = this.insureds;
      this.trxSubmission.esubmission = this.esubmission;
      this.trxSubmission.json = JSON.stringify(this.esubmission);
      this.trxSubmission.tertanggungListJson = JSON.stringify(this.insureds);
      this.bundleService.setData(this.trxSubmission);

      console.log('TERTANGGUNGS ' + JSON.stringify(this.insureds));

      this.esubLocalService.updateTertanggungListJson(this.trxSubmission.draft_id, this.insureds, result => {});
      // this.esubLocalService.saveEsubmission(this.trxSubmission.draft_id, this.trxSubmission, result => {});
      setTimeout(() => {
        this.router.navigate(['/esub/polis-delivery'], { replaceUrl: false });
      }, 500);
    }
  }

  onClickRelationships(nextIndex: number, selectedRelationshipLabel: string, reset: boolean) {
    let selectedRelationship: Relationship = null;
    if (this.relationships && this.relationships[nextIndex - 1]) {
      this.relationships[nextIndex - 1].forEach(value => {
        if (value.label === selectedRelationshipLabel) {
          selectedRelationship = value;
          return;
        }
      });
    }
    if (nextIndex == 2 && selectedRelationshipLabel === 'Anak') {
      this.limitInsured = 3;
    }
    if (nextIndex == 2 && selectedRelationshipLabel != 'Anak') {
      this.limitInsured = 4;
    }

    if (selectedRelationship) {
      let id = null;
      if (nextIndex < 2) {
        id = selectedRelationship.id;
      }
      this.relationshipDao.getRelationship((nextIndex + 1).toString(), id, result => {
        this.relationships[nextIndex] = result;
        console.log('Next Relationship ' + this.relationships[nextIndex]);
        if (!reset) {
          if (this.insureds && this.insureds.length > nextIndex) {
            this.onClickRelationships(nextIndex + 1, this.insureds[nextIndex].hubungan, reset);
          }
        }
      });
      if (reset) {
        let previousInsuredLength = this.insureds ? this.insureds.length : 0;
        this.insureds.splice(nextIndex, previousInsuredLength - nextIndex);
        this.birthdates.splice(nextIndex, previousInsuredLength - nextIndex);
        console.log('BIRTHDATES ### ' + JSON.stringify(this.birthdates));
        this.insuredIndex = nextIndex;
      }
    }
  }

  onClickBack() {
    this.location.back();
  }

  openProgress(): void {
    if (this.progressDialogRef == null) {
      this.progressDialogRef = this.progressDialog.open(LoaderComponent, {
        data: {}
      });
      this.progressDialogRef.afterClosed().subscribe((result: any) => {
        console.log('The dialog was closed');
      });
    }
  }

  closeProgress() {
    if (this.progressDialogRef) {
      this.progressDialog.closeAll();
      this.progressDialogRef = null;
    }
  }
}
