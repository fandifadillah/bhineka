import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { extract } from '@app/core';
import { InsuredComponent } from '@app/esub/insured/insured.component';

const routes: Routes = [{ path: 'esub/insured', component: InsuredComponent, data: { title: extract('Insured') } }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class InsuredRoutingModule {}
