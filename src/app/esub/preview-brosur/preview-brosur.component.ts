import { Component, NgZone, OnInit } from '@angular/core';
import {
  CalcUnitLinkModel,
  FeeSetupModel,
  FundSetupModel,
  GroupPolicyModel,
  LienClauseSetupModel,
  PremiumAllocationSetupModel,
  ProductSetupModel,
  RuleAgeSetupModel,
  RuleProductSetupModel
} from '@app/model/esub';
import { GaneshaProWholeLifePrintService } from '@app/printing/GaneshaProWholeLife/ganesha.print';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';
import {
  Esubmission,
  MstStatus,
  PAID_ID,
  SUBMITTED_ID,
  SUBMITTED_LABEL,
  TrxSubmission
} from '@app/model/esub/transaction';
import { EsubLocalService } from '@app/platform/dao/local/esub-local.service';
import { MatDialog } from '@angular/material';
import { LoaderComponent } from '@app/shared';
import { BundleService } from '@app/platform/dao/local/BundleService';
import { CredentialsService, HttpService } from '@app/core';
import {
  IMEI,
  KPLKGA,
  SUBMIT_ESUB,
  VERSION_CODE,
  VERSION_NAME,
  X_IBM_CLIENT_ID,
  X_IBM_CLIENT_SECRET
} from '@app/app.config';
import { AlertComponent } from '@app/shared/dialog/alert/alert.component';
import { HttpHeaders } from '@angular/common/http';
import moment = require('moment');
import { AuthenticationService } from '@app/core/authentication/authentication.service';

const options = {
  maxSizeMB: 1, // (default: Number.POSITIVE_INFINITY)
  maxWidthOrHeight: 500, // compressedFile will scale down by ratio to a point that width or height is smaller than maxWidthOrHeight (default: undefined)
  useWebWorker: true, // optional, use multi-thread web worker, fallback to run in main-thread (default: true)
  maxIteration: 10 // optional, max number of iteration to compress the image (default: 10)
};

@Component({
  selector: 'app-preview-brosur',
  templateUrl: './preview-brosur.component.html',
  styleUrls: ['./preview-brosur.component.scss']
})
export class PreviewBrosurComponent implements OnInit {
  title = 'lifepos-print';

  base64Src: any = '';
  base64String: any = '';
  type: string = 'apy';

  private printService: GaneshaProWholeLifePrintService = new GaneshaProWholeLifePrintService();

  trxSubmission: TrxSubmission;
  esubmission: Esubmission = new Esubmission();

  agentSignatureBase64: string = null;
  customerSignatureBase64: string = null;
  idCardBase64: string = null;
  progressDialogRef: any;

  constructor(
    private location: Location,
    private router: Router,
    private esubLocalService: EsubLocalService,
    private bundleService: BundleService,
    private ngZone: NgZone,
    private httpService: HttpService,
    public dialog: MatDialog,
    public progressDialog: MatDialog,
    public credentialService: CredentialsService,
    private authenticationService: AuthenticationService
  ) {
    if (this.trxSubmission == undefined) {
      this.trxSubmission = this.bundleService.getData();
      this.esubmission = this.trxSubmission.esubmission;
      this.agentSignatureBase64 = this.trxSubmission.agent_sign_base64;
      this.customerSignatureBase64 = this.trxSubmission.customer_sign_base64;
      this.esubLocalService.getKtpBase64(this.trxSubmission.draft_id, result => {
        this.idCardBase64 = result;
      });
      console.log('TRXSUBMISSION ### ' + JSON.stringify(this.trxSubmission));
    }
    pdfMake.vfs = pdfFonts.pdfMake.vfs;
  }

  ngOnInit() {
    this.openProgress();
    setTimeout(() => {
      this.showPDF();
      this.closeProgress();
    }, 2000);
  }

  onClickBack() {
    if (this.trxSubmission.submissionStatus.id === SUBMITTED_ID || this.trxSubmission.submissionStatus.id === PAID_ID) {
      return;
    }
    this.location.back();
  }

  setSample(service: any) {
    const sign: any = [
      'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAf4AAACWCAYAAAA/v+qmAAAGW0lEQVR4Xu3VAREAAAgCMelf2iA/GzC8Y+cIECBAgACBjMAySQUlQIAAAQIEzvB7AgIECBAgEBIw/KGyRSVAgAABAobfDxAgQIAAgZCA4Q+VLSoBAgQIEDD8foAAAQIECIQEDH+obFEJECBAgIDh9wMECBAgQCAkYPhDZYtKgAABAgQMvx8gQIAAAQIhAcMfKltUAgQIECBg+P0AAQIECBAICRj+UNmiEiBAgAABw+8HCBAgQIBASMDwh8oWlQABAgQIGH4/QIAAAQIEQgKGP1S2qAQIECBAwPD7AQIECBAgEBIw/KGyRSVAgAABAobfDxAgQIAAgZCA4Q+VLSoBAgQIEDD8foAAAQIECIQEDH+obFEJECBAgIDh9wMECBAgQCAkYPhDZYtKgAABAgQMvx8gQIAAAQIhAcMfKltUAgQIECBg+P0AAQIECBAICRj+UNmiEiBAgAABw+8HCBAgQIBASMDwh8oWlQABAgQIGH4/QIAAAQIEQgKGP1S2qAQIECBAwPD7AQIECBAgEBIw/KGyRSVAgAABAobfDxAgQIAAgZCA4Q+VLSoBAgQIEDD8foAAAQIECIQEDH+obFEJECBAgIDh9wMECBAgQCAkYPhDZYtKgAABAgQMvx8gQIAAAQIhAcMfKltUAgQIECBg+P0AAQIECBAICRj+UNmiEiBAgAABw+8HCBAgQIBASMDwh8oWlQABAgQIGH4/QIAAAQIEQgKGP1S2qAQIECBAwPD7AQIECBAgEBIw/KGyRSVAgAABAobfDxAgQIAAgZCA4Q+VLSoBAgQIEDD8foAAAQIECIQEDH+obFEJECBAgIDh9wMECBAgQCAkYPhDZYtKgAABAgQMvx8gQIAAAQIhAcMfKltUAgQIECBg+P0AAQIECBAICRj+UNmiEiBAgAABw+8HCBAgQIBASMDwh8oWlQABAgQIGH4/QIAAAQIEQgKGP1S2qAQIECBAwPD7AQIECBAgEBIw/KGyRSVAgAABAobfDxAgQIAAgZCA4Q+VLSoBAgQIEDD8foAAAQIECIQEDH+obFEJECBAgIDh9wMECBAgQCAkYPhDZYtKgAABAgQMvx8gQIAAAQIhAcMfKltUAgQIECBg+P0AAQIECBAICRj+UNmiEiBAgAABw+8HCBAgQIBASMDwh8oWlQABAgQIGH4/QIAAAQIEQgKGP1S2qAQIECBAwPD7AQIECBAgEBIw/KGyRSVAgAABAobfDxAgQIAAgZCA4Q+VLSoBAgQIEDD8foAAAQIECIQEDH+obFEJECBAgIDh9wMECBAgQCAkYPhDZYtKgAABAgQMvx8gQIAAAQIhAcMfKltUAgQIECBg+P0AAQIECBAICRj+UNmiEiBAgAABw+8HCBAgQIBASMDwh8oWlQABAgQIGH4/QIAAAQIEQgKGP1S2qAQIECBAwPD7AQIECBAgEBIw/KGyRSVAgAABAobfDxAgQIAAgZCA4Q+VLSoBAgQIEDD8foAAAQIECIQEDH+obFEJECBAgIDh9wMECBAgQCAkYPhDZYtKgAABAgQMvx8gQIAAAQIhAcMfKltUAgQIECBg+P0AAQIECBAICRj+UNmiEiBAgAABw+8HCBAgQIBASMDwh8oWlQABAgQIGH4/QIAAAQIEQgKGP1S2qAQIECBAwPD7AQIECBAgEBIw/KGyRSVAgAABAobfDxAgQIAAgZCA4Q+VLSoBAgQIEDD8foAAAQIECIQEDH+obFEJECBAgIDh9wMECBAgQCAkYPhDZYtKgAABAgQMvx8gQIAAAQIhAcMfKltUAgQIECBg+P0AAQIECBAICRj+UNmiEiBAgAABw+8HCBAgQIBASMDwh8oWlQABAgQIGH4/QIAAAQIEQgKGP1S2qAQIECBAwPD7AQIECBAgEBIw/KGyRSVAgAABAobfDxAgQIAAgZCA4Q+VLSoBAgQIEDD8foAAAQIECIQEDH+obFEJECBAgIDh9wMECBAgQCAkYPhDZYtKgAABAgQMvx8gQIAAAQIhAcMfKltUAgQIECBg+P0AAQIECBAICRj+UNmiEiBAgAABw+8HCBAgQIBASMDwh8oWlQABAgQIGH4/QIAAAQIEQgKGP1S2qAQIECBAwPD7AQIECBAgEBIw/KGyRSVAgAABAobfDxAgQIAAgZCA4Q+VLSoBAgQIEHhzJgCXh4cMBQAAAABJRU5ErkJggg==',
      'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAf4AAACWCAYAAAA/v+qmAAAGW0lEQVR4Xu3VAREAAAgCMelf2iA/GzC8Y+cIECBAgACBjMAySQUlQIAAAQIEzvB7AgIECBAgEBIw/KGyRSVAgAABAobfDxAgQIAAgZCA4Q+VLSoBAgQIEDD8foAAAQIECIQEDH+obFEJECBAgIDh9wMECBAgQCAkYPhDZYtKgAABAgQMvx8gQIAAAQIhAcMfKltUAgQIECBg+P0AAQIECBAICRj+UNmiEiBAgAABw+8HCBAgQIBASMDwh8oWlQABAgQIGH4/QIAAAQIEQgKGP1S2qAQIECBAwPD7AQIECBAgEBIw/KGyRSVAgAABAobfDxAgQIAAgZCA4Q+VLSoBAgQIEDD8foAAAQIECIQEDH+obFEJECBAgIDh9wMECBAgQCAkYPhDZYtKgAABAgQMvx8gQIAAAQIhAcMfKltUAgQIECBg+P0AAQIECBAICRj+UNmiEiBAgAABw+8HCBAgQIBASMDwh8oWlQABAgQIGH4/QIAAAQIEQgKGP1S2qAQIECBAwPD7AQIECBAgEBIw/KGyRSVAgAABAobfDxAgQIAAgZCA4Q+VLSoBAgQIEDD8foAAAQIECIQEDH+obFEJECBAgIDh9wMECBAgQCAkYPhDZYtKgAABAgQMvx8gQIAAAQIhAcMfKltUAgQIECBg+P0AAQIECBAICRj+UNmiEiBAgAABw+8HCBAgQIBASMDwh8oWlQABAgQIGH4/QIAAAQIEQgKGP1S2qAQIECBAwPD7AQIECBAgEBIw/KGyRSVAgAABAobfDxAgQIAAgZCA4Q+VLSoBAgQIEDD8foAAAQIECIQEDH+obFEJECBAgIDh9wMECBAgQCAkYPhDZYtKgAABAgQMvx8gQIAAAQIhAcMfKltUAgQIECBg+P0AAQIECBAICRj+UNmiEiBAgAABw+8HCBAgQIBASMDwh8oWlQABAgQIGH4/QIAAAQIEQgKGP1S2qAQIECBAwPD7AQIECBAgEBIw/KGyRSVAgAABAobfDxAgQIAAgZCA4Q+VLSoBAgQIEDD8foAAAQIECIQEDH+obFEJECBAgIDh9wMECBAgQCAkYPhDZYtKgAABAgQMvx8gQIAAAQIhAcMfKltUAgQIECBg+P0AAQIECBAICRj+UNmiEiBAgAABw+8HCBAgQIBASMDwh8oWlQABAgQIGH4/QIAAAQIEQgKGP1S2qAQIECBAwPD7AQIECBAgEBIw/KGyRSVAgAABAobfDxAgQIAAgZCA4Q+VLSoBAgQIEDD8foAAAQIECIQEDH+obFEJECBAgIDh9wMECBAgQCAkYPhDZYtKgAABAgQMvx8gQIAAAQIhAcMfKltUAgQIECBg+P0AAQIECBAICRj+UNmiEiBAgAABw+8HCBAgQIBASMDwh8oWlQABAgQIGH4/QIAAAQIEQgKGP1S2qAQIECBAwPD7AQIECBAgEBIw/KGyRSVAgAABAobfDxAgQIAAgZCA4Q+VLSoBAgQIEDD8foAAAQIECIQEDH+obFEJECBAgIDh9wMECBAgQCAkYPhDZYtKgAABAgQMvx8gQIAAAQIhAcMfKltUAgQIECBg+P0AAQIECBAICRj+UNmiEiBAgAABw+8HCBAgQIBASMDwh8oWlQABAgQIGH4/QIAAAQIEQgKGP1S2qAQIECBAwPD7AQIECBAgEBIw/KGyRSVAgAABAobfDxAgQIAAgZCA4Q+VLSoBAgQIEDD8foAAAQIECIQEDH+obFEJECBAgIDh9wMECBAgQCAkYPhDZYtKgAABAgQMvx8gQIAAAQIhAcMfKltUAgQIECBg+P0AAQIECBAICRj+UNmiEiBAgAABw+8HCBAgQIBASMDwh8oWlQABAgQIGH4/QIAAAQIEQgKGP1S2qAQIECBAwPD7AQIECBAgEBIw/KGyRSVAgAABAobfDxAgQIAAgZCA4Q+VLSoBAgQIEDD8foAAAQIECIQEDH+obFEJECBAgIDh9wMECBAgQCAkYPhDZYtKgAABAgQMvx8gQIAAAQIhAcMfKltUAgQIECBg+P0AAQIECBAICRj+UNmiEiBAgAABw+8HCBAgQIBASMDwh8oWlQABAgQIGH4/QIAAAQIEQgKGP1S2qAQIECBAwPD7AQIECBAgEBIw/KGyRSVAgAABAobfDxAgQIAAgZCA4Q+VLSoBAgQIEHhzJgCXh4cMBQAAAABJRU5ErkJggg==',
      'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAf4AAACWCAYAAAA/v+qmAAAGW0lEQVR4Xu3VAREAAAgCMelf2iA/GzC8Y+cIECBAgACBjMAySQUlQIAAAQIEzvB7AgIECBAgEBIw/KGyRSVAgAABAobfDxAgQIAAgZCA4Q+VLSoBAgQIEDD8foAAAQIECIQEDH+obFEJECBAgIDh9wMECBAgQCAkYPhDZYtKgAABAgQMvx8gQIAAAQIhAcMfKltUAgQIECBg+P0AAQIECBAICRj+UNmiEiBAgAABw+8HCBAgQIBASMDwh8oWlQABAgQIGH4/QIAAAQIEQgKGP1S2qAQIECBAwPD7AQIECBAgEBIw/KGyRSVAgAABAobfDxAgQIAAgZCA4Q+VLSoBAgQIEDD8foAAAQIECIQEDH+obFEJECBAgIDh9wMECBAgQCAkYPhDZYtKgAABAgQMvx8gQIAAAQIhAcMfKltUAgQIECBg+P0AAQIECBAICRj+UNmiEiBAgAABw+8HCBAgQIBASMDwh8oWlQABAgQIGH4/QIAAAQIEQgKGP1S2qAQIECBAwPD7AQIECBAgEBIw/KGyRSVAgAABAobfDxAgQIAAgZCA4Q+VLSoBAgQIEDD8foAAAQIECIQEDH+obFEJECBAgIDh9wMECBAgQCAkYPhDZYtKgAABAgQMvx8gQIAAAQIhAcMfKltUAgQIECBg+P0AAQIECBAICRj+UNmiEiBAgAABw+8HCBAgQIBASMDwh8oWlQABAgQIGH4/QIAAAQIEQgKGP1S2qAQIECBAwPD7AQIECBAgEBIw/KGyRSVAgAABAobfDxAgQIAAgZCA4Q+VLSoBAgQIEDD8foAAAQIECIQEDH+obFEJECBAgIDh9wMECBAgQCAkYPhDZYtKgAABAgQMvx8gQIAAAQIhAcMfKltUAgQIECBg+P0AAQIECBAICRj+UNmiEiBAgAABw+8HCBAgQIBASMDwh8oWlQABAgQIGH4/QIAAAQIEQgKGP1S2qAQIECBAwPD7AQIECBAgEBIw/KGyRSVAgAABAobfDxAgQIAAgZCA4Q+VLSoBAgQIEDD8foAAAQIECIQEDH+obFEJECBAgIDh9wMECBAgQCAkYPhDZYtKgAABAgQMvx8gQIAAAQIhAcMfKltUAgQIECBg+P0AAQIECBAICRj+UNmiEiBAgAABw+8HCBAgQIBASMDwh8oWlQABAgQIGH4/QIAAAQIEQgKGP1S2qAQIECBAwPD7AQIECBAgEBIw/KGyRSVAgAABAobfDxAgQIAAgZCA4Q+VLSoBAgQIEDD8foAAAQIECIQEDH+obFEJECBAgIDh9wMECBAgQCAkYPhDZYtKgAABAgQMvx8gQIAAAQIhAcMfKltUAgQIECBg+P0AAQIECBAICRj+UNmiEiBAgAABw+8HCBAgQIBASMDwh8oWlQABAgQIGH4/QIAAAQIEQgKGP1S2qAQIECBAwPD7AQIECBAgEBIw/KGyRSVAgAABAobfDxAgQIAAgZCA4Q+VLSoBAgQIEDD8foAAAQIECIQEDH+obFEJECBAgIDh9wMECBAgQCAkYPhDZYtKgAABAgQMvx8gQIAAAQIhAcMfKltUAgQIECBg+P0AAQIECBAICRj+UNmiEiBAgAABw+8HCBAgQIBASMDwh8oWlQABAgQIGH4/QIAAAQIEQgKGP1S2qAQIECBAwPD7AQIECBAgEBIw/KGyRSVAgAABAobfDxAgQIAAgZCA4Q+VLSoBAgQIEDD8foAAAQIECIQEDH+obFEJECBAgIDh9wMECBAgQCAkYPhDZYtKgAABAgQMvx8gQIAAAQIhAcMfKltUAgQIECBg+P0AAQIECBAICRj+UNmiEiBAgAABw+8HCBAgQIBASMDwh8oWlQABAgQIGH4/QIAAAQIEQgKGP1S2qAQIECBAwPD7AQIECBAgEBIw/KGyRSVAgAABAobfDxAgQIAAgZCA4Q+VLSoBAgQIEDD8foAAAQIECIQEDH+obFEJECBAgIDh9wMECBAgQCAkYPhDZYtKgAABAgQMvx8gQIAAAQIhAcMfKltUAgQIECBg+P0AAQIECBAICRj+UNmiEiBAgAABw+8HCBAgQIBASMDwh8oWlQABAgQIGH4/QIAAAQIEQgKGP1S2qAQIECBAwPD7AQIECBAgEBIw/KGyRSVAgAABAobfDxAgQIAAgZCA4Q+VLSoBAgQIEHhzJgCXh4cMBQAAAABJRU5ErkJggg==',
      'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mP8z8BQDwAEhQGAhKmMIQAAAABJRU5ErkJggg=='
    ];
    const groupPolicyModel: GroupPolicyModel = new GroupPolicyModel();
    groupPolicyModel.id = 165168186168;
    const calcUnitLinkModel: CalcUnitLinkModel[] = new Array<CalcUnitLinkModel>();
    calcUnitLinkModel.push(new CalcUnitLinkModel());

    const feeSetupModel: FeeSetupModel[] = new Array<FeeSetupModel>();
    feeSetupModel.push(new FeeSetupModel());
    feeSetupModel[0] = {
      id: 1,
      version: null,
      genId: null,
      productId: 200100001000,
      code: 'ADMIN',
      feeType: 'M',
      rate: 17500,
      updateDate: null,
      updateBy: 'default'
    };

    feeSetupModel.push(new FeeSetupModel());
    feeSetupModel[1] = {
      id: 2,
      version: null,
      genId: null,
      productId: 200100001000,
      code: 'ADMIN',
      feeType: 'Q',
      rate: 15000,
      updateDate: null,
      updateBy: 'default'
    };

    feeSetupModel.push(new FeeSetupModel());
    feeSetupModel[2] = {
      id: 3,
      version: null,
      genId: null,
      productId: 200100001000,
      code: 'ADMIN',
      feeType: 'H',
      rate: 12500,
      updateDate: null,
      updateBy: 'default'
    };

    feeSetupModel.push(new FeeSetupModel());
    feeSetupModel[3] = {
      id: 4,
      version: null,
      genId: null,
      productId: 200100001000,
      code: 'ADMIN',
      feeType: 'Y',
      rate: 10000,
      updateDate: null,
      updateBy: 'default'
    };

    const premiumAllocationSetupModel: PremiumAllocationSetupModel[] = new Array<PremiumAllocationSetupModel>();

    premiumAllocationSetupModel.push(new PremiumAllocationSetupModel());
    premiumAllocationSetupModel[0] = {
      id: 1,
      version: null,
      genId: null,
      productId: 200100001000,
      paymentTermFrom: null,
      paymentTermTo: null,
      coverageTermFrom: null,
      coverageTermTo: null,
      policyAgeFrom: null,
      policyAgeTo: null,
      basicPremiumAllocationRate: 0.0,
      singleTopupAllocationRate: 0.96,
      regularTopupAllocationRate: 0.96,
      updateDate: null,
      updateBy: null
    };

    premiumAllocationSetupModel.push(new PremiumAllocationSetupModel());
    premiumAllocationSetupModel[1] = {
      id: 2,
      version: null,
      genId: null,
      productId: 200100001000,
      paymentTermFrom: null,
      paymentTermTo: null,
      coverageTermFrom: null,
      coverageTermTo: null,
      policyAgeFrom: null,
      policyAgeTo: null,
      basicPremiumAllocationRate: 0.7,
      singleTopupAllocationRate: 0.96,
      regularTopupAllocationRate: 0.96,
      updateDate: null,
      updateBy: null
    };

    premiumAllocationSetupModel.push(new PremiumAllocationSetupModel());
    premiumAllocationSetupModel[2] = {
      id: 3,
      version: null,
      genId: null,
      productId: 200100001000,
      paymentTermFrom: null,
      paymentTermTo: null,
      coverageTermFrom: null,
      coverageTermTo: null,
      policyAgeFrom: null,
      policyAgeTo: null,
      basicPremiumAllocationRate: 0.85,
      singleTopupAllocationRate: 0.96,
      regularTopupAllocationRate: 0.96,
      updateDate: null,
      updateBy: null
    };

    premiumAllocationSetupModel.push(new PremiumAllocationSetupModel());
    premiumAllocationSetupModel[3] = {
      id: 4,
      version: null,
      genId: null,
      productId: 200100001000,
      paymentTermFrom: null,
      paymentTermTo: null,
      coverageTermFrom: null,
      coverageTermTo: null,
      policyAgeFrom: null,
      policyAgeTo: null,
      basicPremiumAllocationRate: 1.0,
      singleTopupAllocationRate: 0.96,
      regularTopupAllocationRate: 0.96,
      updateDate: null,
      updateBy: null
    };

    const fundSetupModel: FundSetupModel[] = new Array<FundSetupModel>();
    const ruleAgeSetupModel: RuleAgeSetupModel[] = new Array<RuleAgeSetupModel>();
    const ruleProductSetupModel: RuleProductSetupModel[] = new Array<RuleProductSetupModel>();
    const productSetupModel: ProductSetupModel[] = new Array<ProductSetupModel>();
    const lienClauseSetupModel: LienClauseSetupModel[] = new Array<LienClauseSetupModel>();

    const appInformation: any = {};
    appInformation.version = localStorage.getItem(VERSION_NAME);

    service.setData(
      'id',
      productSetupModel,
      ruleProductSetupModel,
      ruleAgeSetupModel,
      lienClauseSetupModel,
      groupPolicyModel,
      fundSetupModel,
      feeSetupModel,
      premiumAllocationSetupModel,
      null,
      sign,
      10,
      10,
      appInformation
    );
  }

  showPDF() {
    this.setSample(this.printService);
    let docDefinition = this.printService.asuransiPitraYadnyaLayoutPrintService.getTemplate(
      this.type,
      this.trxSubmission,
      this.customerSignatureBase64,
      this.agentSignatureBase64
    );

    pdfMake.createPdf(docDefinition).getBuffer((dataURL: any) => {
      this.ngZone.run(() => {
        // this.isFinishGeneratePdf=true;
        this.base64Src = new Uint8Array(dataURL);
        this.base64String = btoa(String.fromCharCode(...new Uint8Array(dataURL)));
        // this.spinner.hide();
      });
    });
  }

  async submit() {
    if (this.trxSubmission.submissionStatus.id === SUBMITTED_ID || this.trxSubmission.submissionStatus.id === PAID_ID) {
      this.esubLocalService.saveEsubmission(this.trxSubmission.draft_id, this.trxSubmission, result => {});
      this.router.navigate(['/'], { replaceUrl: true });
      return;
    }
    this.openProgress();

    let httpHeaders = new HttpHeaders({
      'X-IBM-Client-Id': X_IBM_CLIENT_ID,
      'X-IBM-Client-secret': X_IBM_CLIENT_SECRET,
      Authorization: this.credentialService.credentials.authorization
    });

    let httpOptions = {
      headers: httpHeaders
    };

    // let ktpFile = await imageCompression.getFilefromDataUrl(this.trxSubmission.ktp_base64);
    // console.log(`ktpFile size ${ktpFile.size / 1024 / 1024} MB`); // smaller than maxSizeMB
    // const compressedKtpFile = await imageCompression(ktpFile, options);
    // console.log('compressedFile instanceof Blob', compressedKtpFile instanceof Blob); // true
    // console.log(`compressedFile size ${compressedKtpFile.size / 1024 / 1024} MB`); // smaller than maxSizeMB
    // let ktpBase64 = await imageCompression.getDataUrlFromFile(compressedKtpFile);

    var formData = new FormData();

    if (this.esubmission.tertanggung) {
      this.esubmission.tertanggung.forEach(value => {
        if (value.hubungan === 'Kepala Keluarga') {
          value.hubungan = KPLKGA;
        }
      });
    }

    this.trxSubmission.esubmission.tgl_pengajuan = moment(new Date()).format('DD-MM-YYYY HH:MM:SS');
    formData.append('imei', localStorage.getItem(IMEI));
    formData.append('spajNo', this.trxSubmission.spajNo);
    formData.append('noVa', '0');
    formData.append('version', localStorage.getItem(VERSION_CODE).toString());
    formData.append('groupPolicyId', '1');
    formData.append('agentCode', this.credentialService.credentials.username);
    formData.append('agentName', this.credentialService.credentials.username);
    formData.append('officeName', this.credentialService.credentials.username);

    formData.append('json', JSON.stringify(this.esubmission));
    formData.append('spajPdfBase64', encodeURI('data:application/pdf;base64,' + this.base64String));
    formData.append('idCardBase64', encodeURI(this.trxSubmission.ktp_base64));

    const $service = this.httpService.post(SUBMIT_ESUB, formData, httpOptions);
    $service.pipe().subscribe(
      (value: any) => {
        this.closeProgress();
        console.log('RESPONSE ' + JSON.stringify(value));
        if (value.status == 200) {
          let esubId = value.data.id;
          this.trxSubmission.id = esubId;
          this.trxSubmission.submissionStatus = new MstStatus();
          this.trxSubmission.submissionStatus.id = SUBMITTED_ID;
          this.trxSubmission.submissionStatus.label = SUBMITTED_LABEL;
          this.trxSubmission.submissionStatusJson = JSON.stringify(this.trxSubmission.submissionStatus);

          this.esubLocalService.updateIdAndSubmissionStatusJson(
            this.trxSubmission.draft_id,
            esubId,
            this.trxSubmission.submissionStatus,
            result => {}
          );
          this.router.navigate(['/'], { replaceUrl: true });
          this.router.dispose();
        } else if (value.status == 500) {
          this.openDialog('Session expired', 'Anda harus login kembali untuk melanjutkan aktifitas', () => {
            this.authenticationService.logout().subscribe(() => this.router.navigate(['/login'], { replaceUrl: true }));
          });
        } else {
          this.openDialog('Submit Gagal', value.data.message, () => {});
        }
      },
      error => {
        console.log(error);

        this.closeProgress();
        if (error.status == 0) {
          this.openDialog('Network Problem', 'Mohon periksa koneksi internet anda', () => {});
        } else if (error.status == 500 && error.error.moreInformation === 'Failed to establish a backside connection') {
          this.openDialog('Connection Timeout', "Server doesn't reply, please try again later", () => {});
        } else if (error.status == 401 && error.error.message === 'token.has.expired') {
          this.openDialog('Session expired', 'Anda harus login kembali untuk melanjutkan aktifitas', () => {
            this.authenticationService.logout().subscribe(() => this.router.navigate(['/login'], { replaceUrl: true }));
          });
        } else {
          this.openDialog('Submit Gagal', error.error.message, () => {});
        }
      },
      () => {}
    );
  }

  openDialog(title: string, message: string, myCallback: () => void): void {
    const dialogRef = this.dialog.open(AlertComponent, {
      width: '80%',
      data: { title: title, message: message }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (myCallback) {
        myCallback();
      }
    });
  }

  openProgress(): void {
    this.progressDialogRef = this.progressDialog.open(LoaderComponent, {
      width: '20%',
      data: {}
    });
    this.progressDialogRef.disableClose = true;

    this.progressDialogRef.afterClosed().subscribe((result: any) => {
      console.log('The dialog was closed');
    });
  }

  closeProgress() {
    if (this.progressDialogRef) {
      this.progressDialog.closeAll();
    }
  }
}
