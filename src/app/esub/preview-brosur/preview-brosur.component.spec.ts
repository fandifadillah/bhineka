import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreviewBrosurComponent } from './preview-brosur.component';

describe('PreviewBrosurComponent', () => {
  let component: PreviewBrosurComponent;
  let fixture: ComponentFixture<PreviewBrosurComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PreviewBrosurComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviewBrosurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
