import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';

import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/material.module';
import { PreviewBrosurComponent } from '@app/esub/preview-brosur/preview-brosur.component';
import { PreviewBrosurRoutingModule } from '@app/esub/preview-brosur/preview-brosur-routing.module';
import { PdfViewerModule } from 'ng2-pdf-viewer';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    TranslateModule,
    SharedModule,
    FlexLayoutModule,
    MaterialModule,
    PdfViewerModule,
    FormsModule,
    PreviewBrosurRoutingModule
  ],
  declarations: [PreviewBrosurComponent]
})
export class PreviewBrosurModule {}
