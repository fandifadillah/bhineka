import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { extract } from '@app/core';
import { PreviewBrosurComponent } from '@app/esub/preview-brosur/preview-brosur.component';

const routes: Routes = [
  { path: 'esub/preview-brosur', component: PreviewBrosurComponent, data: { title: extract('Preview Brosur') } }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class PreviewBrosurRoutingModule {}
