import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { extract } from '@app/core';
import { PolisDeliveryComponent } from '@app/esub/polis-delivery/polis-delivery.component';

const routes: Routes = [
  { path: 'esub/polis-delivery', component: PolisDeliveryComponent, data: { title: extract('Polis Delivery') } }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class PolisDeliveryRoutingModule {}
