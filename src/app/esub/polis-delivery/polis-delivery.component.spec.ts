import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PolisDeliveryComponent } from './polis-delivery.component';

describe('PolisDeliveryComponent', () => {
  let component: PolisDeliveryComponent;
  let fixture: ComponentFixture<PolisDeliveryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PolisDeliveryComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PolisDeliveryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
