import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Esubmission, TrxSubmission } from '@app/model/esub/transaction';
import { EsubLocalService } from '@app/platform/dao/local/esub-local.service';
import { EMAIL, SMS } from '@app/app.config';
import { BundleService } from '@app/platform/dao/local/BundleService';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-polis-delivery',
  templateUrl: './polis-delivery.component.html',
  styleUrls: ['./polis-delivery.component.scss']
})
export class PolisDeliveryComponent implements OnInit {
  public polisDeliveryForm: FormGroup;

  trxSubmission: TrxSubmission;
  esubmission: Esubmission = new Esubmission();

  polisDeliveryWarning: string;

  constructor(
    private location: Location,
    private router: Router,
    private formBuilder: FormBuilder,
    private bundleService: BundleService,
    private esubLocalService: EsubLocalService
  ) {
    if (this.trxSubmission == undefined) {
      this.trxSubmission = this.bundleService.getData();
      this.esubmission = JSON.parse(this.trxSubmission.json);
    }
    this.createForm();
  }

  ngOnInit() {
    if (this.esubmission.pilihan_cetak === EMAIL) {
      this.onSelectEmail();
    }
    if (this.esubmission.pilihan_cetak === SMS) {
      this.onSelectSMS();
    }
  }

  onClickNext() {
    if (!this.esubmission.pilihan_cetak) {
      this.polisDeliveryWarning = 'Mohon pilih salah satu metode pengiriman polis';
      return;
    }
    this.esubmission.epolicy_wa = '';

    this.trxSubmission.esubmission = this.esubmission;
    this.trxSubmission.json = JSON.stringify(this.esubmission);
    this.bundleService.setData(this.trxSubmission);
    this.router.navigate(['/esub/preview-data'], { replaceUrl: false });
    if (this.trxSubmission.spajNo != null && this.trxSubmission.spajNo != '') {
      this.esubLocalService.updateEsubmission(this.trxSubmission.draft_id, this.esubmission, result => {});
    }
  }

  onClickBack() {
    this.location.back();
  }

  onSelectEmail() {
    this.esubmission.pilihan_cetak = EMAIL;
    this.polisDeliveryWarning = '';
    this.createValidateEmailForm();
  }
  onSelectSMS() {
    this.esubmission.pilihan_cetak = SMS;
    this.polisDeliveryWarning = '';
    this.createValidateSMSForm();
  }

  onEditNoHandphone(value: string) {
    if (value.length > 13) {
      value = value.substr(0, 13);
    }
    if (!value.startsWith('62')) {
      if (value === '6') {
        value = value + '2';
      } else {
        value = '62' + value;
      }
    }

    // if(value.length<=2){
    //   this.polisHolderForm.controls.mobilePhone.setErrors({incorrect:true})
    //   return ;
    // }
    this.polisDeliveryForm.controls.sms.setValue(value);
    this.esubmission.no_hp = value;
    // this.esubmission.epolicy_sms = value;
  }

  private createForm() {
    this.polisDeliveryForm = this.formBuilder.group({
      email: ['', Validators.email],
      sms: ['']
    });
  }

  private createValidateEmailForm() {
    let email = this.esubmission.epolicy_email;
    if (!email) {
      email = '';
    }
    let sms = this.esubmission.epolicy_sms;
    if (!sms || sms == '') {
      sms = this.esubmission.no_hp;
    }
    if (!sms) {
      sms = '';
    }
    this.polisDeliveryForm = this.formBuilder.group({
      email: [email, Validators.compose([Validators.email, Validators.required])],
      sms: [sms]
    });
    this.polisDeliveryForm.setErrors({ invalidMobilePhone: false });
  }

  private createValidateSMSForm() {
    let email = this.esubmission.epolicy_email;
    if (!email) {
      email = '';
    }
    let sms = this.esubmission.epolicy_sms;
    if (!sms || sms == '') {
      sms = this.esubmission.no_hp;
    }
    if (!sms) {
      sms = '62';
    }
    this.polisDeliveryForm = this.formBuilder.group(
      {
        email: [email, Validators.compose([Validators.email])],
        sms: [sms]
      },
      {
        validator: this.validator
      }
    );
    if (sms.length <= 2) {
      this.polisDeliveryForm.setErrors({ invalidMobilePhone: true });
    } else {
      this.polisDeliveryForm.setErrors({ invalidMobilePhone: false });
    }
  }

  validator(form: FormGroup) {
    let invalidMobilePhone = false;
    if (form.controls.sms.value.length <= 2) {
      invalidMobilePhone = true;
    }
    return invalidMobilePhone ? { invalidMobilePhone: true } : false;
  }
}
