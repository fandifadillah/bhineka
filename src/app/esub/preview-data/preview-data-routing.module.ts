import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { extract } from '@app/core';
import { PreviewDataComponent } from '@app/esub/preview-data/preview-data.component';

const routes: Routes = [
  { path: 'esub/preview-data', component: PreviewDataComponent, data: { title: extract('Preview Data') } }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class PreviewDataRoutingModule {}
