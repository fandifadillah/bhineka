import { Component, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { EsubLocalService } from '@app/platform/dao/local/esub-local.service';
import { Esubmission, Tertanggung, TrxSubmission } from '@app/model/esub/transaction';
import { EMAIL } from '@app/app.config';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BundleService } from '@app/platform/dao/local/BundleService';
import { MatDialog } from '@angular/material';
import { AlertComponent } from '@app/shared/dialog/alert/alert.component';
import * as moment from 'moment';

// import {SignaturePad} from 'angular2-signaturepad'
// (<any>window).SignaturePad = require('signature_pad').default

export interface Insured {
  fullname: string;
  birthdate: string;
  familyRelationship: string;
}

@Component({
  selector: 'app-preview-data',
  templateUrl: './preview-data.component.html',
  styleUrls: ['./preview-data.component.scss']
})
export class PreviewDataComponent implements OnInit {
  moment = moment;

  public agreementForm: FormGroup;

  trxSubmission: TrxSubmission;
  esubmission: Esubmission = new Esubmission();

  @ViewChild('esignAgent', { static: false }) esignAgent: SignaturePad;
  @ViewChild('esignCustomer', { static: false }) esignCustomer: SignaturePad;

  esignAgentBase64: string;
  esignCustomerBase64: string;

  currentdateString: String = null;

  public signaturePadOptions: Object = {
    minWidth: 0.5,
    canvasWidth: 510,
    canvasHeight: 150
  };

  signatureImage: any;

  expandDisclaimer1Mode = true;
  expandDisclaimer2Mode = false;

  public label: string = 'Sign Above';
  public width: number = 300;
  public height: number = 300;

  agentCode: any;
  agentName: any;

  displayedColumns: string[] = ['fullname', 'birthdate', 'familyRelationship'];
  insureds: Tertanggung[] = new Array();

  constructor(
    private location: Location,
    private router: Router,
    private esubLocalService: EsubLocalService,
    private bundleService: BundleService,
    private formBuilder: FormBuilder,
    public dialog: MatDialog
  ) {
    this.trxSubmission = this.bundleService.getData();
    console.log(this.trxSubmission);
    if (!this.trxSubmission) {
      this.trxSubmission = new TrxSubmission();
      this.esubmission = new Esubmission();
    } else {
      this.esubmission = this.trxSubmission.esubmission;
    }
    if (!this.esubmission) {
      this.insureds = new Array();
    } else {
      this.insureds = this.esubmission.tertanggung;
    }
    this.esubLocalService.getCustomerSignatureBase64(this.trxSubmission.draft_id, result => {
      this.esignCustomerBase64 = result;
      if (this.esignCustomerBase64) {
        this.esignCustomer.fromDataURL(this.esignCustomerBase64);
      }
    });
    setTimeout(() => {
      this.esubLocalService.getAgentSignatureBase64(this.trxSubmission.draft_id, result => {
        this.esignAgentBase64 = result;
        if (this.esignAgentBase64) {
          this.esignAgent.fromDataURL(this.esignAgentBase64);
        }
      });
    }, 1000);
    this.currentdateString = this.currentDate();

    this.createForm();
  }

  ngOnInit() {
    // this.scroll("pael")
  }

  toggleDisclaimer1() {
    this.expandDisclaimer1Mode = !this.expandDisclaimer1Mode;
  }

  toggleDisclaimer2() {
    this.expandDisclaimer2Mode = !this.expandDisclaimer2Mode;
  }

  showImage(data: any) {
    this.signatureImage = data;
  }

  onClickBack() {
    this.location.back();
  }

  onClickNext() {
    if (this.esignCustomer.isEmpty()) {
      this.openDialog('Kelengkapan Data', 'Mohon lakukan tanda tangan calon customer terlebih dahulu');
      return;
    }
    if (this.esignAgent.isEmpty()) {
      this.openDialog('Kelengkapan Data', 'Mohon lakukan tanda tangan agent terlebih dahulu');
      return;
    }
    this.esignAgentBase64 = this.esignAgent.toDataURL();
    this.esignCustomerBase64 = this.esignCustomer.toDataURL();
    console.log('AGENT SIGNATURE ' + this.esignAgent.toDataURL());
    console.log('CUSTOMER SIGNATURE ' + this.esignCustomer.toDataURL());

    this.trxSubmission.agent_sign_base64 = this.esignAgentBase64;
    this.trxSubmission.customer_sign_base64 = this.esignCustomerBase64;
    this.bundleService.setData(this.trxSubmission);
    if (!this.esubmission.customer_signature_date) {
      this.esubmission.customer_signature_date = new Date();
    }
    if (!this.esubmission.agent_signature_date) {
      this.esubmission.agent_signature_date = new Date();
    }
    if (this.trxSubmission.spajNo) {
      this.esubLocalService.updateSignatureAndEsubmission(
        this.trxSubmission.draft_id,
        this.esignAgentBase64,
        this.esignCustomerBase64,
        this.esubmission,
        result => {}
      );
    }
    this.router.navigate(['/esub/preview-brosur'], { replaceUrl: false });
  }

  onClearHandler() {
    console.log('onclear clicked...');
  }

  onSaveHandler(data: any) {
    console.log('onsave clicked');
    console.log(data);
    window.open(data);
  }

  isPolicySendThroughEmail() {
    if (this.trxSubmission.esubmission.epolicy_wa === EMAIL) {
      return true;
    }
    return false;
  }

  currentDate() {
    return this.moment(new Date()).format('DD-MM-YYYY HH:MM:SS');
  }

  private createForm() {
    this.agreementForm = this.formBuilder.group({});
  }

  openDialog(title: string, message: string): void {
    const dialogRef = this.dialog.open(AlertComponent, {
      width: '80%',
      data: { title: title, message: message }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  agentTakingSignature() {
    this.esubmission.agent_signature_date = new Date();
  }
  customerTakingSignature() {
    this.esubmission.customer_signature_date = new Date();
  }
}
