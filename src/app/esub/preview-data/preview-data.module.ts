import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';

import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/material.module';
import { PolisHolderRoutingModule } from '@app/esub/polis-holder/polis-holder-routing.module';
import { PolisHolderComponent } from '@app/esub/polis-holder/polis-holder.component';
import { PolisDeliveryRoutingModule } from '@app/esub/polis-delivery/polis-delivery-routing.module';
import { PolisDeliveryComponent } from '@app/esub/polis-delivery/polis-delivery.component';
import { PreviewDataRoutingModule } from '@app/esub/preview-data/preview-data-routing.module';
import { PreviewDataComponent } from '@app/esub/preview-data/preview-data.component';
import { SignaturePadModule } from 'angular2-signaturepad';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    TranslateModule,
    SharedModule,
    FlexLayoutModule,
    MaterialModule,
    PreviewDataRoutingModule,
    SignaturePadModule
  ],
  declarations: [PreviewDataComponent]
})
export class PreviewDataModule {}
