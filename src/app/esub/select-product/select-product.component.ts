import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
import { Product, ProductDao } from '@app/platform/dao/sqlite/ProductDao';
import { Package, PackageDao } from '@app/platform/dao/sqlite/PackageDao';
import { TrxSubmission } from '@app/model/esub/transaction';
import { EsubLocalService } from '@app/platform/dao/local/esub-local.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RoutingState } from '@app/core/RoutingState';
import { BundleService } from '@app/platform/dao/local/BundleService';
import { MatDialog } from '@angular/material';
import { LoaderComponent } from '@app/shared';

@Component({
  selector: 'app-select-product',
  templateUrl: './select-product.component.html',
  styleUrls: ['./select-product.component.scss']
})
export class SelectProductComponent implements OnInit {
  public productForm: FormGroup;
  products: Product[];
  product: Product;

  packazes: Package[];
  packaze: Package;
  progressDialogRef: any = null;

  trxSubmission: TrxSubmission;

  @ViewChild('productSelect', { static: false }) productSelect: HTMLInputElement;

  constructor(
    private router: Router,
    private location: Location,
    private productDao: ProductDao,
    private packageDao: PackageDao,
    private formBuilder: FormBuilder,
    private bundleService: BundleService,
    private esubLocalService: EsubLocalService,
    public progressDialog: MatDialog
  ) {
    this.trxSubmission = this.bundleService.getData();
    this.openProgress();
    this.productDao.getProducts(result => {
      this.products = result;
      this.closeProgress();
      if (this.trxSubmission.esubmission.produk != null && this.trxSubmission.esubmission.produk != '') {
        this.onClickProduct(this.trxSubmission.esubmission.produk);
      }
    });
    this.createForm();
  }

  ngOnInit() {}

  onClickProduct(name: string) {
    this.openProgress();
    let selectedProduct = this.products.find(value => {
      if (value.name == name) {
        return true;
      }
    });
    this.packageDao.getPackages(selectedProduct.product_id, result => {
      this.packaze = null;
      this.packazes = result;
      this.closeProgress();
    });
  }

  showImage() {
    if (this.trxSubmission.esubmission.produk) {
    } else {
    }
  }

  onClickBack() {
    this.location.back();
  }
  onClickNext() {
    let packaze = this.packazes.find((value, index) => {
      if (value.unit_name == this.trxSubmission.esubmission.pilihan_paket) {
        return true;
      }
    });
    this.trxSubmission.esubmission.premi = packaze.total_premium.toString();
    this.trxSubmission.json = JSON.stringify(this.trxSubmission.esubmission);

    this.bundleService.setData(this.trxSubmission);
    this.router.navigate(['/esub/polis-holder'], { replaceUrl: false });
  }

  private createForm() {
    this.productForm = this.formBuilder.group({
      productSelect: ['', Validators.required],
      packageSelect: ['', Validators.required]
    });
  }

  openProgress(): void {
    if (this.progressDialogRef == null) {
      this.progressDialogRef = this.progressDialog.open(LoaderComponent, {
        data: {}
      });
      this.progressDialogRef.afterClosed().subscribe((result: any) => {
        console.log('The dialog was closed');
      });
    }
  }

  closeProgress() {
    if (this.progressDialogRef) {
      this.progressDialog.closeAll();
      this.progressDialogRef = null;
    }
  }
}
