import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { extract } from '@app/core';
import { FilterComponent } from '@app/home/filter/filter.component';
import { SelectProductComponent } from '@app/esub/select-product/select-product.component';

const routes: Routes = [
  { path: 'esub/select-product', component: SelectProductComponent, data: { title: extract('Select Product') } }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class SelectProductRoutingModule {}
