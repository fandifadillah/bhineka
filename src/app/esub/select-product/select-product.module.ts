import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';

import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/material.module';
import { SelectProductComponent } from '@app/esub/select-product/select-product.component';
import { SelectProductRoutingModule } from '@app/esub/select-product/select-product-routing.module';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    TranslateModule,
    SharedModule,
    FlexLayoutModule,
    MaterialModule,
    FormsModule,
    SelectProductRoutingModule
  ],
  declarations: [SelectProductComponent]
})
export class SelectProductModule {}
