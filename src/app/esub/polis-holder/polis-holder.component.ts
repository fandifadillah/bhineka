import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';
import { City, District, Province, RegionalDao, Village } from '@app/platform/dao/sqlite/RegionalDao';
import { EsubLocalService } from '@app/platform/dao/local/esub-local.service';
import { Esubmission, TrxSubmission } from '@app/model/esub/transaction';
import { FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { JobDao } from '@app/platform/dao/sqlite/JobDao';
import { BundleService } from '@app/platform/dao/local/BundleService';
import { MatDialog } from '@angular/material';
import { LoaderComponent } from '@app/shared';
import imageCompression from 'browser-image-compression';
import { AlertComponent } from '@app/shared/dialog/alert/alert.component';

const compressOptions = {
  maxSizeMB: 0.5, // (default: Number.POSITIVE_INFINITY)
  maxWidthOrHeight: 500, // compressedFile will scale down by ratio to a point that width or height is smaller than maxWidthOrHeight (default: undefined)
  useWebWorker: true, // optional, use multi-thread web worker, fallback to run in main-thread (default: true)
  maxIteration: 10 // optional, max number of iteration to compress the image (default: 10)
};

var options: any = {
  quality: 90,
  destinationType: 0,
  sourceType: 1,
  encodingType: 0,
  mediaType: 0,
  allowEdit: true,
  correctOrientation: true
};

@Component({
  selector: 'app-polis-holder',
  templateUrl: './polis-holder.component.html',
  styleUrls: ['./polis-holder.component.scss']
})
export class PolisHolderComponent implements OnInit {
  public imageSrc: any;

  public polisHolderForm: FormGroup;

  trxSubmission: TrxSubmission;
  esubmission: Esubmission = new Esubmission();

  jobs: string[] = new Array();
  job: string;

  provinces: Province[];
  province: Province;

  cities: City[];
  city: City;

  districts: District[];
  district: District;

  villages: Village[];
  village: Village;

  progressDialogRef: any = null;

  @ViewChild('provinceSelect', { static: false }) provinceSelect: HTMLInputElement;

  constructor(
    private location: Location,
    private router: Router,
    private sanitizer: DomSanitizer,
    private regionalDao: RegionalDao,
    private jobDao: JobDao,
    private bundleService: BundleService,
    private formBuilder: FormBuilder,
    private esubLocalService: EsubLocalService,
    public progressDialog: MatDialog,
    public dialog: MatDialog
  ) {
    this.trxSubmission = this.bundleService.getData();
    this.esubmission = JSON.parse(this.trxSubmission.json);
    if (this.esubmission.no_hp === '') {
      this.esubmission.no_hp = '62';
    }
    this.job = this.esubmission.pekerjaan;
    this.jobDao.getJobs(result => {
      this.jobs = result;
    });
    setTimeout(() => {
      this.regionalDao.getProvinces(result => {
        this.provinces = result;
        if (this.trxSubmission.esubmission.propinsi != null) {
          this.onClickProvince(this.trxSubmission.esubmission.propinsi);
        }
      });
    }, 2000);
    setTimeout(() => {
      this.esubLocalService.getKtpBase64(this.trxSubmission.draft_id, result => {
        if (result) {
          this.imageSrc = result;
        }
      });
    }, 4000);
    this.createForm();
  }

  ngOnInit() {}

  onClickBack() {
    this.location.back();
  }

  onClickProvince(provinceName: string) {
    let selectedProvince = this.provinces.find(value => {
      if (value.nama_propinsi == provinceName) {
        return true;
      }
      return false;
    });
    if (selectedProvince) {
      this.regionalDao.getCities(selectedProvince.kd_propinsi, result => {
        this.cities = result;
        if (this.trxSubmission.esubmission.kota != null) {
          this.onClickCity(this.trxSubmission.esubmission.kota);
        }
      });
    } else {
      this.cities = new Array();
      this.districts = new Array();
      this.villages = new Array();
      this.esubmission.kota = null;
      this.esubmission.kecamatan = null;
      this.esubmission.kelurahan = null;
      this.esubmission.kode_pos = null;
    }
  }

  onClickCity(cityName: string) {
    let selectedCity = this.cities.find(value => {
      if (value.nama_kota == cityName) {
        return true;
      }
      return false;
    });
    if (selectedCity) {
      this.regionalDao.getDistricts(selectedCity.kd_kota_1, result => {
        this.districts = result;
        if (this.trxSubmission.esubmission.kecamatan) {
          this.onClickDistrict(this.trxSubmission.esubmission.kecamatan);
        }
      });
    } else {
      this.districts = new Array();
      this.villages = new Array();
      this.esubmission.kecamatan = null;
      this.esubmission.kelurahan = null;
      this.esubmission.kode_pos = null;
    }
  }
  onClickDistrict(districtName: string) {
    let selectedDistrict = this.districts.find(value => {
      if (value.nama_kecamatan == districtName) {
        return true;
      }
      return false;
    });
    if (selectedDistrict) {
      this.regionalDao.getVillages(selectedDistrict.kd_kecamatan, result => {
        this.villages = result;
      });
    } else {
      this.villages = new Array();
      this.esubmission.kelurahan = null;
      this.esubmission.kode_pos = null;
    }
  }

  async onClickNext() {
    if (!this.imageSrc || this.imageSrc == null || this.imageSrc == undefined) {
      this.openDialog('Kelengkapan Data', 'Foto KTP harus diisi', () => {});
      return;
    }
    this.esubmission.nama_pempol = this.esubmission.nama;
    this.esubmission.epolicy_sms = this.esubmission.no_hp;
    this.esubmission.no_agen = this.trxSubmission.agentCode;
    this.trxSubmission.spajNo = this.esubmission.no_ktp;

    // if (this.esubmission.no_hp.length <= 2) {
    //   this.polisHolderForm.controls.mobilePhone.setErrors({ incorrect: true });
    //   return;
    // }

    // let ktpFile = await imageCompression.getFilefromDataUrl(this.imageSrc);
    // console.log(`ktpFile size ${ktpFile.size / 1024 / 1024} MB`); // smaller than maxSizeMB
    // const compressedKtpFile = await imageCompression(ktpFile, compressOptions);
    // console.log('compressedFile instanceof Blob', compressedKtpFile instanceof Blob); // true
    // console.log(`compressedFile size ${compressedKtpFile.size / 1024 / 1024} MB`); // smaller than maxSizeMB
    // let ktpBase64 = await imageCompression.getDataUrlFromFile(compressedKtpFile);

    this.trxSubmission.ktp_base64 = this.imageSrc;
    this.trxSubmission.esubmission = this.esubmission;
    this.trxSubmission.json = JSON.stringify(this.esubmission);

    if (this.trxSubmission.spajNo) {
      this.esubLocalService.saveEsubmission(this.trxSubmission.draft_id, this.trxSubmission, result => {});
      this.bundleService.setData(this.trxSubmission);
      this.router.navigate(['/esub/insured'], { replaceUrl: false });
    }
  }

  // onNameChanged(event : any){
  //   // event.target.value = event.target.value.replace(/[^A-Za-z ]/g, "")
  //   const inputElement = document.getElementById('name') as HTMLInputElement;
  //   this.esubmission.nama = event.target.value.replace(/[^A-Za-z ]/g, "")
  //   inputElement.value = this.esubmission.nama
  //   console.log(inputElement.value)
  // }

  update(): void {
    if (!this.polisHolderForm.controls.spajNo.invalid) {
      this.esubLocalService.updateEsubmission(this.trxSubmission.draft_id, this.esubmission, result => {});
    }
  }
  onEditKtpNumber(value: string) {
    console.log('VALUE  ' + value);
    if (value.length > 16) {
      value = value.substr(0, 16);
    }

    this.polisHolderForm.controls.spajNo.setValue(value);
    this.esubmission.no_ktp = value;
    this.esubLocalService.updateSpajNo(this.trxSubmission.draft_id, this.esubmission.no_ktp, result => {});
    this.update();
  }

  onEditPostalCode(value: string) {
    console.log('VALUE  ' + value);
    if (value.length > 5) {
      value = value.substr(0, 5);
    }
    this.polisHolderForm.controls.postalCode.setValue(value);
    this.esubmission.kode_pos = value;
    this.update();
  }

  onEditNoHandphone(value: string) {
    if (value.length > 13) {
      value = value.substr(0, 13);
    }
    if (!value.startsWith('62')) {
      if (value === '6') {
        value = value + '2';
      } else {
        value = '62' + value;
      }
    }

    // if(value.length<=2){
    //   this.polisHolderForm.controls.mobilePhone.setErrors({incorrect:true})
    //   return ;
    // }

    this.polisHolderForm.controls.mobilePhone.setValue(value);
    this.esubmission.no_hp = value;
    this.update();
  }

  onEditYearlyIncome(value: string) {
    if (value.length > 15) {
      value = value.substr(0, 5);
    }
    this.polisHolderForm.controls.yearlyIncome.setValue(value);
    this.esubmission.penghasilan_per_tahun = value;
    this.update();
  }

  onClickCamera() {
    (<any>window).navigator.camera.getPicture(
      (cameraSuccess: any) => {
        console.log('CAMERA RETURN ' + cameraSuccess);
        this.imageSrc = 'data:image/jpeg;base64,' + cameraSuccess;
        const imageElement = document.getElementById('ktpImage') as HTMLImageElement;
        imageElement.src = this.imageSrc;
        this.compressAndSaveImage();
      },
      (error: any) => {},
      options
    );
  }

  async compressAndSaveImage() {
    let ktpFile = await imageCompression.getFilefromDataUrl(this.imageSrc);
    console.log(`ktpFile size ${ktpFile.size / 1024 / 1024} MB`); // smaller than maxSizeMB
    const compressedKtpFile = await imageCompression(ktpFile, compressOptions);
    console.log('compressedFile instanceof Blob', compressedKtpFile instanceof Blob); // true
    console.log(`compressedFile size ${compressedKtpFile.size / 1024 / 1024} MB`); // smaller than maxSizeMB
    let ktpBase64 = await imageCompression.getDataUrlFromFile(compressedKtpFile);
    this.esubLocalService.updateKtpBase64(this.trxSubmission.draft_id, ktpBase64, result => {});
  }

  validator(form: FormGroup) {
    let invalidMobilePhone = false;
    if (form.controls.mobilePhone.value.length <= 2) {
      invalidMobilePhone = true;
    }
    return invalidMobilePhone ? { invalidMobilePhone: true } : false;
  }

  private createForm() {
    this.polisHolderForm = this.formBuilder.group(
      {
        spajNo: ['', Validators.required],
        name: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-Z ]*')])],
        address: ['', Validators.required],
        provinceSelect: ['', Validators.required],
        citySelect: ['', Validators.required],
        districtSelect: ['', Validators.required],
        villageSelect: ['', Validators.required],
        postalCode: ['', Validators.compose([Validators.maxLength(5)])],
        mobilePhone: [''],
        email: ['', [Validators.email]],
        jobSelect: [''],
        yearlyIncome: ['']
      }
      // ,
      // {
      //   validator: this.validator
      // }
    );
  }

  openDialog(title: string, message: string, myCallback: () => void): void {
    const dialogRef = this.dialog.open(AlertComponent, {
      width: '80%',
      data: { title: title, message: message }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (myCallback) {
        myCallback();
      }
    });
  }

  openProgress(): void {
    if (this.progressDialogRef == null) {
      this.progressDialogRef = this.progressDialog.open(LoaderComponent, {
        data: {}
      });
      this.progressDialogRef.afterClosed().subscribe((result: any) => {
        console.log('The dialog was closed');
      });
    }
  }

  closeProgress() {
    if (this.progressDialogRef) {
      this.progressDialog.closeAll();
      this.progressDialogRef = null;
    }
  }
}
