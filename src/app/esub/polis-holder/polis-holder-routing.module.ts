import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { extract } from '@app/core';
import { PolisHolderComponent } from '@app/esub/polis-holder/polis-holder.component';

const routes: Routes = [
  { path: 'esub/polis-holder', component: PolisHolderComponent, data: { title: extract('Polis Holder') } }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class PolisHolderRoutingModule {}
