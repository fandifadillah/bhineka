import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';

import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/material.module';
import { PolisHolderRoutingModule } from '@app/esub/polis-holder/polis-holder-routing.module';
import { PolisHolderComponent } from '@app/esub/polis-holder/polis-holder.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    TranslateModule,
    SharedModule,
    FlexLayoutModule,
    MaterialModule,
    FormsModule,
    PolisHolderRoutingModule
  ],
  declarations: [PolisHolderComponent]
})
export class PolisHolderModule {}
