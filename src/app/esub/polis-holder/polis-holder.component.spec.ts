import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PolisHolderComponent } from './polis-holder.component';

describe('PolisHolderComponent', () => {
  let component: PolisHolderComponent;
  let fixture: ComponentFixture<PolisHolderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PolisHolderComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PolisHolderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
