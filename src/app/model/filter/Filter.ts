export class Filter {
  polisFieldFilterId: number = 1;
  polisFieldFilterDirection: number = 1;
  startDate: Date = null;
  endDate: Date = null;
  productName: string = '--Semua--';
  status: string = '--Semua--';
}
