export class RuleAgeSetupModel {
  id: number;
  version: number;
  genId: string;
  productId: number;
  insuredType: string;
  relation: string;
  minPolicyHolderEntryAgeInYear: number;
  minPolicyHolderEntryAgeInMonth: number;
  minPolicyHolderEntryAgeInDay: number;
  minInsuredEntryAgeInYear: number;
  minInsuredEntryAgeInMonth: number;
  minInsuredEntryAgeInDay: number;
  maxPolicyHolderEntryAgeInYear: number;
  maxPolicyHolderEntryAgeInMonth: number;
  maxPolicyHolderEntryAgeInDay: number;
  maxInsuredEntryAgeInYear: number;
  maxInsuredEntryAgeInMonth: number;
  maxInsuredEntryAgeInDay: number;
  minInsuredCoverageAge: number;
  maxInsuredCoverageAge: number;
  updateDate: Date;
  updateBy: string;
}
