export class FeeSetupModel {
  id: number;
  version: number;
  genId: string;
  productId: number;
  code: string;
  feeType: string;
  rate: number;
  updateDate: Date;
  updateBy: string;
}
