export class CoiModel {
  id: number;
  version: number;
  baseCoi: number;
  totalRiderCoi: number;
  totalCoi: number;
  coiPercentage: number;
}
