export class WithdrawalScheduleModel {
  id: number;
  version: number;
  withdrawalType: string;
  startDate: Date;
  endDate: Date;
  paymentMethod: string;
  paymenMode: string;
  yearFrom: number;
  yearTo: number;
  amount: number;
  description: string;
}
