export class TopupModel {
  id: number;
  version: number;
  year: number;
  amount: number;
  description: string;
}
