export class FundSetupModel {
  id: number;
  version: number;
  genId: string;
  productId: number;
  code: string;
  name: string;
  managerCode: string;
  lowInvestRate: number;
  midInvestRate: number;
  highInvestRate: number;
  investmentFee: number;
  updateDate: Date;
  updateBy: string;
}
