import { CustomerModel } from '@app/model/esub/CustomerModel';

export class InsuredModel extends CustomerModel {
  insuredSeq: number;
  relationWithPolicyHolder: string;
}
