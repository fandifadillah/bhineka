import { InsuredModel } from '@app/model/esub/InsuredModel';

export class SubInsuredModel extends InsuredModel {
  relationWithMainInsured: string;
}
