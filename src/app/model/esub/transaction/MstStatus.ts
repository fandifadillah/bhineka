import { EBase } from '@app/model/esub/transaction/EBase';

export let DRAFT = 'draft';
export let SUBMITTED = 'submitted';
export let INACTIVE_AGENT = 'inactive.agent';
export let IDCARD_REJECTED = 'idcard.rejected';
export let INCOMPLETE_AGENT_CORE = 'incomplete.agent.form';
export let LICENSE_EXPIRED = 'license.expired';
export let PAYMENT_INVALID = 'payment.invalid';
export let PAYMENT_VALIDATION = 'payment.validation';
export let PAID = 'paid';

export let DRAFT_LABEL = 'Draft';
export let SUBMITTED_LABEL = 'Belum Bayar';
export let INACTIVE_AGENT_LABEL = 'Inactive agent';
export let IDCARD_REJECTED_LABEL = 'Idcard rejected';
export let INCOMPLETE_AGENT_CORE_LABEL = 'Incomplete agent form';
export let LICENSE_EXPIRED_LABEL = 'License expired';
export let PAYMENT_INVALID_LABEL = 'Payment invalid';
export let PAYMENT_VALIDATION_LABEL = 'Payment validation';
export let PAID_LABEL = 'Validasi Pembayaran';

export let DRAFT_ID = 0;
export let SUBMITTED_ID = 1;
export let INACTIVE_AGENT_ID = 2;
export let IDCARD_REJECTED_ID = 3;
export let INCOMPLETE_AGENT_CORE_ID = 4;
export let LICENSE_EXPIRED_ID = 5;
export let PAYMENT_INVALID_ID = 6;
export let PAYMENT_VALIDATION_ID = 7;
export let PAID_ID = 8;

export class MstStatus extends EBase {
  status: string;
  label: string;
  comment: string;
}
