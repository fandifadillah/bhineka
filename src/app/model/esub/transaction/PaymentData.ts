export let GOPAY = 1;
export let TRANSFER = 2;

export class PaymentData {
  no_ktp: string = '';
  kode_setor: string = '';
  metode_pembayaran: number = 1;
  payment_date: Date;
}
