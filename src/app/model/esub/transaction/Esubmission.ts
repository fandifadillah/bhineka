import { Tertanggung } from './Tertanggung';

export class Esubmission {
  no_ktp: string = '';
  nama_pempol: string = '';

  produk: any = '';

  pilihan_paket: string = '';

  alamat: string = '';
  propinsi: string = '';

  kota: string = '';

  kecamatan: string = '';

  kelurahan: string = '';

  kode_pos: string = '';

  no_agen: string = '';
  no_hp: string = '';
  metode_pembayaran: string = '';

  kode_emoney: string = '';

  file_ktp: string = '';
  file_spaj: string = '';

  file_scan_pembayaran: string = '';

  pilihan_cetak: string = '';

  epolicy_email: string = '';

  epolicy_sms: string = '';

  epolicy_wa: string = '';
  tertanggung: Tertanggung[];

  // @JsonProperty("tertanggung")
  // @SerializedName("tertanggung")
  // List<Tertanggung> tertanggungList = new ArrayList<>();

  kd_ukerja: string = '';

  nohp_polis: string = '';

  nama: string = '';
  tgl_pengajuan: string = '';

  premi: string = '';
  status: string = '';
  penghasilan_per_tahun: string = '';
  pekerjaan: string = '';

  cetak: boolean = false;

  agent_signature_date: Date = null;
  customer_signature_date: Date = null;
}
