export class EBase {
  id: number;
  created: Date = new Date();
  createdBy: number;
  updated: Date = new Date();
  updatedBy: any;
}
