import { Esubmission } from '@app/model/esub/transaction/Esubmission';
import { EBase } from '@app/model/esub/transaction/EBase';

export class Submission extends EBase {
  spajNo: string = '';
  noVa: string = '';
  version: number;
  groupPolicyId: string = '';
  agentCode: string = '';
  agentName: string = '';
  officeName: string = '';
  json: string = null;
  spajCloudUrl: string = '';
  idCardCloudUrl: string = '';
  kodeSetor: string = '';
  metodePembayaran: number;
  paymentDate: any;
  esubmission: Esubmission = new Esubmission();
  tertanggungListJson: string = '[]';

  constructor() {
    super();
    if (this.json == null && this.esubmission != null) {
      this.json = JSON.stringify(this.esubmission);
    } else if (this.json != null && this.esubmission == null) {
      this.esubmission = JSON.parse(this.json);
    } else if (this.json != null && this.esubmission != null) {
      this.json = JSON.stringify(this.esubmission);
    }
  }
}
