import { MstStatus } from '@app/model/esub/transaction/MstStatus';
import { EBase } from '@app/model/esub/transaction/EBase';

export class PVSubmissionStatus extends EBase {
  status: MstStatus;
}
