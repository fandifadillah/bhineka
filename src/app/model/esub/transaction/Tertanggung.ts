export class Tertanggung {
  no: number;
  nama: string = '';
  tgl_lahir: string = null;
  hubungan: string = null;

  tglLahirError: string = null;
}
