import { Submission } from '@app/model/esub/transaction/Submission';
import { PVSubmissionStatus } from '@app/model/esub/transaction/PVSubmissionStatus';
import { PaymentData } from '@app/model/esub/transaction/PaymentData';
import { DRAFT, DRAFT_ID, DRAFT_LABEL, MstStatus } from '@app/model/esub/transaction/MstStatus';

export let mstStatus = {
  id: DRAFT_ID,
  status: DRAFT,
  label: DRAFT_LABEL,
  comment: '',
  created: new Date(),
  createdBy: 0,
  updated: new Date(),
  updatedBy: 0
};

export class TrxSubmission extends Submission {
  draft_id: number;
  submissionStatuses: PVSubmissionStatus[];
  submissionStatus: MstStatus = null;
  submissionStatusJson: string = null;

  paymentData: PaymentData = new PaymentData();
  ktp_base64: string;
  agent_sign_base64: string;
  customer_sign_base64: string;

  constructor() {
    super();
    if (this.submissionStatusJson == null && this.submissionStatus != null) {
      this.submissionStatusJson = JSON.stringify(this.submissionStatus);
    } else if (this.submissionStatusJson != null && this.submissionStatus == null) {
      this.submissionStatus = JSON.parse(this.submissionStatusJson);
    } else if (this.submissionStatusJson != null && this.submissionStatus != null) {
      this.submissionStatusJson = JSON.stringify(this.submissionStatus);
    } else if (this.submissionStatus == null && this.submissionStatusJson == null) {
      this.submissionStatus = mstStatus;
      this.submissionStatusJson = JSON.stringify(this.submissionStatus);
    }
    if (!this.paymentData) {
      this.paymentData = new PaymentData();
    }
  }
}
