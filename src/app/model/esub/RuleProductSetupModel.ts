export class RuleProductSetupModel {
  id: number;
  version: number;
  genId: string;
  productId: number;
  ruleCode: string;
  relation: string;
  insuredAgeFrom: number;
  insuredAgeTo: number;
  paymentTerm: number;
  coverageTerm: number;
  coverageAge: number;
  mutualExclusiveExceptionProduct: string;
  mandatoryPrerequisitesProduct: string;
  needRegularTopup: string;
  coiHoldYearFrom: number;
  coiHoldYearTo: number;
  coiDeductYearFrom: number;
  coiDeductYearTo: number;
  updateDate: Date;
  updateBy: string;
}
