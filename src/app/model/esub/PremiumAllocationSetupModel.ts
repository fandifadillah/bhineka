export class PremiumAllocationSetupModel {
  id: number;
  version: number;
  genId: string;
  productId: number;
  paymentTermFrom: number;
  paymentTermTo: number;
  coverageTermFrom: number;
  coverageTermTo: number;
  policyAgeFrom: number;
  policyAgeTo: number;
  basicPremiumAllocationRate: number;
  singleTopupAllocationRate: number;
  regularTopupAllocationRate: number;
  updateDate: Date;
  updateBy: string;
}
