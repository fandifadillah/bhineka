export class FundModel {
  id: number;
  version: number;
  code: string;
  name: string;
  rate: number;
}
