export class ExtraRiskModel {
  healthRisk: number;
  occupationRisk: number;
  recreationRisk: number;
  residenceRisk: number;
  healthRiskAlgorithm: string;
  occupationRiskAlgorithm: string;
  recreationRiskAlgorithm: string;
  residenceRiskAlgorithm: string;
  healthRiskUnit: number;
  occupationRiskUnit: number;
  recreationRiskUnit: number;
  residenceRiskUnit: number;
  healthRiskTerm: string;
  occupationRiskTerm: string;
  recreationRiskTerm: string;
  residenceRiskTerm: string;
  healthRiskStartDate: Date;
  occupationRiskStartDate: Date;
  recreationRiskStartDate: Date;
  residenceRiskStartDate: Date;
  healthRiskEndDate: Date;
  occupationRiskEndDate: Date;
  recreationRiskEndDate: Date;
  residenceRiskEndDate: Date;
}
