import { ProductModel } from '@app/model/esub/ProductModel';

export class RiderModel extends ProductModel {
  selected: boolean;
  productType: string;
  category: string;
  riderType: string;
}
