import { PolicyHolderModel } from '@app/model/esub/PolicyHolderModel';
import { InsuredModel } from '@app/model/esub/InsuredModel';
import { SubInsuredModel } from '@app/model/esub/SubInsuredModel';
import { BaseModel } from '@app/model/esub/BaseModel';
import { FundModel } from '@app/model/esub/FundModel';
import { RiderModel } from '@app/model/esub/RiderModel';
import { TopupModel } from '@app/model/esub/TopupModel';
import { WithdrawalModel } from '@app/model/esub/WithdrawalModel';
import { CoiModel } from '@app/model/esub/CoiModel';
import { WithdrawalScheduleModel } from '@app/model/esub/WithdrawalScheduleModel';

export class PolicyModel {
  description: string;
  policyId: number;
  policyHolder: PolicyHolderModel;
  insured: InsuredModel;
  additionalInsured: InsuredModel[];
  subInsured: SubInsuredModel[];
  base: BaseModel;
  funds: FundModel[];
  riders: RiderModel[];
  topups: TopupModel[];
  withdrawals: WithdrawalModel[];
  coi: CoiModel;
  withdrawalSchedule: WithdrawalScheduleModel;

  constructor() {
    this.policyHolder = new PolicyHolderModel();
    this.insured = new InsuredModel();
    this.base = new BaseModel();
    this.coi = new CoiModel();
    this.withdrawalSchedule = new WithdrawalScheduleModel();
    this.topups = [];
  }
}
