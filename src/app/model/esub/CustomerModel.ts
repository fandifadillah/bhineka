import { ExtraRiskModel } from '@app/model/esub/ExtraRiskModel';

export class CustomerModel extends ExtraRiskModel {
  id: number;
  version: number;
  customerType: number;
  firstName: string;
  middleName: string;
  lastName: string;
  maritalStatus: string;
  sex: string;
  dob: string;
  age: number;
  smoker: string;
  riskClass: string;
  riskClassCode: string;
}
