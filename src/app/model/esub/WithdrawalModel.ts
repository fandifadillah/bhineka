export class WithdrawalModel {
  id: number;
  version: number;
  year: number;
  amount: number;
  description: string;
}
