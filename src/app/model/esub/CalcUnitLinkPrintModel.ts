export class CalcUnitLinkPrintModel {
  id: number;
  version: number;
  groupPolicyId: number;
  fundCode: string;
  fundName: string;
  policyAge: number;
  insuredAge: number;
  basicPremium: number;
  regularTopup: number;
  singleTopup: number;
  withdrawal: number;
  sumAssured: number;
  investLow: number;
  investMid: number;
  investHigh: number;
  deathLow: number;
  deathMid: number;
  deathHigh: number;
}
