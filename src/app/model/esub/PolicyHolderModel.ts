import { CustomerModel } from '@app/model/esub/CustomerModel';

export class PolicyHolderModel extends CustomerModel {
  isInstitution: boolean;
  institutionName: string;
}
