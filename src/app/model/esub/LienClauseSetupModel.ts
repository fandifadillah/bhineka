export class LienClauseSetupModel {
  id: number;
  version: number;
  genId: string;
  productId: number;
  insuredAgeFrom: number;
  insuredAgeTo: number;
  rate: number;
  updateDate: Date;
  updateBy: string;
}
