export class MstApplication {
  version: number;
  api: number;
  platform: string;
  appVersion: string;
  status: string;
  startDate: Date;
  cutOffDate: Date;
  expiryDate: Date;
  message: string;
  url: string;
}
