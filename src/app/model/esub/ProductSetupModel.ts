export class ProductSetupModel {
  id: number;
  version: number;
  genId: number;
  productId: number;
  productCode: string;
  name: string;
  currency: string;
  category: string;
  productType: string;
  financeType: string;
  industry: string;
  salesChannel: string;
  configBased: string;
  birthdayCalculationMode: string;
  coiCalculationRuleCode: string;
  validationRuleList: string;
  illustrationTemplateCode: string;
  enable: boolean;
  expiryDate: Date;
  image: string;
  wording: string;
  updateDate: Date;
  updateBy: string;
}
