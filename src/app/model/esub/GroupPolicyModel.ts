import { PolicyModel } from '@app/model/esub/PolicyModel';

export class GroupPolicyModel {
  id: number;
  version: number;
  groupPolicyId: number;
  agentCode: string;
  agentName: string;
  createDate: string;
  policys: PolicyModel[];
  updateBy: string;
  updateDate: Date;
}
