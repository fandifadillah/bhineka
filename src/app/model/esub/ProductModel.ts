import { ExtraRiskModel } from '@app/model/esub/ExtraRiskModel';

export class ProductModel extends ExtraRiskModel {
  id: number;
  version: number;
  insuredId: number;
  productId: number;
  productCode: string;
  productName: string;
  currency: string;
  paymentMode: string;
  coverageTerm: number;
  paymentTerm: number;
  coverageAge: number;
  basicPremium: number;
  basicPremiumPlan: any;
  savingPremium: number;
  savingPremiumPlan: any;
  totalExtraPremium: number;
  totalPremium: number;
  totalPremiumPlan: any;
  premiumDiscount: number;
  paidPremium: number;
  basicSumAssured: number;
  basicSumAssuredPlan: any;
  totalSumAssured: number;
  coi: number;
  extraCoi: number;
  totalCoi: number;
  inflationRate: number;
  inflationRatePlan: any;
  regularTopup: number;
  regularTopupPlan: any;
  healthRiskAmount: number;
  occupationRiskAmount: number;
  recreationRiskAmount: number;
  residenceRiskAmount: number;
}
