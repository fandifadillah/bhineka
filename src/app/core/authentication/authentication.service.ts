import { Injectable } from '@angular/core';
import { from, Observable, of } from 'rxjs';

import { Credentials, CredentialsService } from './credentials.service';
import { logger } from 'codelyzer/util/logger';
import { HttpService } from '@app/core';
import { defaultHttpOptions, IMEI, LOGIN_URL, PLATFORM, VERSION_CODE } from '@app/app.config';
import { AlertComponent } from '@app/shared/dialog/alert/alert.component';
import { Utility } from '@app/utils/utility.service';
import { MatDialog } from '@angular/material';
import { LoaderComponent } from '@app/shared';
import { EsubLocalService } from '@app/platform/dao/local/esub-local.service';

export interface LoginContext {
  username: string;
  password: string;
  remember?: boolean;
}

/**
 * Provides a base for authentication workflow.
 * The login/logout methods should be replaced with proper implementation.
 */
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  progressDialogRef: any = null;

  constructor(
    private credentialsService: CredentialsService,
    private httpService: HttpService,
    public dialog: MatDialog,
    private utility: Utility,
    public progressDialog: MatDialog,
    public esubLocalService: EsubLocalService
  ) {}

  /**
   * Authenticates the user.
   * @param context The login parameters.
   * @return The user credentials.
   */
  login(context: LoginContext): Observable<Credentials> {
    // Replace by proper authentication call
    const data = {
      username: context.username,
      authorization: '123456',
      jLogin: {},
      created: new Date()
    };
    logger.debug('DATA #### {} ', JSON.stringify(data));
    this.credentialsService.setCredentials(data, true);
    return of(data);
  }

  auth(context: LoginContext, callback: any): any {
    var formData = new FormData();
    formData.append('username', this.utility.encryptStringWithRsaPrivateKey(context.username));
    formData.append('password', this.utility.encryptStringWithRsaPrivateKey(context.password));
    formData.append('platform', PLATFORM);
    formData.append('imei', localStorage.getItem(IMEI));
    formData.append('applicationVersion', localStorage.getItem(VERSION_CODE).toString());

    const data = {
      username: context.username,
      authorization: '1234567890',
      jLogin: {},
      created: new Date()
    };

    // if (1 + 1 === 2) {
    //   this.credentialsService.setCredentials(data, true);
    //   callback(data, true);
    //   return;
    // }
    // console.log('USERNAME :: ' + this.utility.encryptStringWithRsaPrivateKey(context.username));
    // console.log('PASSWORD :: ' + this.utility.encryptStringWithRsaPrivateKey(context.password));
    this.openProgress();
    const $service = this.httpService.post(LOGIN_URL, formData, defaultHttpOptions);
    $service.pipe().subscribe(
      (value: any) => {
        console.log('VALUE ' + JSON.stringify(value));
        data.authorization = value.message;
        data.jLogin = value.data;
        data.created = new Date();
        this.credentialsService.setCredentials(data, true);
        this.closeProgress();
        callback(value, true);
      },
      (error: any) => {
        console.log('ERROR ' + JSON.stringify(error));
        callback(error, false);
        this.closeProgress();
        if (error.status == 0) {
          this.openDialog('Network Problem', 'Login tidak bisa diproses. Mohon periksa koneksi internet anda');
        } else if (error.status == 500 && error.error.moreInformation === 'Failed to establish a backside connection') {
          this.openDialog('Connection Timeout', "Server doesn't reply, please try again later");
        } else {
          switch (error.error.message) {
            case 'InvalidUserName':
              this.openDialog('Login Gagal', 'Username tidak valid');

              break;

            default:
              this.openDialog('Login Gagal', error.error.message);
              break;
          }
        }
      },
      () => {}
    );
    return $service;
  }

  /**
   * Logs out the user and clear credentials.
   * @return True if the user was logged out successfully.
   */
  logout(): Observable<boolean> {
    // Customize credentials invalidation here
    this.credentialsService.setCredentials();
    return of(true);
  }

  openDialog(title: string, message: string): void {
    const dialogRef = this.dialog.open(AlertComponent, {
      width: '80%',
      data: { title: title, message: message }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  openProgress(): void {
    if (this.progressDialogRef == null) {
      this.progressDialogRef = this.progressDialog.open(LoaderComponent, {
        data: {}
      });
      this.progressDialogRef.afterClosed().subscribe((result: any) => {
        console.log('The dialog was closed');
      });
    }
    this.progressDialogRef.disableClose = true;
  }

  closeProgress() {
    if (this.progressDialogRef) {
      this.progressDialog.closeAll();
      this.progressDialogRef = null;
    }
  }
}
