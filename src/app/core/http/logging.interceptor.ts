import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
// import { AuthService } from './auth/auth.service';
// import { Observable } from 'rxjs/Observable';
import { tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Logger } from '@app/core'; // fancy pipe-able operators

var log = new Logger('LoggingInterceptor');
@Injectable()
export class LoggingInterceptor implements HttpInterceptor {
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      tap(
        response => log.debug('RESPONSE === ' + JSON.stringify(response)),
        error => log.debug('ERROR === ' + JSON.stringify(error))
      )
    );
  }
}
