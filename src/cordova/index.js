/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
  // Application Constructor
  initialize: function() {
    document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    document.addEventListener(
      'exitApp',
      function(evt) {
        console.log('exitApp event ### ');
        // cordova.exitApp();
        navigator.app.exitApp();
      },
      false
    );
  },

  onDeviceReady: function() {
    var db = window.sqlitePlugin.openDatabase({
      name: 'lifepos.sqlite.db',
      location: 'default',
      androidDatabaseImplementation: 2,
      createFromLocation: 1
    });
    console.log('openDatabase done-------------------------------------');
    db.transaction(function(txn) {
      console.log('readTransaction done-------------------------------------');
    }),
      function(error) {
        console.log('Transaction ERROR: ' + error.message);
      },
      function() {
        console.log('Populated database OK');
      };

    this.receivedEvent('deviceready');
  },

  // Update DOM on a Received Event
  receivedEvent: function(id) {
    console.log('Received Event: ' + id);
  }
};

function exitApp() {
  navigator.app.exitApp();
}

app.initialize();
